﻿using AppNivel.Resources;
using System.Threading;

namespace AppNivel.iOS
{
    public class CloseApplication : ICloseApplication
    {
        public CloseApplication()
        {
        }

        public void QuitApplication()
        {
            Thread.CurrentThread.Abort();
        }
    }
}