﻿using AppNivel.Helpers;
using AppNivel.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(LandscapePage), typeof(LandscapePageRenderer))]

namespace AppNivel.iOS
{
    public class LandscapePageRenderer : PageRenderer
    {
        public override bool ShouldAutorotate()
        {
            return false;
        }

        public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations()
        {
            return UIInterfaceOrientationMask.Landscape;
        }

        public override UIInterfaceOrientation PreferredInterfaceOrientationForPresentation()
        {
            return UIInterfaceOrientation.LandscapeRight;
        }
    }
}