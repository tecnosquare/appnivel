﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using AppNivel.Droid;
using AppNivel.Helpers;
using AppNivel.Pages;
using AppNivel.Resources;
using FFImageLoading.Forms.Droid;
using Java.Util;
using Plugin.CurrentActivity;
using Plugin.Permissions;
using PushNotification.Plugin;
using UXDivers.Artina.Shared;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using XLabs.Ioc;
using XLabs.Platform.Device;

[assembly: Xamarin.Forms.Dependency(typeof(CloseApplication))]

namespace AppNivel.Droid
{
    [Activity(
        Label = "NivelCard",
        Icon = "@drawable/icon",
        Theme = "@style/Theme.Splash",
        MainLauncher = true,
        LaunchMode = LaunchMode.SingleTask,
        ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize | ConfigChanges.Locale | ConfigChanges.LayoutDirection
        )
    ]
    //[Activity(Label = "NivelCard", Icon = "@drawable/icon", Theme = "@style/Theme.Splash", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    //public class MainActivity : XFormsAppCompatDroid, Application.IActivityLifecycleCallbacks
    public class MainActivity : FormsAppCompatActivity
    {
        private Locale _locale;

        protected override void OnCreate(Bundle bundle)
        {
            var width = (int)Resources.DisplayMetrics.WidthPixels;
            var height = (int)Resources.DisplayMetrics.HeightPixels;
            Settings.DeviceWidth = width;
            Settings.DeviceHeight = height;
            Settings.DeviceVersionCode = (int)Build.VERSION.SdkInt;
            //TabLayoutResource = Resource.Layout.Tabbar;
            //ToolbarResource = Resource.Layout.Toolbar;

            MessagingCenter.Subscribe<QRCodePage>(this, "LandscapeOnly", sender =>
            {
                RequestedOrientation = ScreenOrientation.Landscape;
            });
            //during page close setting back to portrait
            MessagingCenter.Subscribe<QRCodePage>(this, "PortraitOnly", sender =>
            {
                RequestedOrientation = ScreenOrientation.Portrait;
            });

            //TEMA
            base.Window.RequestFeature(WindowFeatures.ActionBar);
            base.SetTheme(Resource.Style.AppTheme);

            FormsAppCompatActivity.ToolbarResource = Resource.Layout.Toolbar;
            FormsAppCompatActivity.TabLayoutResource = Resource.Layout.Tabs;
            //TEMA

            base.OnCreate(bundle);

            //TEMA

            // Initializing FFImageLoading
            CachedImageRenderer.Init(enableFastRenderer: false);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            UXDivers.Artina.Shared.GrialKit.Init(this, "AppNivel.Droid.GrialLicense");

            //TEMA

            var resolverContainer = new SimpleContainer();
            resolverContainer.Register<IDevice>(t => AndroidDevice.CurrentDevice);

            if (Resolver.IsSet)
            {
                Resolver.ResetResolver(resolverContainer.GetResolver());
            }
            else
            {
                Resolver.SetResolver(resolverContainer.GetResolver());
            }

            global::Xamarin.Forms.Forms.Init(this, bundle);
            //ImageCircleRenderer.Init();
            CrossPushNotification.Initialize<CrossPushNotificationListener>(Keys.GOOGLE_APIs_ID);

            //TEMA

            FormsHelper.ForceLoadingAssemblyContainingType(typeof(UXDivers.Effects.Effects));

            _locale = Resources.Configuration.Locale;

            ReferenceCalendars();

            //TEMA

            LoadApplication(new App());
        }

        //TEMA

        public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);

            GrialKit.NotifyConfigurationChanged(newConfig);

            if ((int)Build.VERSION.SdkInt <= 19 &&
                !_locale.Equals(newConfig.Locale))
            {
                // Need to recreate the activity when locale has changed for APIs 18 and 19
                // as changes in ConfigChanges.Locale brake images used in the app
                Recreate();
            }
        }

        private void ReferenceCalendars()
        {
            // When compiling in release, you may need to instantiate the specific
            // calendar so it doesn't get stripped out by the linker. Just uncomment
            // the lines you need according to the localization needs of the app.
            // For instance, in 'ar' cultures UmAlQuraCalendar is required.
            // https://bugzilla.xamarin.com/show_bug.cgi?id=59077

            new System.Globalization.UmAlQuraCalendar();
            // new System.Globalization.ChineseLunisolarCalendar();
            // new System.Globalization.ChineseLunisolarCalendar();
            // new System.Globalization.HebrewCalendar();
            // new System.Globalization.HijriCalendar();
            // new System.Globalization.IdnMapping();
            // new System.Globalization.JapaneseCalendar();
            // new System.Globalization.JapaneseLunisolarCalendar();
            // new System.Globalization.JulianCalendar();
            // new System.Globalization.KoreanCalendar();
            // new System.Globalization.KoreanLunisolarCalendar();
            // new System.Globalization.PersianCalendar();
            // new System.Globalization.TaiwanCalendar();
            // new System.Globalization.TaiwanLunisolarCalendar();
            // new System.Globalization.ThaiBuddhistCalendar();
        }

        //TEMA

        public void OnActivityCreated(Activity activity, Bundle savedInstanceState)
        {
            CrossCurrentActivity.Current.Activity = activity;
        }

        public void OnActivityDestroyed(Activity activity)
        {
        }

        public void OnActivityPaused(Activity activity)
        {
        }

        public void OnActivityResumed(Activity activity)
        {
            CrossCurrentActivity.Current.Activity = activity;
        }

        public void OnActivitySaveInstanceState(Activity activity, Bundle outState)
        {
        }

        public void OnActivityStarted(Activity activity)
        {
            CrossCurrentActivity.Current.Activity = activity;
        }

        public void OnActivityStopped(Activity activity)
        {
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}