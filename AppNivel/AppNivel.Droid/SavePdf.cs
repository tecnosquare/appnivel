﻿using Android.Content;
using AppNivel.Resources;
using Java.IO;
using System;
using System.IO;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(AppNivel.Droid.SavePdf))]

namespace AppNivel.Droid
{
    internal class SavePdf : ISavePdf
    {
        public async Task SaveFile(string fileName, string contentType, MemoryStream stream)
        {
            try
            {
                string root = null;

                if (Android.OS.Environment.IsExternalStorageEmulated)
                {
                    root = Android.OS.Environment.ExternalStorageDirectory.ToString();
                }
                else
                    root = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

                Java.IO.File myDir = new Java.IO.File(root + "/Syncfusion");

                myDir.Mkdir();

                Java.IO.File file = new Java.IO.File(myDir, fileName);

                if (file.Exists()) file.Delete();

                try
                {
                    FileOutputStream outs = new FileOutputStream(file);
                    outs.Write(stream.ToArray());
                    outs.Flush();
                    outs.Close();
                }
                catch (Exception e)
                {
                    //resume
                }

                if (file.Exists())
                {
                    Android.Net.Uri path = Android.Net.Uri.FromFile(file);
                    string extension =
                        Android.Webkit.MimeTypeMap.GetFileExtensionFromUrl(Android.Net.Uri.FromFile(file).ToString());
                    string mimeType = Android.Webkit.MimeTypeMap.Singleton.GetMimeTypeFromExtension(extension);
                    Intent intent = new Intent(Intent.ActionView);
                    intent.SetDataAndType(path, mimeType);
                    Forms.Context.StartActivity(Intent.CreateChooser(intent, "Escolha o aplicativo para abrir o arquivo"));
                }
            }
            catch (Exception ex)
            {
                //Resume
            }
        }
    }
}