using Android.App;
using Android.Content;
using Android.Runtime;
using AppNivel.Helpers;
using AppNivel.Resources;
using Plugin.Connectivity;
using PushNotification.Plugin;
using System;

namespace AppNivel.Droid
{
    //This is a starting point application class so that push notifications can be handle even when push is closed.

    [Application]
    public class PushNotificationAppStarter : Application
    {
        public static Context AppContext;

        public PushNotificationAppStarter(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public override void OnCreate()
        {
            base.OnCreate();

            try
            {
                AppContext = this.ApplicationContext;
                CrossPushNotification.Initialize<CrossPushNotificationListener>(Keys.GOOGLE_APIs_ID);
                StartPushService();
            }
            catch (Exception ex)
            {
                //resume...
            }
        }

        public static void StartPushService()
        {
            try
            {
                AppContext.StartService(new Intent(AppContext, typeof(PushNotificationService)));

                if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Kitkat)
                {
                    if (CrossConnectivity.Current.IsConnected)
                    {
                        PendingIntent pintent = PendingIntent.GetService(AppContext, 0,
                            new Intent(AppContext, typeof(PushNotificationService)), 0);
                        AlarmManager alarm = (AlarmManager)AppContext.GetSystemService(Context.AlarmService);
                        alarm.Cancel(pintent);
                    }
                }
            }
            catch (Exception ex)
            {
                //resume...
            }
        }

        public static void StopPushService()
        {
            try
            {
                AppContext.StopService(new Intent(AppContext, typeof(PushNotificationService)));
                if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Kitkat)
                {
                    if (CrossConnectivity.Current.IsConnected)
                    {
                        PendingIntent pintent = PendingIntent.GetService(AppContext, 0,
                            new Intent(AppContext, typeof(PushNotificationService)), 0);
                        AlarmManager alarm = (AlarmManager)AppContext.GetSystemService(Context.AlarmService);
                        alarm.Cancel(pintent);
                    }
                }
            }
            catch (Exception ex)
            {
                //resume...
            }
        }
    }
}