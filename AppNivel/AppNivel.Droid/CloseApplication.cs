using Android.App;
using AppNivel.Resources;
using Xamarin.Forms;

namespace AppNivel.Droid
{
    public class CloseApplication : ICloseApplication
    {
        public CloseApplication()
        {
        }

        public void QuitApplication()
        {
            var activity = (Activity)Forms.Context;
            activity.FinishAffinity();
        }
    }
}