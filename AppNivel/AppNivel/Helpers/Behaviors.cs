﻿using AppNivel.Resources;
using System;
using System.Globalization;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace AppNivel.Helpers
{
    public static class NumericValidationBehavior
    {
        public static readonly BindableProperty AttachBehaviorProperty =
            BindableProperty.CreateAttached(
                "AttachBehavior",
                typeof(bool),
                typeof(NumericValidationBehavior),
                false,
                propertyChanged: OnAttachBehaviorChanged);

        public static bool GetAttachBehavior(BindableObject view)
        {
            return (bool)view.GetValue(AttachBehaviorProperty);
        }

        public static void SetAttachBehavior(BindableObject view, bool value)
        {
            view.SetValue(AttachBehaviorProperty, value);
        }

        private static void OnAttachBehaviorChanged(BindableObject view, object oldValue, object newValue)
        {
            var entry = view as Entry;
            if (entry == null)
            {
                return;
            }

            bool attachBehavior = (bool)newValue;
            if (attachBehavior)
            {
                entry.TextChanged += OnEntryTextChanged;
            }
            else
            {
                entry.TextChanged -= OnEntryTextChanged;
            }
        }

        private static void OnEntryTextChanged(object sender, TextChangedEventArgs args)
        {
            double result;
            bool isValid = double.TryParse(args.NewTextValue, out result);
            ((Entry)sender).TextColor = isValid ? Color.Default : Color.Red;
        }
    }

    public class EmailValidatorBehavior : Behavior<Entry>
    {
        private const string emailRegex = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
            @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";

        private static readonly BindablePropertyKey IsValidPropertyKey = BindableProperty.CreateReadOnly("IsValid", typeof(bool), typeof(NumericValidationBehavior), false);

        public static readonly BindableProperty IsValidProperty = IsValidPropertyKey.BindableProperty;

        public bool IsValid
        {
            get { return (bool)base.GetValue(IsValidProperty); }
            private set { base.SetValue(IsValidPropertyKey, value); }
        }

        protected override void OnAttachedTo(Entry bindable)
        {
            bindable.TextChanged += HandleTextChanged;
        }

        private void HandleTextChanged(object sender, TextChangedEventArgs e)
        {
            IsValid = (Regex.IsMatch(e.NewTextValue, emailRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));
            ((Entry)sender).TextColor = IsValid ? Color.Default : Color.Red;
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            bindable.TextChanged -= HandleTextChanged;
        }
    }

    public class DecimalValueValidatorBehavior : Behavior<Entry>
    {
        private const string valueRegex = @"^\d+(\,\d{1,2})?$";

        private static readonly BindablePropertyKey IsValidPropertyKey = BindableProperty.CreateReadOnly("IsValid", typeof(bool), typeof(decimal), false);

        public static readonly BindableProperty IsValidProperty = IsValidPropertyKey.BindableProperty;

        public bool IsValid
        {
            get { return (bool)base.GetValue(IsValidProperty); }
            private set { base.SetValue(IsValidPropertyKey, value); }
        }

        protected override void OnAttachedTo(Entry bindable)
        {
            bindable.TextChanged += HandleTextChanged;
        }

        private void HandleTextChanged(object sender, TextChangedEventArgs e)
        {
            IsValid = (Regex.IsMatch(e.NewTextValue, valueRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));

            if (IsValid)
            {
                var dec = 0m;
                IsValid = decimal.TryParse(e.NewTextValue, NumberStyles.Currency, CultureInfo.CurrentCulture, out dec);
            }

            ((Entry)sender).TextColor = IsValid ? Color.Default : Color.Silver;
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            bindable.TextChanged -= HandleTextChanged;
        }
    }

    public class DocumentValidatorBehavior : Behavior<Entry>
    {
        private const string cpfRegex = @"(^(\d{3}.\d{3}.\d{3}-\d{2})|(\d{11})$)";
        private const string cnpjRegex = @"(^(\d{2}.\d{3}.\d{3}/\d{4}-\d{2})|(\d{14})$)";
        private const string nifRegex = @"(^(\d{9})$)";

        private static readonly BindablePropertyKey IsValidPropertyKey = BindableProperty.CreateReadOnly("IsValid", typeof(bool), typeof(NumericValidationBehavior), false);

        public static readonly BindableProperty IsValidProperty = IsValidPropertyKey.BindableProperty;

        public bool IsValid
        {
            get { return (bool)base.GetValue(IsValidProperty); }
            private set { base.SetValue(IsValidPropertyKey, value); }
        }

        protected override void OnAttachedTo(Entry bindable)
        {
            bindable.TextChanged += HandleTextChanged;
        }

        private void HandleTextChanged(object sender, TextChangedEventArgs e)
        {
            switch (CultureInfo.CurrentCulture.Name)
            {
                case "pt-PT":
                    IsValid = (Regex.IsMatch(e.NewTextValue, nifRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));

                    if (IsValid)
                    {
                        IsValid = Util.VerifyNif(e.NewTextValue);
                    }

                    ((Entry)sender).TextColor = IsValid ? Color.Default : Color.Red;
                    break;

                default:
                    if (e.NewTextValue.Length > 14)
                    {
                        IsValid = (Regex.IsMatch(e.NewTextValue, cnpjRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));

                        if (IsValid)
                        {
                            IsValid = Util.VerifyCnpj(e.NewTextValue);
                        }

                        ((Entry)sender).TextColor = IsValid ? Color.Default : Color.Red;
                    }
                    else
                    {
                        IsValid = (Regex.IsMatch(e.NewTextValue, cpfRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));

                        if (IsValid)
                        {
                            IsValid = Util.VerifyCpf(e.NewTextValue);
                        }

                        ((Entry)sender).TextColor = IsValid ? Color.Default : Color.Red;
                    }
                    break;
            }
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            bindable.TextChanged -= HandleTextChanged;
        }
    }

    public class PostalCodeValidatorBehavior : Behavior<Entry>
    {
        private string postalCodeRegex = @"(^(\d{5}-\d{3})|(\d{8})$)";

        private static readonly BindablePropertyKey IsValidPropertyKey = BindableProperty.CreateReadOnly("IsValid", typeof(bool), typeof(NumericValidationBehavior), false);

        public static readonly BindableProperty IsValidProperty = IsValidPropertyKey.BindableProperty;

        public bool IsValid
        {
            get { return (bool)base.GetValue(IsValidProperty); }
            private set { base.SetValue(IsValidPropertyKey, value); }
        }

        protected override void OnAttachedTo(Entry bindable)
        {
            bindable.TextChanged += HandleTextChanged;
        }

        private void HandleTextChanged(object sender, TextChangedEventArgs e)
        {
            switch (CultureInfo.CurrentCulture.Name)
            {
                case "pt-PT":
                    postalCodeRegex = @"(^(\d{4}-\d{3})|(\d{7})$)";
                    break;
            }

            IsValid = (Regex.IsMatch(e.NewTextValue, postalCodeRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));

            ((Entry)sender).TextColor = IsValid ? Color.Default : Color.Red;
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            bindable.TextChanged -= HandleTextChanged;
        }
    }

    public class ExtendedDocumentValidatorBehavior : Behavior<Entry>
    {
        private const string cpfRegex = @"(^(\d{3}.\d{3}.\d{3}-\d{2})|(\d{11})$)";
        private const string cnpjRegex = @"(^(\d{2}.\d{3}.\d{3}/\d{4}-\d{2})|(\d{14})$)";

        private const string emailRegex = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
            @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";

        private const string nifRegex = @"(^(\d{9})$)";

        private static readonly BindablePropertyKey IsValidPropertyKey = BindableProperty.CreateReadOnly("IsValid", typeof(bool), typeof(NumericValidationBehavior), false);

        public static readonly BindableProperty IsValidProperty = IsValidPropertyKey.BindableProperty;

        public bool IsValid
        {
            get { return (bool)base.GetValue(IsValidProperty); }
            private set { base.SetValue(IsValidPropertyKey, value); }
        }

        protected override void OnAttachedTo(Entry bindable)
        {
            bindable.TextChanged += HandleTextChanged;
        }

        private void HandleTextChanged(object sender, TextChangedEventArgs e)
        {
            //Se contém letras, testamos e-mail
            if (Regex.Matches(e.NewTextValue, @"[a-zA-Z]").Count > 0)
            {
                IsValid = (Regex.IsMatch(e.NewTextValue, emailRegex, RegexOptions.IgnoreCase,
                    TimeSpan.FromMilliseconds(250)));
                ((Entry)sender).TextColor = IsValid ? Color.Default : Color.Silver;
            }
            else
            {
                switch (CultureInfo.CurrentCulture.Name)
                {
                    case "pt-PT":
                        IsValid =
                            (Regex.IsMatch(e.NewTextValue, nifRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));

                        if (IsValid)
                        {
                            IsValid = Util.VerifyNif(e.NewTextValue);
                        }

                        ((Entry)sender).TextColor = IsValid ? Color.Default : Color.Silver;
                        break;

                    default:
                        if (e.NewTextValue.Length > 14)
                        {
                            IsValid =
                                (Regex.IsMatch(e.NewTextValue, cnpjRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));

                            if (IsValid)
                            {
                                IsValid = Util.VerifyCnpj(e.NewTextValue);
                            }

                            ((Entry)sender).TextColor = IsValid ? Color.Default : Color.Silver;
                        }
                        else
                        {
                            IsValid =
                                (Regex.IsMatch(e.NewTextValue, cpfRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));

                            if (IsValid)
                            {
                                IsValid = Util.VerifyCpf(e.NewTextValue);
                            }

                            ((Entry)sender).TextColor = IsValid ? Color.Default : Color.Silver;
                        }
                        break;
                }
            }
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            bindable.TextChanged -= HandleTextChanged;
        }
    }
}