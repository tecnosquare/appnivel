// Helpers/Settings.cs
using AppNivel.Resources;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using Xamarin.Forms;

namespace AppNivel.Helpers
{
    /// <summary>
    /// This is the Settings static class that can be used in your Core solution or in any
    /// of your client applications. All settings are laid out the same exact way with getters
    /// and setters.
    /// </summary>

    public enum Theme
    {
        Light,
        Dark,
        Enterprise,
        Custom
    }

    public static class Settings
    {
        public static void SetTheme(Theme theme)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.Resources.MergedDictionaries.Clear();

                switch (theme)
                {
                    case Theme.Dark:
                        Application.Current.Resources.MergedDictionaries.Add(new GrialDarkTheme());
                        break;

                    case Theme.Enterprise:
                        Application.Current.Resources.MergedDictionaries.Add(new GrialEnterpriseTheme());
                        break;

                    case Theme.Light:
                        Application.Current.Resources.MergedDictionaries.Add(new GrialLightTheme());
                        break;

                    default:
                        Application.Current.Resources.MergedDictionaries.Add(new MyAppTheme());
                        break;
                }
            });
        }

        private static ISettings AppSettings => CrossSettings.Current;

        #region Setting Constants

        public const int ItensPerPage = 20;
        private const string SettingsKey = "settings_key";
        private static readonly string SettingsDefault = string.Empty;
        private const string CurrentPageKey = "CurrentPage";
        private static readonly string CurrentPageDefault = "Main";
        private const string DeviceTokenKey = "AppDeviceToken";
        private static readonly string DeviceTokenDefault = string.Empty;
        private const string AppTokenKey = "AppToken";
        private static readonly string AppTokenDefault = string.Empty;
        private const string AppWsUrlKey = "AppWsUrl";
        private static readonly string AppWsUrlDefault = "https://www.nivel.com.br/wsnivel/ServiceNivel.svc/";
        private const string UserIdKey = "AppUserId";
        private static readonly int UserIdDefault = 0;
        private const string UserChildIdKey = "AppUserChildId";
        private static readonly int UserChildIdDefault = 0;
        private const string IsChildUserKey = "AppIsChildUser";
        private static readonly bool IsChildUserDefault = false;
        private const string TotalAccessKey = "AppTotalAccess";
        private static readonly bool TotalAccessDefault = true;
        private const string UsernameKey = "AppUsername";
        private static readonly string UsernameDefault = string.Empty;
        private const string TransactionValueLimitKey = "AppTransactionValueLimit";
        private static readonly decimal TransactionValueLimitDefault = 0m;
        private const string TransactionLimitKey = "AppTransactionLimit";
        private static readonly int TransactionLimitDefault = 0;
        private const string UserFirstnameKey = "AppUserFirstname";
        private static readonly string UserFirstnameDefault = string.Empty;
        private const string UserLastnameKey = "AppUserLastname";
        private static readonly string UserLastnameDefault = string.Empty;
        private const string UserPasswordKey = "AppUserPassword";
        private static readonly string UserPasswordDefault = string.Empty;
        private const string UserEmailKey = "AppUserEmail";
        private static readonly string UserEmailDefault = string.Empty;
        private const string UserPhoneKey = "AppUserPhone";
        private static readonly string UserPhoneDefault = string.Empty;
        private const string UserDocumentKey = "AppUserDocument";
        private static readonly string UserDocumentDefault = string.Empty;
        private const string UserPictureKey = "AppUserPicture";
        private static readonly string UserPictureDefault = string.Empty;
        private const string UserTypeKey = "AppUserType";
        private static readonly int UserTypeDefault = 0;
        private const string UserRemainingEstablishmentIndicationsKey = "AppUserRemainingEstablishmentIndications";
        private static readonly int UserRemainingEstablishmentIndicationsDefault = 0;
        private const string UserRemainingCustomerIndicationsKey = "AppUserRemainingCustomerIndications";
        private static readonly int UserRemainingCustomerIndicationsDefault = 0;
        private const string IsLoggedKey = "AppIsLogged";
        private static readonly bool IsLoggedDefault = false;
        private const string UserBalanceKey = "AppUserBalance";
        private static readonly decimal UserBalanceDefault = 0m;
        private const string UserCashbackKey = "AppUserCashback";
        private static readonly decimal UserCashbackDefault = 0m;
        private const string UserPointsKey = "AppUserPoints";
        private static readonly int UserPointsDefault = 0;
        private const string UserPointsValueKey = "AppUserPointsValue";
        private static readonly decimal UserPointsValueDefault = 0m;
        private const string UserTotalTransactionKey = "AppUserTotalTransaction";
        private static readonly decimal UserTotalTransactionDefault = 0m;
        private const string BusinessActivityFilterKey = "AppBusinessActivityFilter";
        private static readonly int BusinessActivityFilterDefault = 0;
        private const string BusinessActivityFilterNameKey = "AppBusinessActivityFilterName";
        private static readonly string BusinessActivityFilterNameDefault = string.Empty;
        private const string CurrentViewKey = "CurrentView";
        private static readonly int CurrentViewDefault = (int)ViewPages.Establishments;
        private const string BalancePeriodSelectedKey = "AppBalancePeriodSelected";
        private static readonly string BalancePeriodSelectedDefault = "year";
        private const string ShareLinkKey = "ShareLink";
        private static readonly string ShareLinkDefault = "https://www.nivel.com.br";
        private const string IndicateCustomerLinkKey = "IndicateCustomerLink";
        private static readonly string IndicateCustomerLinkDefault = "https://www.nivel.com.br";
        private const string IndicateEstablishmentLinkKey = "IndicateEstablishmentLink";
        private static readonly string IndicateEstablishmentLinkDefault = "https://www.nivel.com.br";
        private const string DeviceWidthKey = "DeviceWidth";
        private static readonly int DeviceWidthDefault = 0;
        private const string DeviceHeightKey = "DeviceHeight";
        private static readonly int DeviceHeightDefault = 0;
        private const string DeviceVersionCodeKey = "DeviceVersionCode";
        private static readonly int DeviceVersionCodeDefault = 0;
        private const string GenerateQrKey = "GenerateQr";
        private static readonly bool GenerateQrDefault = true;
        private const string CityFilterKey = "AppCityFilter";
        private static readonly int CityFilterDefault = 0;
        private const string CityFilterNameKey = "AppCityFilterName";
        private static readonly string CityFilterNameDefault = string.Empty;
        private const string UserLatitudeKey = "AppUserLatitude";
        private static readonly double UserLatitudeDefault = 0D;
        private const string UserLongitudeKey = "AppUserLongitude";
        private static readonly double UserLongitudeDefault = 0D;
        private const string EmailBeneficiaryKey = "TransactionEmailBeneficiary";
        private static readonly string EmailBeneficiaryDefault = string.Empty;
        private const string UserHasAddressKey = "UserHasAddress";
        private static readonly bool UserHasAddressDefault = true;
        private const string UserIsExecutiveCommissionerKey = "UserIsExecutiveCommissioner";
        private static readonly bool UserIsExecutiveCommissionerDefault = false;
        private const string VerifiedUserKey = "VerifiedUser";
        private static readonly bool VerifiedUserDefault = false;
        private const string SelectedCultureKey = "SelectedCulture";
        private static readonly string SelectedCultureDefault = "";

        public const string PositionListActiveImage = "positionlist.png";
        public const string PositionListInactiveImage = "_positionlist.png";
        public const string EstablishmentListActiveImage = "establishmentlist.png";
        public const string EstablishmentListInactiveImage = "_establishmentlist.png";
        public const string FavoritesListActiveImage = "favorite.png";
        public const string FavoritesListInactiveImage = "favoritelist.png";
        public const string OffersListActiveImage = "offer.png";
        public const string OffersListInactiveImage = "_offer.png";
        public const string HotOffersListActiveImage = "hotoffer.png";
        public const string HotOffersListInactiveImage = "_hotoffer.png";
        public const string TourismListActiveImage = "tourism.png";
        public const string TourismListInactiveImage = "_tourism.png";
        public const string UrlForgotPassword = "https://www.nivel.com.br/painel/conta/recuperarsenha";

        #endregion Setting Constants

        #region Methods

        public static void Logout()
        {
            IsLogged = false;
            UserId = 0;
            Username = string.Empty;
            UserDocument = string.Empty;
            UserBalance = 0m;
            UserTotalTransaction = 0m;
            UserCashback = 0m;
            UserPoints = 0;
            UserPointsValue = 0m;
        }

        #endregion Methods

        public static string GeneralSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(SettingsKey, SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(SettingsKey, value);
            }
        }

        public static string CurrentPage
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(CurrentPageKey, CurrentPageDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(CurrentPageKey, value);
            }
        }

        public static string DeviceToken
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(DeviceTokenKey, DeviceTokenDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(DeviceTokenKey, value);
            }
        }

        public static string AppToken
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(AppTokenKey, AppTokenDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(AppTokenKey, value);
            }
        }

        public static string AppWsUrl
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(AppWsUrlKey, AppWsUrlDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(AppWsUrlKey, value);
            }
        }

        public static int UserId
        {
            get
            {
                return AppSettings.GetValueOrDefault<int>(UserIdKey, UserIdDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<int>(UserIdKey, value);
            }
        }

        public static int UserChildId
        {
            get
            {
                return AppSettings.GetValueOrDefault<int>(UserChildIdKey, UserChildIdDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<int>(UserChildIdKey, value);
            }
        }

        public static bool IsChildUser
        {
            get
            {
                return AppSettings.GetValueOrDefault<bool>(IsChildUserKey, IsChildUserDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<bool>(IsChildUserKey, value);
            }
        }

        public static bool TotalAccess
        {
            get
            {
                return AppSettings.GetValueOrDefault<bool>(TotalAccessKey, TotalAccessDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<bool>(TotalAccessKey, value);
            }
        }

        public static string Username
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(UsernameKey, UsernameDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(UsernameKey, value);
            }
        }

        public static string UserDocument
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(UserDocumentKey, UserDocumentDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(UserDocumentKey, value);
            }
        }

        public static string UserPicture
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(UserPictureKey, UserPictureDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(UserPictureKey, value);
            }
        }

        public static string UserFirstname
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(UserFirstnameKey, UserFirstnameDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(UserFirstnameKey, value);
            }
        }

        public static string UserLastname
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(UserLastnameKey, UserLastnameDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(UserLastnameKey, value);
            }
        }

        public static string UserPassword
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(UserPasswordKey, UserPasswordDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(UserPasswordKey, value);
            }
        }

        public static string UserEmail
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(UserEmailKey, UserEmailDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(UserEmailKey, value);
            }
        }

        public static string UserPhone
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(UserPhoneKey, UserPhoneDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(UserPhoneKey, value);
            }
        }

        public static int UserRemainingCustomerIndications
        {
            get
            {
                return AppSettings.GetValueOrDefault<int>(UserRemainingCustomerIndicationsKey, UserRemainingCustomerIndicationsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<int>(UserRemainingCustomerIndicationsKey, value);
            }
        }

        public static int UserRemainingEstablishmentIndications
        {
            get
            {
                return AppSettings.GetValueOrDefault<int>(UserRemainingEstablishmentIndicationsKey, UserRemainingEstablishmentIndicationsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<int>(UserRemainingEstablishmentIndicationsKey, value);
            }
        }

        public static int TransactionLimit
        {
            get
            {
                return AppSettings.GetValueOrDefault<int>(TransactionLimitKey, TransactionLimitDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<int>(TransactionLimitKey, value);
            }
        }

        public static bool IsLogged
        {
            get
            {
                return AppSettings.GetValueOrDefault<bool>(IsLoggedKey, IsLoggedDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<bool>(IsLoggedKey, value);
            }
        }

        public static decimal UserCashback
        {
            get
            {
                return AppSettings.GetValueOrDefault<decimal>(UserCashbackKey, UserCashbackDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<decimal>(UserCashbackKey, value);
            }
        }

        public static decimal TransactionValueLimit
        {
            get
            {
                return AppSettings.GetValueOrDefault<decimal>(TransactionValueLimitKey, TransactionValueLimitDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<decimal>(TransactionValueLimitKey, value);
            }
        }

        public static decimal UserPointsValue
        {
            get
            {
                return AppSettings.GetValueOrDefault<decimal>(UserPointsValueKey, UserPointsValueDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<decimal>(UserPointsValueKey, value);
            }
        }

        public static decimal UserBalance
        {
            get
            {
                return AppSettings.GetValueOrDefault<decimal>(UserBalanceKey, UserBalanceDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<decimal>(UserBalanceKey, value);
            }
        }

        public static int UserPoints
        {
            get
            {
                return AppSettings.GetValueOrDefault<int>(UserPointsKey, UserPointsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<int>(UserPointsKey, value);
            }
        }

        public static decimal UserTotalTransaction
        {
            get
            {
                return AppSettings.GetValueOrDefault<decimal>(UserTotalTransactionKey, UserTotalTransactionDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<decimal>(UserTotalTransactionKey, value);
            }
        }

        public static string BusinessActivityFilterName
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(BusinessActivityFilterNameKey, BusinessActivityFilterNameDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(BusinessActivityFilterNameKey, value);
            }
        }

        public static int BusinessActivityFilter
        {
            get
            {
                return AppSettings.GetValueOrDefault<int>(BusinessActivityFilterKey, BusinessActivityFilterDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<int>(BusinessActivityFilterKey, value);
            }
        }

        public static bool UserHasAddress
        {
            get
            {
                return AppSettings.GetValueOrDefault<bool>(UserHasAddressKey, UserHasAddressDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<bool>(UserHasAddressKey, value);
            }
        }

        public static bool UserIsExecutiveCommissioner
        {
            get
            {
                return AppSettings.GetValueOrDefault<bool>(UserIsExecutiveCommissionerKey, UserIsExecutiveCommissionerDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<bool>(UserIsExecutiveCommissionerKey, value);
            }
        }

        public static bool VerifiedUser
        {
            get
            {
                return AppSettings.GetValueOrDefault<bool>(VerifiedUserKey, VerifiedUserDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<bool>(VerifiedUserKey, value);
            }
        }

        public static int CurrentView
        {
            get
            {
                return AppSettings.GetValueOrDefault<int>(CurrentViewKey, CurrentViewDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<int>(CurrentViewKey, value);
            }
        }

        public static string BalancePeriodSelected
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(BalancePeriodSelectedKey, BalancePeriodSelectedDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(BalancePeriodSelectedKey, value);
            }
        }

        public static string ShareLink
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(ShareLinkKey, ShareLinkDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(ShareLinkKey, value);
            }
        }

        public static string IndicateCustomerLink
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(IndicateCustomerLinkKey, IndicateCustomerLinkDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(IndicateCustomerLinkKey, value);
            }
        }

        public static string IndicateEstablishmentLink
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(IndicateEstablishmentLinkKey, IndicateEstablishmentLinkDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(IndicateEstablishmentLinkKey, value);
            }
        }

        public static int DeviceWidth
        {
            get
            {
                return AppSettings.GetValueOrDefault<int>(DeviceWidthKey, DeviceWidthDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<int>(DeviceWidthKey, value);
            }
        }

        public static int DeviceHeight
        {
            get
            {
                return AppSettings.GetValueOrDefault<int>(DeviceHeightKey, DeviceHeightDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<int>(DeviceHeightKey, value);
            }
        }

        public static int DeviceVersionCode
        {
            get
            {
                return AppSettings.GetValueOrDefault<int>(DeviceVersionCodeKey, DeviceVersionCodeDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<int>(DeviceVersionCodeKey, value);
            }
        }

        public static int UserType
        {
            get
            {
                return AppSettings.GetValueOrDefault<int>(UserTypeKey, UserTypeDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<int>(UserTypeKey, value);
            }
        }

        public static bool GenerateQr
        {
            get
            {
                return AppSettings.GetValueOrDefault<bool>(GenerateQrKey, GenerateQrDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<bool>(GenerateQrKey, value);
            }
        }

        public static string CityFilterName
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(CityFilterNameKey, CityFilterNameDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(CityFilterNameKey, value);
            }
        }

        public static int CityFilter
        {
            get
            {
                return AppSettings.GetValueOrDefault<int>(CityFilterKey, CityFilterDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<int>(CityFilterKey, value);
            }
        }

        public static double UserLatitude
        {
            get
            {
                return AppSettings.GetValueOrDefault<double>(UserLatitudeKey, UserLatitudeDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<double>(UserLatitudeKey, value);
            }
        }

        public static double UserLongitude
        {
            get
            {
                return AppSettings.GetValueOrDefault<double>(UserLongitudeKey, UserLongitudeDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<double>(UserLongitudeKey, value);
            }
        }

        public static string EmailBeneficiary
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(EmailBeneficiaryKey, EmailBeneficiaryDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(EmailBeneficiaryKey, value);
            }
        }

        public static string SelectedCulture
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(SelectedCultureKey, SelectedCultureDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(SelectedCultureKey, value);
            }
        }
    }
}