using Newtonsoft.Json.Linq;
using PushNotification.Plugin;
using PushNotification.Plugin.Abstractions;
using System.Diagnostics;

namespace AppNivel.Helpers
{
    //Class to handle push notifications listens to events such as registration, unregistration, message arrival and errors.
    public class CrossPushNotificationListener : IPushNotificationListener
    {
        public void OnMessage(JObject values, DeviceType deviceType)
        {
            Debug.WriteLine("Message Arrived");
        }

        public void OnRegistered(string Token, DeviceType deviceType)
        {
            Debug.WriteLine($"Push Notification - Device Registered - Token : {Token}");
            Settings.DeviceToken = Token;
        }

        public void OnUnregistered(DeviceType deviceType)
        {
            Debug.WriteLine("Push Notification - Device Unnregistered");
        }

        public void OnError(string message, DeviceType deviceType)
        {
            Debug.WriteLine($"Push notification error - {message}");
        }

        public bool ShouldShowNotification()
        {
            return true;
        }
    }
}