﻿using System;
using System.Collections.Generic;

namespace AppNivel.Models
{
    #region Recarga

    //No metodo de recarga enviar no json os dados na classe
    public class WsRvRecargaCelular
    {
        public string CodigoOperadora { get; set; }
        public string CodigoProduto { get; set; }
        public decimal ValorRecarga { get; set; }
        public string CpfCnpj { get; set; }
        public string Ddd { get; set; }
        public string Telefone { get; set; }
        public string CodigoAssinante { get; set; }
        public string UfTerminal { get; set; }
    }

    //Retorno feito pela classe
    public class WSRecargaCelular : BaseModels
    {
        public string MsgErro { get; set; }
        public bool TipoRecargaOnline { get; set; }
        public DateTime DataHora { get; set; }
        public string NomeLoja { get; set; }
        public string CodigoCompraOnline { get; set; }
        public string NomeOperadora { get; set; }
        public decimal ValorRecarga { get; set; }
        public string Fone { get; set; }
        public string NsuTransacao { get; set; }
        public string MensagemPromo { get; set; }
        public string PIN { get; set; }
        public string Lote { get; set; }
        public string Serie { get; set; }
    }

    public class WsRvCodOperadoraLista
    {
        public string CodOperadora { get; set; }
        public string userDoc { get; set; }
    }

    public class RetornoMetodoOperadorasProdutos : BaseModels
    {
        public List<OperadoraRecarga> ListaOperadora { get; set; }
        public string TelefoneCadastro { get; set; }
    }

    public class OperadoraRecarga
    {
        public string Codigo { get; set; }
        public string Nome { get; set; }
        public string TipoOp { get; set; }
        public bool OperadoraTv { get; set; }

        //public List<string> Estados { get; set; }
        public List<ProdutoOperadora> Produtos { get; set; }
    }

    public class ProdutoOperadora
    {
        public string Codigo { get; set; }
        public string Nome { get; set; }
        public string ModeloRecargaNome { get; set; }
        public bool ModeloRecargaPin { get; set; }
        public decimal PrecoCompra { get; set; }
        public decimal PrecoVenda { get; set; }
        public string Validade { get; set; }
        public decimal ValorMinimo { get; set; }
        public decimal ValorMaximo { get; set; }
        public List<int> DddsProdutoOnline { get; set; }
    }

    #endregion Recarga

    public enum DeviceSystem
    {
        Android = 1,
        IOS = 2,
        Windows = 3,
        Web = 4,
        Integration = 5,
    }

    public enum VerificationCodeType
    {
        Sms = 1,
        Imei = 2,
        Cartao = 3,
        QrCode = 4,
        Email = 5
    }

    public enum ContactType
    {
        Email = 1,
        Telefone = 2,
        Celular = 3,
        Fax = 4,
    }

    public enum WSTransactionType
    {
        Credit = 1,
        Debit = 2,
        Transfer = 3,
        Buy = 4,
        Redeem = 5,
        Commission = 6,
        RvRecarga = 99
    }

    public enum BuyerProfileEnum
    {
        Parceiro = 1,
        CNA = 2,
        Normal = 3,
    }

    public enum WSRoleId
    {
        Customer = 1,
        Establishment = 2,
        Administrator = 3,
    }

    public enum RequestType
    {
        EstablishmentList = 1,
        Login = 2,
        BalanceAmmount = 3,
        CustomerRegister = 4,
        Transaction = 5,
        FinancialBalanceAmmount = 6,
        Favorite = 7,
        QrCode = 8,
        BusinessActivityList = 9,
        AppVersion = 10,
        UpdateCustomer = 11,
        GetIndications = 12,
        IndicatePerson = 13,
        MessageToDevice = 14,
        ComissionBalance = 15,
        AcceptCommissioner = 16,
        EstimateCommission = 17,
        GetPerson = 18,
        RegisterCustomerWithCommissioner = 19,
        Logout = 20,
        SendPush = 21,
        TransactionConfirm = 22,
        CityList = 23,
        ClosestCity = 24,
        StateList = 25,
        PersonExists = 26,
        SpecialList = 27,
        RedeemRequest = 28,
        PreviousBalance = 29,
        UpdateCustomerAddress = 30,
        AcceptTerms = 31,
        EstablishmentNativeList = 32,
        BalanceAmmountPaginated = 33,
        IsExecutiveCommissioner = 34,
        SearchPostalCode = 35,

        //Recarga
        CreditRecharge = 36,

        PhoneRechargeGetOperatorProducts = 37
        //Recarga
    }

    public enum PersonTypeEnum
    {
        Customer = 1,
        Establishment = 2,
    }

    public enum WSError
    {
        Undentified = 0,
        BadRequest = 1,
        InvalidJsonContent = 2,
        InvalidCpfCnpj = 3,
        LoginFailure = 4,
        InvalidToken = 5,
        CriterionRequired = 6,
        DeviceCodeRequired = 7,
        ManyErrors = 8,
        AlreadyRegistered = 9,
        InvalidEmail = 10,
        WeakPassword = 11,
        CpfCnpjAlreadyRegistered = 12,
        UpdateRequired = 13,
        InvalidSender = 14,
        InvalidBeneficiary = 15,
        InvalidCommissioner = 16,
        InvalidTransactionValues = 17,
        InvalidPointPercentual = 18,
        InvalidCashbackPercentual = 19,
        InvalidTransactionDate = 20,
        InvalidVerificationCode = 21,
        InvalidCancelMotive = 22,
        InvalidTransactionType = 23,
        InvalidTaxPercentual = 24,
        InvalidTransactionParent = 25,
        WrongComissionerId = 26,
        IdenticalPreviousOne = 27,
        IdenticalSenderAndBeneficiary = 28,
        InvalidIpAddress = 29,
        MaxValueTransferByDayExceeded = 30,
        QtdTransferByDayExceeded = 31,
        TransactionInCourse = 32,
        NotAuthorized = 33,
        InvalidCustomer = 34,
        InvalidEstablishment = 35,
        SumOfBonusReach100Percent = 36,
        TransactionNotFound = 37,
        TransactionFromAnotherSource = 38,
        TransactionNotPending = 39,
        ForbiddenOperation = 40,
        SpendPermitedOnlyWithEstablishment = 41,
        FileError = 42,
        InvalidTelefone = 43,
        FirstNameRequired = 44,
        LastNameRequired = 45,
        PersonNotFound = 46,
        InvalidFirstName = 47,
        InvalidLastName = 48,
        InvalidPersonType = 49,
        NoIndicationsRemaining = 50,
        EmailInvitation = 51,
        NotBelongToCommissioner = 52,
        LogoutFailure = 53,
        InvalidUser = 54,
        TransactionDocumentRequired = 55,
        TransactionLimitReached = 56,
        InvalidCommissionValue = 57,
        InvalidTransactionReferred = 58,
        InvalidRedeemValue = 59,
        InvalidHolder = 60,
        InvalidBank = 61,
        InvalidAgency = 62,
        InvalidAccount = 63,
        RedeemTransactionError = 64,
        RedeemNotFound = 65,
        MoreThan1ToBonify = 66,
        InvalidPostalCode = 67,

        //Recarga
        InvalidOffer = 68,

        VerificationCodeEmpty = 69,
        IdenticalBonusPaymentPreviousOne = 70,
        ReachedLimitTransactionsExceeded = 71,
        InvalidOperator = 72,
        RvRechargeError = 73,
        InvalidOperatorProducts = 74,
        InvalidRvRechargeValue = 75,
        RvRechargeConfirmError = 76,
        //\\Recarga
    }

    public enum WSTransactionStatus
    {
        Received = 1,
        Processing = 2,
        WaitingVerificationCode = 3,
        Authorized = 4,
        Canceled = 5,
        NotAuthorized = 6,
    }

    public class BaseModels
    {
        public bool Success { get; set; }
        public List<int> Error { get; set; }
    }

    /// <summary>
    /// The model for any requests to Web service
    /// </summary>
    public class RequestModel : BaseModels
    {
        public string ErrorText { get; set; }
        public int RequestType { get; set; }
        public string Token { get; set; }
        public string Json { get; set; }
        public string CurrentCultureName { get; set; }
    }

    /// <summary>
    /// The model for the response from Web service
    /// </summary>
    public class RequestModelResult
    {
        public string HandleResult { get; set; }
    }

    public class TokenModel
    {
        public int AppId { get; set; }
        public long Date { get; set; }
    }

    /// <summary>
    /// Return of RequestType 3
    /// </summary>
    public class Balance : BaseModels
    {
        public decimal TotalTransactions { get; set; }
        public decimal TotalCashBack { get; set; }
        public decimal TotalPoints { get; set; }
        public decimal TotalPointsValue { get; set; }
        public decimal TotalBalance { get; set; }
        public List<WSTransaction> Movement { get; set; }
        public bool UserHasAddress { get; set; }
        public bool Paginate { get; set; }
        public int TotalResults { get; set; }
        public int TotalPages { get; set; }
        public int Page { get; set; }
        public int ItensPerPage { get; set; }
    }

    public class FinancialBalance : BaseModels
    {
        public decimal TotalValue { get; set; }
        public List<WSFinancialTransaction> Movement { get; set; }
    }

    public class Favorite : BaseModels
    {
        public int PersonId { get; set; }
        public int PersonEstablishmentId { get; set; }
        public bool IsFavorite { get; set; }
    }

    public class WSTransaction : BaseModels
    {
        public int Id { get; set; }
        public string CpfCnpjSender { get; set; }
        public int PersonSenderId { get; set; }
        public string PersonSenderName { get; set; }
        public string PersonSenderNameDisplay { get; set; }
        public string PersonSenderCpfCnpj { get; set; }
        public string CpfCnpjBeneficiary { get; set; }
        public int PersonBeneficiaryId { get; set; }
        public string PersonBeneficiaryName { get; set; }
        public string PersonBeneficiaryNameDisplay { get; set; }
        public string PersonBeneficiaryCpfCnpj { get; set; }
        public int CommissionerId { get; set; }
        public string CommissionerName { get; set; }
        public string CommissionerCpfCnpj { get; set; }
        public decimal TransactionPointsValue { get; set; }
        public decimal TransactionPoints { get; set; }
        public decimal TransactionValue { get; set; }
        public decimal TransactionCashback { get; set; }
        public decimal TransactionTotalCash { get; set; }
        public decimal PointPercentual { get; set; }
        public decimal CashbackPercentual { get; set; }
        public long TransactionDate { get; set; }
        public string Token { get; set; }
        public bool Accountable { get; set; }
        public string VerificationCode { get; set; }
        public string Ip { get; set; }
        public string CancelMotive { get; set; }
        public int TransactionTypeId { get; set; }
        public string TransactionType { get; set; }
        public decimal TaxPercentual { get; set; }
        public int? TransactionId { get; set; }
        public int TransactionStatus { get; set; }
        public bool IsCredit { get; set; }
        public string TextColor { get; set; }
        public bool NewBeneficiary { get; set; }
        public string BeneficiaryEmail { get; set; }
        public bool IsChildRewarding { get; set; }
        public int ChildUserId { get; set; }
        public string TransactionDocument { get; set; }
        public int OperationId { get; set; }
        public List<WSCommission> Commission { get; set; }
        public WSTransaction TransactionOrigin { get; set; }
        public decimal PointsValueMoney { get; set; }
        public decimal TaxaValueMoney { get; set; }
        public decimal TotalBonification { get; set; }
        public decimal TotalBonificationWithoutTax { get; set; }
        public bool ShowInExtrato { get; set; }
        public bool HaveComission { get; set; }
        public string MailBeneficiary { get; set; }
        public bool MakeSenderBeneficiaryCommissioner { get; set; }
        public string TransactionText { get; set; }
    }

    public class WSFinancialTransaction
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public int TransactionId { get; set; }
        public bool TransactionType { get; set; }
        public decimal Value { get; set; }
        public string Token { get; set; }
        public bool Status { get; set; }
        public int ErrorCode { get; set; }
        public string Ip { get; set; }
        public string CancelMotive { get; set; }
        public long Date { get; set; }
    }

    /// <summary>
    /// Get the balance of a person account (RequestType 3 and 6)
    /// </summary>
    public class GetBalanceAmmount
    {
        public int PersonId { get; set; }
        public long InitialDate { get; set; }
        public long EndDate { get; set; }
        public int Take { get; set; }
        public int TransactionTypeId { get; set; }
        public string Device { get; set; }
        public int Page { get; set; }
        public int ItensPerPage { get; set; }
    }

    /// <summary>
    /// Request mode that returns the user data when successfull (RequestType 2)
    /// </summary>
    public class Login
    {
        public string CpfCnpj { get; set; }
        public string Password { get; set; }
        public string Device { get; set; }
        public string UserEmail { get; set; }
        public bool LoginAsUser { get; set; }
        public List<int> Error { get; set; }
    }

    public class GetCity
    {
        public string StateCode { get; set; }
        public string SearchText { get; set; }
    }

    /// <summary>
    /// Request mode that returns the EstablishmentList object (RequestType 1)
    /// </summary>
    public class GetEstablishment
    {
        public int CustomerId { get; set; }
        public int BusinessActivityId { get; set; }
        public int Id { get; set; }
        public int StateId { get; set; }
        public int CityId { get; set; }
        public string Search { get; set; }
        public int Page { get; set; }
        public int ItensByPage { get; set; }
        public bool IsFavorite { get; set; }
        public bool IsOffer { get; set; }
        public double UserLatitude { get; set; }
        public double UserLongitude { get; set; }
        public bool IsHotOffer { get; set; }
        public bool Promote { get; set; } = false;
    }

    /// <summary>
    /// Object that contains a list of stablishments (Return of RequestType 1)
    /// </summary>
    public class EstablishmentList : BaseModels
    {
        public string ErrorText { get; set; }
        public int TotalResults { get; set; }
        public int TotalPages { get; set; }
        public int Page { get; set; }
        public int ItensByPage { get; set; }
        public List<WSEstablishment> List { get; set; }
    }

    /// <summary>
    /// Object that contains a list of customers
    /// </summary>
    public class CustomerList : BaseModels
    {
        public string ErrorText { get; set; }
        public int TotalResults { get; set; }
        public int TotalPages { get; set; }
        public int Page { get; set; }
        public int ItensByPage { get; set; }
        public List<WSCustomer> List { get; set; }
    }

    public class WSQrCode
    {
        public int PersonId { get; set; }
        public string Code { get; set; }
        public string QrCodePath { get; set; }
        public string EstablishmentLogoPath { get; set; }
        public long Remaining { get; set; }
    }

    /// <summary>
    /// Return of RequestType 2
    /// </summary>
    public class WSPerson
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public string CpfCnpj { get; set; }
        public string RgIe { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public long Birthdate { get; set; }
        public string Picture { get; set; }
        public string BackgroundImage { get; set; }
        public int PersonTypeId { get; set; }
        public string PersonType { get; set; }
        public int PersonId { get; set; }
        public bool PersonAccepted { get; set; }
        public bool IsCommissioned { get; set; }
        public long Date { get; set; }
        public bool Status { get; set; }
        public List<WSAddress> Address { get; set; }
        public int BuyerProfileId { get; set; }
        public WSBuyerProfile BuyerProfile { get; set; }
        public List<WSContact> Contact { get; set; }
        public List<int> Error { get; set; }
        public bool IsConfirmed { get; set; }
        public int RemainingIndicationEstablishment { get; set; }
        public int RemainingIndicationCustomer { get; set; }
        public string EmailIndication { get; set; }
        public string IndicationLink { get; set; }
        public string CustomerIndicationLink { get; set; }
        public string EstablishmentIndicationLink { get; set; }
        public bool IsExecutiveCommissioner { get; set; }
        public bool IsCommissionedFake { get; set; }
    }

    public class WSEstablishment : WSPerson
    {
        public int BusinessActivityId { get; set; }
        public string BusinessActivity { get; set; }
        public List<WSConfiguration> Configuration { get; set; }
        public bool Favorite { get; set; }
        public string Discount { get; set; }
        public decimal DistanceToUser { get; set; }
        public decimal DiscountOrder { get; set; }
        public string DescriptionLink { get; set; }
        public List<WSPaymentForm> PaymentForms { get; set; }
        public string OpeningHours { get; set; }
        public List<PersonConfigDaysList> SuperCashbacks { get; set; }
        public bool IsHotDiscount { get; set; }
        public long TimeLeft { get; set; }
        public List<WSImage> Images { get; set; }
    }

    public class WSImage
    {
        public string Path { get; set; }
        public string Name { get; set; }
    }

    public class PersonConfigDaysList
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public string Description { get; set; }
        public decimal CashBack { get; set; }
        public decimal Points { get; set; }
        public int? DayOfWeek { get; set; }
        public string DayOfWeekName { get; set; }
        public string DateStart { get; set; }
        public string DateEnd { get; set; }
        public bool Status { get; set; }
        public bool DayClosed { get; set; }
        public string Date { get; set; }
        public bool OneBonusLock { get; set; }
        public decimal TotalBonus { get; set; }
        public bool Supercashback { get; set; }
        public bool Recurrent { get; set; }
        public string HourStart { get; set; }
        public string HourEnd { get; set; }
    }

    public class WSPaymentForm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
    }

    public class WSCustomer : WSPerson
    {
        public bool LoggedAsUser { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
        public decimal TransactionValueLimit { get; set; }
        public int TransactionLimit { get; set; }
        public bool TotalAccess { get; set; }
        public string RegistrationCity { get; set; }
        public string RegistrationPostalCode { get; set; }
    }

    public class WSConfiguration
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public string Token { get; set; }
        public decimal CashBack { get; set; }
        public decimal Points { get; set; }
        public long Date { get; set; }
        public long DateEnd { get; set; }
        public bool Status { get; set; }
        public int? DayOfWeek { get; set; }
        public bool DayClosed { get; set; }
    }

    public class WSBuyerProfile
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal TaxRate { get; set; }
    }

    public class WSAddress
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public string Name { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string Number { get; set; }
        public string PostalCode { get; set; }
        public int CityId { get; set; }
        public string City { get; set; }
        public int StateId { get; set; }
        public string State { get; set; }
        public int CountryId { get; set; }
        public string Country { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public bool Main { get; set; }
        public long Date { get; set; }
    }

    public class WSContact
    {
        public int Id { get; set; }
        public int ContactTypeId { get; set; }
        public string ContactType { get; set; }
        public int PersonId { get; set; }
        public string ContactValue { get; set; }
        public bool Main { get; set; }
    }

    public class WSBusinessActivity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class BusinessActivityList : BaseModels
    {
        public List<WSBusinessActivity> List { get; set; }
    }

    public class BalanceFilterObj
    {
        public string Period { get; set; }
        public string Text { get; set; }
        public bool IsSelected { get; set; }
    }

    public class WSAppVersion
    {
        public string Version { get; set; }
        public bool Notify { get; set; }
        public string Message { get; set; }
        public long VersionDate { get; set; }
        public bool Mandatory { get; set; }
        public string AppStoreLink { get; set; }
        public string GooglePlayStoreLink { get; set; }
        public string WindowsStoreLink { get; set; }
    }

    public class RemainingIndication : BaseModels
    {
        public int PersonId { get; set; }
        public int RemainingIndicationCustomer { get; set; }
        public int RemainingIndicationEstablishment { get; set; }
        public string IndicationLink { get; set; }
        public string CustomerIndicationLink { get; set; }
        public string EstablishmentIndicationLink { get; set; }
    }

    public class WSIndication : BaseModels
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public int PersonIndicatedId { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public bool LinkClicked { get; set; }
        public long Date { get; set; }
        public int PersonTypeId { get; set; }
    }

    public class GetCommissionBalance
    {
        public int PersonId { get; set; }
        public long InitialDate { get; set; }
        public long EndDate { get; set; }
        public int Take { get; set; }
    }

    public class CommissionBalance : BaseModels
    {
        public decimal TotalCustomer { get; set; }
        public decimal TotalEstablishment { get; set; }
        public decimal TotalComission { get; set; }
        public List<WSCommission> Movement { get; set; }
    }

    public class WSCommission
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public int TransactionId { get; set; }
        public decimal CommissionPercentual { get; set; }
        public decimal CommissionValue { get; set; }
        public long CommissionDate { get; set; }
        public bool Paid { get; set; }
    }

    public class AcceptCommissioner : BaseModels
    {
        public int PersonId { get; set; }
        public int CommissionerId { get; set; }
        public string CommissionerCpfCnpj { get; set; }
    }

    public class CityList : BaseModels
    {
        public List<WSCity> List { get; set; }
    }

    public class WSCity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int StateId { get; set; }
    }

    public class WSSendPush : BaseModels
    {
        public int PersonId { get; set; }
        public string Message { get; set; }
        public List<GcmResponse> Response { get; set; }
    }

    public class GcmResponse
    {
        public string multicast_id { get; set; }
        public int success { get; set; }
        public int failure { get; set; }
        public string canonical_ids { get; set; }
    }

    public class WSState
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class StateList : BaseModels
    {
        public List<WSState> List { get; set; }
    }

    public class TransactionConfirm : BaseModels
    {
        public int TransactionId { get; set; }
        public string VerificationCode { get; set; }
        public int Status { get; set; }
        public FinancialBalance Balance { get; set; }
        public bool IsInternal { get; set; }
    }

    public class WsGenericText : BaseModels
    {
        public string Text { get; set; }
    }

    public class WsCEPConsulta : BaseModels
    {
        public string CEP { get; set; }
        public string TipoLogradouro { get; set; }
        public string Logradouro { get; set; }
        public string Bairro { get; set; }
        public string Estado { get; set; }
        public string Municipio { get; set; }
        public string CodIBGE { get; set; }
        public bool Distrito { get; set; }
        public string MunicipioPrincipal { get; set; }
    }
}