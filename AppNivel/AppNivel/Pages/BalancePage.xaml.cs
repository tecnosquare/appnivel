﻿using AppNivel.Helpers;
using AppNivel.Models;
using AppNivel.Resources;
using Plugin.Connectivity;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class BalancePage : ContentPage
    {
        private HttpClient _client = new HttpClient();
        private ObservableCollection<WSTransaction> _movements;
        private int _currentPage = 1;
        private bool _paginate = true;

        public BalancePage()
        {
            Settings.CurrentPage = "Balance";
            InitializeComponent();
            Settings.BalancePeriodSelected = "year";
        }

        protected override async void OnAppearing()
        {
            var factor = (Settings.DeviceWidth <= 480 && Settings.DeviceWidth > 0 ? 0.76 : 1);

            if (Settings.UserBalance > 999M)
            {
                UserBalance.FontSize = 40 * factor;
            }

            if (Settings.UserBalance > 9999M)
            {
                UserBalance.FontSize = 30 * factor;
            }

            if (Settings.UserBalance > 99999M)
            {
                UserBalance.FontSize = 20 * factor;
            }

            if (CrossConnectivity.Current.IsConnected)
            {
                var endDate = DateTime.Now;
                var initDate = endDate.AddYears(-1);

                switch (Settings.BalancePeriodSelected)
                {
                    case "3months":
                        initDate = endDate.AddMonths(-3);
                        break;

                    case "month":
                        initDate = endDate.AddMonths(-1);
                        break;

                    case "current":
                        initDate = new DateTime(endDate.Year, endDate.Month, 1);
                        break;

                    case "january":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 1);
                        break;

                    case "february":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 2);
                        break;

                    case "march":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 3);
                        break;

                    case "april":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 4);
                        break;

                    case "may":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 5);
                        break;

                    case "june":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 6);
                        break;

                    case "july":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 7);
                        break;

                    case "august":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 8);
                        break;

                    case "september":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 9);
                        break;

                    case "october":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 10);
                        break;

                    case "november":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 11);
                        break;

                    case "december":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 12);
                        break;
                }
                _currentPage = 1;
                var balance = await CustomerApi.GetBalancePaginated(_client, initDate.Ticks, endDate.Ticks, _currentPage);
                if (balance != null)
                {
                    Settings.UserBalance = balance.TotalBalance;
                    Settings.UserCashback = balance.TotalCashBack;
                    Settings.UserPoints = Convert.ToInt32(balance.TotalPoints);
                    Settings.UserPointsValue = balance.TotalPointsValue;
                    Settings.UserTotalTransaction = balance.TotalTransactions;
                    UserBalance.Text = Settings.UserBalance.ToString("C2", CultureInfo.CurrentCulture);
                    UserBalanceCashback.Text = "CASHBACK " + Settings.UserCashback.ToString("C2", CultureInfo.CurrentCulture);
                    UserBalancePoints.Text = "CASHCRED " + Settings.UserPointsValue.ToString("C2", CultureInfo.CurrentCulture);
                    LoadList(balance);
                }
                else
                {
                    await DisplayAlert("Atenção", "Não foi possível consultar seu saldo. Tente novamente mais tarde.", "Ok");
                    Settings.UserBalance = 0;
                    Settings.UserCashback = 0;
                    Settings.UserPoints = 0;
                    Settings.UserTotalTransaction = 0;
                }

                LoadingBalance.IsRunning = false;
                LoadingBalance.IsVisible = false;
                UserBalance.IsVisible = true;
                UserBalanceCashback.IsVisible = true;
                UserBalancePoints.IsVisible = true;
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
            base.OnAppearing();
        }

        private async void ExtractListView_OnItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            try
            {
                if (extractLoading.IsRunning)
                    return;
                if (!_paginate) return;

                var endDate = DateTime.Now;
                var initDate = endDate.AddYears(-1);

                switch (Settings.BalancePeriodSelected)
                {
                    case "3months":
                        initDate = endDate.AddMonths(-3);
                        break;

                    case "month":
                        initDate = endDate.AddMonths(-1);
                        break;

                    case "current":
                        initDate = new DateTime(endDate.Year, endDate.Month, 1);
                        break;

                    case "january":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 1);
                        break;

                    case "february":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 2);
                        break;

                    case "march":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 3);
                        break;

                    case "april":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 4);
                        break;

                    case "may":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 5);
                        break;

                    case "june":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 6);
                        break;

                    case "july":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 7);
                        break;

                    case "august":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 8);
                        break;

                    case "september":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 9);
                        break;

                    case "october":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 10);
                        break;

                    case "november":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 11);
                        break;

                    case "december":
                        Util.SetInitialAndEndDates(ref initDate, ref endDate, 12);
                        break;
                }

                if (e.Item as WSTransaction != _movements[_movements.Count - 1] ||
                    _movements.Count < 20) return;
                if (CrossConnectivity.Current.IsConnected)
                {
                    var loadingPage = new LoadingPage();
                    await Navigation.PushPopupAsync(loadingPage);
                    _currentPage += 1;
                    var balance = await CustomerApi.GetBalancePaginated(_client, initDate.Ticks, endDate.Ticks, _currentPage);
                    _paginate = balance.TotalPages > _currentPage;
                    await PopupNavigation.PopAllAsync();
                    LoadPageList(balance);
                }
                else
                {
                    Util.NotConnectedMessage(this);
                }
            }
            catch (Exception ex)
            {
                //resume...
            }
        }

        private async void ListView_OnRefreshing(object sender, EventArgs e)
        {
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    var endDate = DateTime.Now;
                    var initDate = endDate.AddYears(-1);

                    switch (Settings.BalancePeriodSelected)
                    {
                        case "3months":
                            initDate = endDate.AddMonths(-3);
                            break;

                        case "month":
                            initDate = endDate.AddMonths(-1);
                            break;

                        case "current":
                            initDate = new DateTime(endDate.Year, endDate.Month, 1);
                            break;

                        case "january":
                            Util.SetInitialAndEndDates(ref initDate, ref endDate, 1);
                            break;

                        case "february":
                            Util.SetInitialAndEndDates(ref initDate, ref endDate, 2);
                            break;

                        case "march":
                            Util.SetInitialAndEndDates(ref initDate, ref endDate, 3);
                            break;

                        case "april":
                            Util.SetInitialAndEndDates(ref initDate, ref endDate, 4);
                            break;

                        case "may":
                            Util.SetInitialAndEndDates(ref initDate, ref endDate, 5);
                            break;

                        case "june":
                            Util.SetInitialAndEndDates(ref initDate, ref endDate, 6);
                            break;

                        case "july":
                            Util.SetInitialAndEndDates(ref initDate, ref endDate, 7);
                            break;

                        case "august":
                            Util.SetInitialAndEndDates(ref initDate, ref endDate, 8);
                            break;

                        case "september":
                            Util.SetInitialAndEndDates(ref initDate, ref endDate, 9);
                            break;

                        case "october":
                            Util.SetInitialAndEndDates(ref initDate, ref endDate, 10);
                            break;

                        case "november":
                            Util.SetInitialAndEndDates(ref initDate, ref endDate, 11);
                            break;

                        case "december":
                            Util.SetInitialAndEndDates(ref initDate, ref endDate, 12);
                            break;
                    }

                    _currentPage = 1;
                    var balance = await CustomerApi.GetBalancePaginated(_client, initDate.Ticks, endDate.Ticks, _currentPage);
                    LoadList(balance);
                }
                else
                {
                    ShowList(false);
                    Util.NotConnectedMessage(this);
                }
                ExtractListView.EndRefresh();
            }
            catch (Exception ex)
            {
                ExtractListView.EndRefresh();
            }
        }

        private void ConfigTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Navigation.PopAsync(true);
        }

        private void ShowList(bool show = true)
        {
            ExtractListView.IsVisible = show;
        }

        private async void LoadPageList(Balance balance)
        {
            try
            {
                if (balance != null)
                {
                    if (balance.Error.Count == 0)
                    {
                        foreach (var movement in new ObservableCollection<WSTransaction>(balance.Movement))
                        {
                            _movements.Add(movement);
                        }
                        ExtractListView.ItemsSource = _movements;
                        ShowList();
                    }
                    else
                    {
                        ShowList(false);
                        await DisplayAlert("Atenção", "Sem movimentos cadastrados", "Ok");
                    }
                }
                else
                {
                    ShowList(false);
                    await DisplayAlert("Atenção", "Sem movimentos cadastrados", "Ok");
                }
            }
            catch (Exception ex)
            {
                //resume...
            }
        }

        private void LoadList(Balance balance)
        {
            if (balance != null)
            {
                if (balance.Error.Count == 0)
                {
                    _paginate = balance.TotalPages > 1 && _currentPage < balance.TotalPages;
                    _movements = new ObservableCollection<WSTransaction>(balance.Movement);
                    ExtractListView.ItemsSource = _movements;
                    TextExtract.IsVisible = !_movements.Any();
                    ShowList();
                }
                else
                {
                    ShowList(false);
                    TextExtract.IsVisible = true;
                }
            }
            else
            {
                ShowList(false);
                TextExtract.IsVisible = true;
            }
        }

        private void ExtractListView_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ExtractListView.SelectedItem = null;
        }

        private void ExtractListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            var movement = e.Item as WSTransaction;
            if (movement != null)
            {
                PopupNavigation.PushAsync(new BalanceDetailPage(movement));
            }
        }

        private void FilterBalanceTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Navigation.PushAsync(new BalanceFilter());
        }
    }
}