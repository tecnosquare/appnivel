﻿using AppNivel.GrialTheme.Views.Logins;
using AppNivel.Helpers;
using AppNivel.Resources;
using Plugin.Connectivity;
using Plugin.Share;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Version.Plugin;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class SettingsPage : ContentPage
    {
        private HttpClient _client = new HttpClient();

        public SettingsPage()
        {
            Settings.CurrentPage = "Settings";
            InitializeComponent();
            LineReward.IsVisible = PanelReward.IsVisible = Settings.UserType == 2;
            LineReceive.IsVisible = PanelReceive.IsVisible = Settings.UserType == 2;
        }

        protected override void OnAppearing()
        {
            //LabelRemainingIndicationsCustomer.Text = Settings.UserRemainingCustomerIndications.ToString();
            //LabelRemainingIndicationsEstablishment.Text = Settings.UserRemainingEstablishmentIndications.ToString();

            CreditRechargeStackLayout.IsVisible = CultureInfo.CurrentCulture.ToString() == "pt-BR";

            TxtUsername.Text = Settings.Username;
            AppVersionLabel.Text = "Versão " + CrossVersion.Current.Version;
            //if (!string.IsNullOrEmpty(Settings.UserPicture))
            //{
            //    ProfileImage.Source = "https://www.nivel.com.br/" + Settings.UserPicture;
            //}
            if (!Settings.TotalAccess && Settings.IsChildUser)
            {
                LineForgot.IsVisible = false;
                Forgot.IsVisible = false;
                UsernameTgr.Tapped -= Username_OnTapped;
                UsernameTgr.Tapped += UsernameTgr_Tapped;
            }

            if (Settings.UserIsExecutiveCommissioner)
            {
                MenuWannaBeExecutive.IsVisible = false;
                WannaBeExecutiveLine.IsVisible = false;
                TransferLine.IsVisible = true;
                Transfer.IsVisible = true;
            }
            base.OnAppearing();
        }

        private async void UsernameTgr_Tapped(object sender, EventArgs e)
        {
            await DisplayAlert("Mensagem", "Função não habilitada", "Ok");
        }

        private async void Logout_OnTapped(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                Settings.VerifiedUser = false;
                Settings.UserIsExecutiveCommissioner = false;
                var customer = await CustomerApi.Logout(new HttpClient(), Settings.UserDocument, Settings.DeviceToken);
                if (customer != null)
                {
                    if (customer.Error.Any())
                    {
                        var errorText = Util.GetWsErrorDescription(customer.Error.First());
                        await DisplayAlert("Atenção", errorText, "Fechar");
                    }
                    else
                    {
                        Settings.Logout();
                        Application.Current.MainPage = new NavigationPage(new LoginPage());
                        await Navigation.PopToRootAsync(true);
                    }
                }
                else
                {
                    await DisplayAlert("Erro", "Erro ao desconectar", "Ok");
                }
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }

        private void TapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Navigation.PopAsync(true);
        }

        private void Username_OnTapped(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new AppNivel.GrialTheme.Views.Settings.SettingsPage());
        }

        private void Establishments_OnTapped(object sender, EventArgs e)
        {
            Settings.BusinessActivityFilter = 0;
            Settings.BusinessActivityFilterName = "";
            Application.Current.MainPage = new NavigationPage(new MainPage());
            this.Navigation.PopToRootAsync(true);
        }

        private async void Indicate_OnTapped(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                await Navigation.PushAsync(new IndicatePage());
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
            //await Share();
            //Navigation.PushAsync(new IndicatePage(), true);
        }

        private async void Suggest_OnTapped(object sender, EventArgs e)
        {
            await Share(true);
            //Navigation.PushAsync(new IndicatePage(PersonTypeEnum.Establishment), true);
        }

        private async void GenerateQr_OnTapped(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var loadingPage = new LoadingPage();
                await Navigation.PushPopupAsync(loadingPage);
                var qrCode = await TransactionApi.GetAuthorizationQr(_client);
                if (qrCode != null)
                {
                    await PopupNavigation.PopAllAsync();
                    await Navigation.PushModalAsync(new QRCodePage(qrCode));
                }
                else
                {
                    await PopupNavigation.PopAllAsync();
                }
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }

        private void ForgotPassword_OnTapped(object sender, EventArgs e)
        {
            //Device.OpenUri(new Uri(Settings.UrlForgotPassword));

            Device.OpenUri(new Uri(Language.RecoverPassword));
        }

        private void Categories_OnTapped(object sender, EventArgs e)
        {
            var categoriesPage = new EstablishmentFilter();
            //NavigationPage.SetHasNavigationBar(establishmentDetailPage, false);
            this.Navigation.PushAsync(categoriesPage, true);
        }

        public async Task<bool> Share(bool establishment = false)
        {
            var loadingPage = new LoadingPage();
            await Navigation.PushPopupAsync(loadingPage);
            await CrossShare.Current.ShareLink("Esta é a sua indicação para ter Nível, o aplicativo que dá dinheiro. Com ele você tem retorno em dinheiro das compras que você faz nos estabelecimentos credenciados. Não é ótimo você receber de volta uma parte do que você gasta comprando ao longo do mês? Baixe agora, é só clicar aqui: " +
                                               (establishment ? Settings.IndicateEstablishmentLink : Settings.IndicateCustomerLink),
                "NivelCard - Cashback a toda compra",
                "NivelCard - Cashback a toda compra");
            await PopupNavigation.PopAllAsync();
            return true;
        }

        private void Reward_OnTapped(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new RewardPage());
        }

        private void Receive_OnTapped(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new RewardPage(true));
        }

        private void Panel_OnTapped(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                this.Navigation.PushAsync(new PanelPage());
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }

        private void HowTo_OnTapped(object sender, EventArgs e)
        {
            //Device.OpenUri(new Uri("https://www.nivel.com.br/"));
            Device.OpenUri(new Uri(Language.MainUrl));
        }

        private void Executive_OnTapped(object sender, EventArgs e)
        {
            //Device.OpenUri(new Uri("https://www.nivel.com.br/site/executivo-nivel/"));
            Device.OpenUri(new Uri(Language.BeExecutive));
        }

        private void Transfer_OnTapped(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new TransferPage());
        }

        private void CreditRecharge_Tapped(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                //this.Navigation.PushAsync(new CreditRecharge());
                this.Navigation.PushAsync(new CreditRecharge());
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }
    }
}