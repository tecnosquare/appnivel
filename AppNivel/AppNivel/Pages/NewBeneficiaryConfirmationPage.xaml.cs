﻿using AppNivel.Helpers;
using AppNivel.Resources;
using Plugin.Connectivity;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;

namespace AppNivel.Pages
{
    public partial class NewBeneficiaryConfirmationPage : PopupPage
    {
        public NewBeneficiaryConfirmationPage()
        {
            InitializeComponent();
        }

        protected override bool OnBackgroundClicked()
        {
            return false;
        }

        private async void CloseAllPopup()
        {
            await Navigation.PopAllPopupAsync();
        }

        private void Cancel_OnClicked(object sender, EventArgs e)
        {
            CloseAllPopup();
        }

        private void NextStep()
        {
            //await PopupNavigation.PopAsync();
            //var answer = await DisplayAlert("Mensagem", "O que deseja fazer agora?", "Nova Bonificação", "Fechar");
            //if (answer)
            //{
            //    await Navigation.PopAllPopupAsync();
            //    await Navigation.PopAsync();
            //}
            //else
            //{
            //    await Navigation.PopAllPopupAsync();
            //    await Navigation.PopToRootAsync(true);
            //}
        }

        private async void Confirm_OnClicked(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var loadingPage = new LoadingPage();
                await PopupNavigation.PushAsync(loadingPage);
                var isValid = true;

                if (!EmailValidator.IsValid)
                {
                    await DisplayAlert("Atenção", "O e-mail digitado não é válido.", "Fechar");
                    isValid = false;
                }

                if (isValid)
                {
                    Settings.EmailBeneficiary = UserEmail.Text;
                    CloseAllPopup();
                }
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }
    }
}