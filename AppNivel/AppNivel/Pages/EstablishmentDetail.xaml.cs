﻿using AppNivel.Helpers;
using AppNivel.Models;
using AppNivel.Resources;
using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class EstablishmentDetail : ContentPage
    {
        private HttpClient _client = new HttpClient();
        private WSAddress _wsAddress;
        private string _address;
        private string _completeAddress;
        private WSEstablishment _establishment;
        private WSContact _telephone;

        private string _mondayDiscount;
        private string _tuesdayDiscount;
        private string _wednesdayDiscount;
        private string _thursdayDiscount;
        private string _fridayDiscount;
        private string _saturdayDiscount;
        private string _sundayDiscount;

        private string _mondayDiscountCashback;
        private string _tuesdayDiscountCashback;
        private string _wednesdayDiscountCashback;
        private string _thursdayDiscountCashback;
        private string _fridayDiscountCashback;
        private string _saturdayDiscountCashback;
        private string _sundayDiscountCashback;

        private string _mondayDiscountPes;
        private string _tuesdayDiscountPes;
        private string _wednesdayDiscountPes;
        private string _thursdayDiscountPes;
        private string _fridayDiscountPes;
        private string _saturdayDiscountPes;
        private string _sundayDiscountPes;

        public EstablishmentDetail(WSEstablishment establishment)
        {
            Settings.CurrentPage = "Establishment";
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            BindingContext = establishment;
            _establishment = establishment;
        }

        protected override void OnAppearing()
        {
            OpeningHoursPanel.IsVisible = !string.IsNullOrWhiteSpace(_establishment.OpeningHours);
            BusinessActivity.IsVisible = !string.IsNullOrWhiteSpace(_establishment.BusinessActivity);
            _wsAddress = _establishment.Address.FirstOrDefault();
            if (_wsAddress != null)
            {
                _address = Util.GetAddressMin(_wsAddress);
                _completeAddress = Util.GetAddress(_wsAddress);
                Address.Text = _completeAddress;
            }
            var typePhone = (int)ContactType.Telefone;
            var telephone = _establishment.Contact.FirstOrDefault(x => x.ContactTypeId == typePhone && x.Main);
            _telephone = telephone;
            if (telephone != null)
            {
                phone.IsVisible = true;
                phoneLabel.IsVisible = true;
                phone.Text = telephone.ContactValue;
            }
            imageHeader.Source = string.IsNullOrWhiteSpace(_establishment.BackgroundImage) ? "semimagem.jpg" : _establishment.BackgroundImage;
            if (_establishment.Configuration.Any())
            {
                if (_establishment.Configuration.Any(x => x.DayOfWeek != null))
                {
                    var config = _establishment.Configuration.First(x => x.DayOfWeek == null);
                    var discount = (config.CashBack + config.Points).ToString("N2") + "%";
                    var discountCashback = (config.CashBack).ToString("N2") + "%";
                    var discountPes = (config.Points).ToString("N2") + "%";

                    var configMon = _establishment.Configuration.FirstOrDefault(x => x.DayOfWeek == (int)DayOfWeek.Monday);
                    var discountMon = configMon != null ? (configMon.CashBack + configMon.Points).ToString("N2") + "%" : discount;
                    var discountMonCashback = configMon != null ? (configMon.CashBack).ToString("N2") + "%" : discountCashback;
                    var discountMonPes = configMon != null ? (configMon.Points).ToString("N2") + "%" : discountPes;
                    var monClosed = configMon?.DayClosed ?? false;

                    var configTue = _establishment.Configuration.FirstOrDefault(x => x.DayOfWeek == (int)DayOfWeek.Tuesday);
                    var discountTue = configTue != null ? (configTue.CashBack + configTue.Points).ToString("N2") + "%" : discount;
                    var discountTueCashback = configTue != null ? (configTue.CashBack).ToString("N2") + "%" : discountCashback;
                    var discountTuePes = configTue != null ? (configTue.Points).ToString("N2") + "%" : discountPes;
                    var tueClosed = configTue?.DayClosed ?? false;

                    var configWed = _establishment.Configuration.FirstOrDefault(x => x.DayOfWeek == (int)DayOfWeek.Wednesday);
                    var discountWed = configWed != null ? (configWed.CashBack + configWed.Points).ToString("N2") + "%" : discount;
                    var discountWedCashback = configWed != null ? (configWed.CashBack).ToString("N2") + "%" : discountCashback;
                    var discountWedPes = configWed != null ? (configWed.Points).ToString("N2") + "%" : discountPes;
                    var wedClosed = configWed?.DayClosed ?? false;

                    var configThu = _establishment.Configuration.FirstOrDefault(x => x.DayOfWeek == (int)DayOfWeek.Thursday);
                    var discountThu = configThu != null ? (configThu.CashBack + configThu.Points).ToString("N2") + "%" : discount;
                    var discountThuCashback = configThu != null ? (configThu.CashBack).ToString("N2") + "%" : discountCashback;
                    var discountThuPes = configThu != null ? (configThu.Points).ToString("N2") + "%" : discountPes;
                    var thuClosed = configThu?.DayClosed ?? false;

                    var configFri = _establishment.Configuration.FirstOrDefault(x => x.DayOfWeek == (int)DayOfWeek.Friday);
                    var discountFri = configFri != null ? (configFri.CashBack + configFri.Points).ToString("N2") + "%" : discount;
                    var discountFriCashback = configFri != null ? (configFri.CashBack).ToString("N2") + "%" : discountCashback;
                    var discountFriPes = configFri != null ? (configFri.Points).ToString("N2") + "%" : discountPes;
                    var friClosed = configFri?.DayClosed ?? false;

                    var configSat = _establishment.Configuration.FirstOrDefault(x => x.DayOfWeek == (int)DayOfWeek.Saturday);
                    var discountSat = configSat != null ? (configSat.CashBack + configSat.Points).ToString("N2") + "%" : discount;
                    var discountSatCashback = configSat != null ? (configSat.CashBack).ToString("N2") + "%" : discountCashback;
                    var discountSatPes = configSat != null ? (configSat.Points).ToString("N2") + "%" : discountPes;
                    var satClosed = configSat?.DayClosed ?? false;

                    var configSun = _establishment.Configuration.FirstOrDefault(x => x.DayOfWeek == (int)DayOfWeek.Sunday);
                    var discountSun = configSun != null ? (configSun.CashBack + configSun.Points).ToString("N2") + "%" : discount;
                    var discountSunCashback = configSun != null ? (configSun.CashBack).ToString("N2") + "%" : discountCashback;
                    var discountSunPes = configSun != null ? (configSun.Points).ToString("N2") + "%" : discountPes;
                    var sunClosed = configSun?.DayClosed ?? false;

                    _mondayDiscount = discountMon;
                    _tuesdayDiscount = discountTue;
                    _wednesdayDiscount = discountWed;
                    _thursdayDiscount = discountThu;
                    _fridayDiscount = discountFri;
                    _saturdayDiscount = discountSat;
                    _sundayDiscount = discountSun;

                    _mondayDiscountCashback = discountMonCashback;
                    _tuesdayDiscountCashback = discountTueCashback;
                    _wednesdayDiscountCashback = discountWedCashback;
                    _thursdayDiscountCashback = discountThuCashback;
                    _fridayDiscountCashback = discountFriCashback;
                    _saturdayDiscountCashback = discountSatCashback;
                    _sundayDiscountCashback = discountSunCashback;

                    _mondayDiscountPes = discountMonPes;
                    _tuesdayDiscountPes = discountTuePes;
                    _wednesdayDiscountPes = discountWedPes;
                    _thursdayDiscountPes = discountThuPes;
                    _fridayDiscountPes = discountFriPes;
                    _saturdayDiscountPes = discountSatPes;
                    _sundayDiscountPes = discountSunPes;

                    ClosedSunday.IsVisible = sunClosed;
                    DiscountSunday.IsVisible = !ClosedSunday.IsVisible;

                    ClosedMonday.IsVisible = monClosed;
                    DiscountMonday.IsVisible = !ClosedMonday.IsVisible;

                    ClosedTuesday.IsVisible = tueClosed;
                    DiscountTuesday.IsVisible = !ClosedTuesday.IsVisible;

                    ClosedWednesday.IsVisible = wedClosed;
                    DiscountWednesday.IsVisible = !ClosedWednesday.IsVisible;

                    ClosedThursday.IsVisible = thuClosed;
                    DiscountThursday.IsVisible = !ClosedThursday.IsVisible;

                    ClosedFriday.IsVisible = friClosed;
                    DiscountFriday.IsVisible = !ClosedFriday.IsVisible;

                    ClosedSaturday.IsVisible = satClosed;
                    DiscountSaturday.IsVisible = !ClosedSaturday.IsVisible;
                }
                else
                {
                    var config = _establishment.Configuration.First();
                    var discount = (config.CashBack + config.Points).ToString("N2") + "%";
                    var discountCashback = (config.CashBack).ToString("N2") + "%";
                    var discountPes = (config.Points).ToString("N2") + "%";

                    _mondayDiscount = discount;
                    _tuesdayDiscount = discount;
                    _wednesdayDiscount = discount;
                    _thursdayDiscount = discount;
                    _fridayDiscount = discount;
                    _saturdayDiscount = discount;
                    _sundayDiscount = discount;

                    _mondayDiscountCashback = discountCashback;
                    _tuesdayDiscountCashback = discountCashback;
                    _wednesdayDiscountCashback = discountCashback;
                    _thursdayDiscountCashback = discountCashback;
                    _fridayDiscountCashback = discountCashback;
                    _saturdayDiscountCashback = discountCashback;
                    _sundayDiscountCashback = discountCashback;

                    _mondayDiscountPes = discountPes;
                    _tuesdayDiscountPes = discountPes;
                    _wednesdayDiscountPes = discountPes;
                    _thursdayDiscountPes = discountPes;
                    _fridayDiscountPes = discountPes;
                    _saturdayDiscountPes = discountPes;
                    _sundayDiscountPes = discountPes;
                }

                DiscountMonday.Text = _mondayDiscount;
                DiscountTuesday.Text = _tuesdayDiscount;
                DiscountWednesday.Text = _wednesdayDiscount;
                DiscountThursday.Text = _thursdayDiscount;
                DiscountFriday.Text = _fridayDiscount;
                DiscountSaturday.Text = _saturdayDiscount;
                DiscountSunday.Text = _sundayDiscount;

                switch (DateTime.Now.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        if (ClosedMonday.IsVisible)
                        {
                            TotalDiscountToday.Text = "FECHADO";
                            DetailDiscountToday.Text = "";
                        }
                        else
                        {
                            TotalDiscountToday.Text = "GANHE " + _mondayDiscount + "*";
                            DetailDiscountToday.Text = "*CASHBACK " + _mondayDiscountCashback + " + CASHCRED " +
                                                       _mondayDiscountPes;
                        }
                        break;

                    case DayOfWeek.Tuesday:
                        if (ClosedTuesday.IsVisible)
                        {
                            TotalDiscountToday.Text = "FECHADO";
                            DetailDiscountToday.Text = "";
                        }
                        else
                        {
                            TotalDiscountToday.Text = "GANHE " + _tuesdayDiscount + "*";
                            DetailDiscountToday.Text = "*CASHBACK " + _tuesdayDiscountCashback + " + CASHCRED " +
                                                       _tuesdayDiscountPes;
                        }
                        break;

                    case DayOfWeek.Wednesday:
                        if (ClosedWednesday.IsVisible)
                        {
                            TotalDiscountToday.Text = "FECHADO";
                            DetailDiscountToday.Text = "";
                        }
                        else
                        {
                            TotalDiscountToday.Text = "GANHE " + _wednesdayDiscount + "*";
                            DetailDiscountToday.Text = "*CASHBACK " + _wednesdayDiscountCashback + " + CASHCRED " +
                                                       _wednesdayDiscountPes;
                        }
                        break;

                    case DayOfWeek.Thursday:
                        if (ClosedThursday.IsVisible)
                        {
                            TotalDiscountToday.Text = "FECHADO";
                            DetailDiscountToday.Text = "";
                        }
                        else
                        {
                            TotalDiscountToday.Text = "GANHE " + _thursdayDiscount + "*";
                            DetailDiscountToday.Text = "*CASHBACK " + _thursdayDiscountCashback + " + CASHCRED " +
                                                       _thursdayDiscountPes;
                        }
                        break;

                    case DayOfWeek.Friday:
                        if (ClosedFriday.IsVisible)
                        {
                            TotalDiscountToday.Text = "FECHADO";
                            DetailDiscountToday.Text = "";
                        }
                        else
                        {
                            TotalDiscountToday.Text = "GANHE " + _fridayDiscount + "*";
                            DetailDiscountToday.Text = "*CASHBACK " + _fridayDiscountCashback + " + CASHCRED " +
                                                       _fridayDiscountPes;
                        }
                        break;

                    case DayOfWeek.Saturday:
                        if (ClosedSaturday.IsVisible)
                        {
                            TotalDiscountToday.Text = "FECHADO";
                            DetailDiscountToday.Text = "";
                        }
                        else
                        {
                            TotalDiscountToday.Text = "GANHE " + _saturdayDiscount + "*";
                            DetailDiscountToday.Text = "*CASHBACK " + _saturdayDiscountCashback + " + CASHCRED " +
                                                       _saturdayDiscountPes;
                        }
                        break;

                    case DayOfWeek.Sunday:
                        if (ClosedSunday.IsVisible)
                        {
                            TotalDiscountToday.Text = "FECHADO";
                            DetailDiscountToday.Text = "";
                        }
                        else
                        {
                            TotalDiscountToday.Text = "GANHE " + _sundayDiscount + "*";
                            DetailDiscountToday.Text = "*CASHBACK " + _sundayDiscountCashback + " + CASHCRED " +
                                                       _sundayDiscountPes;
                        }
                        break;
                }
            }
            PaymentForms.Children.Clear();
            if (_establishment.PaymentForms.Any())
            {
                foreach (var paymentForm in _establishment.PaymentForms)
                {
                    var img = new Image
                    {
                        HeightRequest = 58,
                        Source = paymentForm.Icon
                    };

                    PaymentForms.Children.Add(img);
                }
            }
            else
            {
                PaymentFormsPanel.IsVisible = false;
                PaymentFormLine.IsVisible = false;
            }

            SuperCashbackPanel.Children.Clear();
            if (_establishment.SuperCashbacks.Any())
            {
                var panelA = new StackLayout
                {
                    BackgroundColor = (Color)Application.Current.Resources["GreenColor"],
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    Padding = new Thickness(25, 5),
                    Margin = new Thickness(0, 0, 0, 10)
                };
                var lblA = new Label
                {
                    Text = "SUPERCASHBACK HOJE",
                    TextColor = (Color)Application.Current.Resources["PrimaryColor"],
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    FontAttributes = FontAttributes.Bold,
                    FontSize = 18
                };
                panelA.Children.Add(lblA);
                SuperCashbackPanel.Children.Add(panelA);
                foreach (var establishmentSuperCashback in _establishment.SuperCashbacks)
                {
                    var lblB = new Label
                    {
                        Text = establishmentSuperCashback.Description.ToUpper(),
                        FontSize = 16,
                        TextColor = (Color)Application.Current.Resources["CreditsColor"],
                        FontAttributes = FontAttributes.Bold
                    };
                    var lblC = new Label
                    {
                        Text = "Ganhe " + establishmentSuperCashback.TotalBonus.ToString("N2", CultureInfo.CurrentCulture) + "%",
                        FontSize = 28,
                        TextColor = (Color)Application.Current.Resources["FeaturedBackgroundColor"]
                    };

                    SuperCashbackPanel.Children.Add(lblB);
                    SuperCashbackPanel.Children.Add(lblC);
                }
            }
            else
            {
                SuperCashbackLine.IsVisible = false;
                SuperCashbackPanel.IsVisible = false;
            }

            ImagesPanel.Children.Clear();
            if (_establishment.Images.Any())
            {
                foreach (var image in _establishment.Images)
                {
                    var img = new Image
                    {
                        Source = ImageSource.FromUri(new Uri(image.Path)),
                        Aspect = Aspect.AspectFill,
                        HeightRequest = 160
                    };
                    var lbl = new Label
                    {
                        Text = image.Name,
                        TextColor = (Color)Application.Current.Resources["TertiaryColor"]
                    };

                    var bxView = new BoxView()
                    {
                        HeightRequest = 2
                    };

                    ImagesPanel.Children.Add(img);
                    ImagesPanel.Children.Add(lbl);
                    ImagesPanel.Children.Add(bxView);
                }
            }
            else
            {
                ImagesLine.IsVisible = false;
                ImagesPanel.IsVisible = false;
            }

            base.OnAppearing();
        }

        private async void FavoriteTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            var image = sender as Image;

            var establishment = (image.GestureRecognizers[0] as TapGestureRecognizer).CommandParameter as WSEstablishment;
            if (establishment.Favorite)
            {
                establishment.Favorite = false;
                image.Source = "_favorite.png";
            }
            else
            {
                establishment.Favorite = true;
                image.Source = "favorite.png";
            }
            await CustomerApi.Favorite(_client, establishment.Id, establishment.Favorite);
        }

        private void TapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        private void Address_OnTapped(object sender, EventArgs e)
        {
            switch (Device.OS)
            {
                case TargetPlatform.iOS:
                    Device.OpenUri(new Uri($"http://maps.apple.com/?q={WebUtility.UrlEncode(_completeAddress)}"));
                    break;

                case TargetPlatform.Android:
                    Device.OpenUri(new Uri($"geo:0,0?q={WebUtility.UrlEncode(_completeAddress)}"));
                    break;

                case TargetPlatform.Other:
                    break;

                case TargetPlatform.WinPhone:
                    break;

                case TargetPlatform.Windows:
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void Mon_OnTapped(object sender, EventArgs e)
        {
            if (ClosedMonday.IsVisible)
            {
                TotalDiscountToday.Text = "FECHADO";
                DetailDiscountToday.Text = "";
            }
            else
            {
                TotalDiscountToday.Text = "GANHE " + _mondayDiscount + "*";
                DetailDiscountToday.Text = "*CASHBACK " + _mondayDiscountCashback + " + CASHCRED " + _mondayDiscountPes;
            }
            LabelCredits.Text = "SEGUNDA-FEIRA";
        }

        private void Tue_OnTapped(object sender, EventArgs e)
        {
            if (ClosedTuesday.IsVisible)
            {
                TotalDiscountToday.Text = "FECHADO";
                DetailDiscountToday.Text = "";
            }
            else
            {
                TotalDiscountToday.Text = "GANHE " + _tuesdayDiscount + "*";
                DetailDiscountToday.Text = "*CASHBACK " + _tuesdayDiscountCashback + " + CASHCRED " + _tuesdayDiscountPes;
            }
            LabelCredits.Text = "TERÇA-FEIRA";
        }

        private void Wed_OnTapped(object sender, EventArgs e)
        {
            if (ClosedWednesday.IsVisible)
            {
                TotalDiscountToday.Text = "FECHADO";
                DetailDiscountToday.Text = "";
            }
            else
            {
                TotalDiscountToday.Text = "GANHE " + _wednesdayDiscount + "*";
                DetailDiscountToday.Text = "*CASHBACK " + _wednesdayDiscountCashback + " + CASHCRED " +
                                           _wednesdayDiscountPes;
            }
            LabelCredits.Text = "QUARTA-FEIRA";
        }

        private void Thu_OnTapped(object sender, EventArgs e)
        {
            if (ClosedThursday.IsVisible)
            {
                TotalDiscountToday.Text = "FECHADO";
                DetailDiscountToday.Text = "";
            }
            else
            {
                TotalDiscountToday.Text = "GANHE " + _thursdayDiscount + "*";
                DetailDiscountToday.Text = "*CASHBACK " + _thursdayDiscountCashback + " + CASHCRED " +
                                           _thursdayDiscountPes;
            }
            LabelCredits.Text = "QUINTA-FEIRA";
        }

        private void Fri_OnTapped(object sender, EventArgs e)
        {
            if (ClosedFriday.IsVisible)
            {
                TotalDiscountToday.Text = "FECHADO";
                DetailDiscountToday.Text = "";
            }
            else
            {
                TotalDiscountToday.Text = "GANHE " + _fridayDiscount + "*";
                DetailDiscountToday.Text = "*CASHBACK " + _fridayDiscountCashback + " + CASHCRED " + _fridayDiscountPes;
            }
            LabelCredits.Text = "SEXTA-FEIRA";
        }

        private void Sat_OnTapped(object sender, EventArgs e)
        {
            if (ClosedSaturday.IsVisible)
            {
                TotalDiscountToday.Text = "FECHADO";
                DetailDiscountToday.Text = "";
            }
            else
            {
                TotalDiscountToday.Text = "GANHE " + _saturdayDiscount + "*";
                DetailDiscountToday.Text = "*CASHBACK " + _saturdayDiscountCashback + " + CASHCRED " +
                                           _saturdayDiscountPes;
            }
            LabelCredits.Text = "SÁBADO";
        }

        private void Sun_OnTapped(object sender, EventArgs e)
        {
            if (ClosedSunday.IsVisible)
            {
                TotalDiscountToday.Text = "FECHADO";
                DetailDiscountToday.Text = "";
            }
            else
            {
                TotalDiscountToday.Text = "GANHE " + _sundayDiscount + "*";
                DetailDiscountToday.Text = "*CASHBACK " + _sundayDiscountCashback + " + CASHCRED " + _sundayDiscountPes;
            }
            LabelCredits.Text = "DOMINGO";
        }

        private void Phone_OnTapped(object sender, EventArgs e)
        {
            if (_telephone != null)
                Device.OpenUri(new Uri($"tel:{_telephone.ContactValue}"));
        }

        private void Description_OnTapped(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(_establishment.DescriptionLink))
            {
                Device.OpenUri(new Uri(_establishment.DescriptionLink));
            }
        }

        private async void Payment_OnTapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PaymentPage(_establishment));
        }
    }
}