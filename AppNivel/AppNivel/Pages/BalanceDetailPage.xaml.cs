﻿using AppNivel.Models;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class BalanceDetailPage : PopupPage
    {
        private WSTransaction _movement;

        public BalanceDetailPage(WSTransaction movement)
        {
            InitializeComponent();
            _movement = movement;
            BindingContext = movement;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (_movement.TransactionTotalCash < 0.01M)
            {
                lblLowerThanCent.Text = "Valor menor que 1 centavo";
            }
            personBeneficiaryName.Text = _movement.TransactionTypeId == (int)WSTransactionType.Commission ? "Premiação por compra de amigo" : $"Beneficiário: {_movement.PersonBeneficiaryName}";
            CloseImage.Rotation = 30;
            CloseImage.Scale = 0.3;
            CloseImage.Opacity = 0;
        }

        protected override async Task OnAppearingAnimationEnd()
        {
            await Task.WhenAll(
                CloseImage.FadeTo(1),
                CloseImage.ScaleTo(1, easing: Easing.SpringOut),
                CloseImage.RotateTo(0));
        }

        private void OnCloseButtonTapped(object sender, EventArgs e)
        {
            CloseAllPopup();
        }

        protected override bool OnBackgroundClicked()
        {
            CloseAllPopup();

            return false;
        }

        private async void CloseAllPopup()
        {
            await Navigation.PopAllPopupAsync();
        }
    }
}