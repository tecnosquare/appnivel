﻿using AppNivel.Help;
using AppNivel.Helpers;
using AppNivel.Models;
using AppNivel.Resources;
using System;
using System.IO;
using System.Reflection;
using Xamarin.Forms;
using Color = Xamarin.Forms.Color;

namespace AppNivel.Pages
{
    public partial class ReceiptPage : ContentPage
    {
        private WSPerson _beneficiary;
        private WSPerson _sender;
        private string _transactionValue;
        private WSTransactionType _transactionType;
        private bool _isChildRewarding;
        private string _transactionDoc;
        private string _transactionName;

        public ReceiptPage(WSPerson sender, WSPerson beneficiary, string transactionValue, WSTransactionType transactionType, string transactionDoc, bool isChildRewarding = false)
        {
            Settings.CurrentPage = "Receipt";
            InitializeComponent();
            _beneficiary = beneficiary;
            _sender = sender;
            _transactionValue = transactionValue;
            _transactionDoc = transactionDoc;
            _isChildRewarding = isChildRewarding;
            _transactionType = transactionType;
            _transactionName = Util.GetTransactionType(_transactionType);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            TransactionDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            if (_transactionType == WSTransactionType.Debit)
                TransactionValue.TextColor = Color.FromHex("#E66363");
            TransactionValue.Text = _transactionValue;
            TransactionDoc.Text = _transactionDoc;
            Sender.Text = Util.CpfCnpjMask(_sender.CpfCnpj);
            Beneficiary.Text = Util.CpfCnpjMask(_beneficiary.CpfCnpj);
            TransactionType.Text = _transactionName;
        }

        private async void Close()
        {
            await Navigation.PopModalAsync(true);
        }

        private void Cancel_OnClicked(object sender, EventArgs e)
        {
            Close();
        }

        private void Confirm_OnClicked(object sender, EventArgs e)
        {
            var pdfGenerator = new PdfGenerator();
            pdfGenerator.Setup("Recibo de Transação");

            var assembly = this.GetType().GetTypeInfo().Assembly;
            byte[] buffer;
            using (Stream s = assembly.GetManifestResourceStream("AppNivel.Assets.nivel.jpg"))
            {
                long length = s.Length;
                buffer = new byte[length];
                s.Read(buffer, 0, (int)length);
            }

            float y = pdfGenerator.GenerateBodyTransactionReceipt(buffer, 65, 30, Beneficiary.Text, Sender.Text, _transactionName, _transactionValue, TransactionDate.Text, _transactionDoc);

            pdfGenerator.Save($"ReciboTransacaoNivel{_transactionName}{DateTime.Now.Ticks}.pdf");
        }
    }
}