﻿using AppNivel.Resources;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using System;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class NotConnectedPage : ContentPage
    {
        public NotConnectedPage()
        {
            //NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            CrossConnectivity.Current.ConnectivityChanged += CurrentOnConnectivityChanged;
        }

        private async void CurrentOnConnectivityChanged(object sender, ConnectivityChangedEventArgs connectivityChangedEventArgs)
        {
            if (connectivityChangedEventArgs.IsConnected)
            {
                try
                {
                    await Navigation.PopToRootAsync(true);
                    Application.Current.MainPage = new NavigationPage(new MainPage());
                }
                catch (Exception ex)
                {
                    await DisplayAlert("Mensagem", "Por favor, reinicie o aplicativo para continuar", "Ok");
                    var closer = DependencyService.Get<ICloseApplication>();
                    closer?.QuitApplication();
                }
            }
        }
    }
}