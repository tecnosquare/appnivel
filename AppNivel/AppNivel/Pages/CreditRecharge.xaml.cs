﻿using AppNivel.Helpers;
using AppNivel.Resources;
using Plugin.Connectivity;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppNivel.Pages
{
    public class ProductProvider
    {
        public string Codigo { get; set; }
        public string Nome { get; set; }
        public string TipoOperadora { get; set; }
    }

    public class Product
    {
        public string CodigoProduto { get; set; }
        public string Nome { get; set; }
        public string ModeloRecargaNome { get; set; }
        public bool ModeloRecargaPin { get; set; }
        public decimal PrecoCompra { get; set; }
        public decimal PrecoVenda { get; set; }
        public string Validade { get; set; }
        public decimal ValorMinimo { get; set; }
        public decimal ValorMaximo { get; set; }
    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreditRecharge : ContentPage
    {
        private HttpClient _client = new HttpClient();
        private List<ProductProvider> ListProductProvider = new List<ProductProvider>();
        private List<Product> ListProduct = new List<Product>();
        public string TelefoneCadastro;

        public CreditRecharge()
        {
            Settings.CurrentPage = "CreditRecharge";
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var loadingPage = new LoadingPage();
                await PopupNavigation.PushAsync(loadingPage);

                //PhoneLabel.Text = Language.Cellphone;
                PhoneInput.Placeholder = Language.Cellphone;

                //PasswordLabel.Text = Language.PlaceholderPassword;
                PasswordInput.Placeholder = Language.PlaceholderPassword;

                //Redundância para garantir
                ReceiptStack.IsVisible = false;
                RechargeStack.IsVisible = true;

                //Executa a chamada do webservice para preencher o combo de operadora
                var providers = await GeneralApi.GetProviderCreditAsync(_client, "", Settings.UserDocument);

                if (providers != null)
                {
                    if (!providers.Error.Any())
                    {
                        TelefoneCadastro = providers.TelefoneCadastro;
                        ListProductProvider = new List<ProductProvider>();

                        foreach (var wsProvider in providers.ListaOperadora)
                        {
                            //Preenche o picker com os dados do webservice
                            CreditProviderPicker.Items.Add(wsProvider.Nome);

                            var productP = new ProductProvider()
                            {
                                Codigo = wsProvider.Codigo,
                                Nome = wsProvider.Nome,
                                TipoOperadora = wsProvider.TipoOp
                            };
                            //Preenche a lista com os dados da classe
                            ListProductProvider.Add(productP);
                        }
                    }
                    else
                    {
                        await DisplayAlert("Atenção", "Sem operadoras cadastradas", "Ok");
                    }
                }
                else
                {
                    await DisplayAlert("Atenção", "Sem operadoras cadastradas", "Ok");
                }

                CloseAllPopup();
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
            base.OnAppearing();
        }

        private async void CloseAllPopup()
        {
            await Navigation.PopAllPopupAsync();
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            Navigation.PopAsync(true);
        }

        private void UserPhone_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            Util.CelPhoneMask(sender, e);
        }

        private void HideShowInputs()
        {
            var providerSelected = CreditProviderPicker.SelectedIndex != -1 ? CreditProviderPicker.Items[CreditProviderPicker.SelectedIndex] : "";

            if (!(string.IsNullOrEmpty(providerSelected)))
            {
                //Cria um objeto(operadoras) a partir de uma comparação por lista, única maneira encontrada para o tal
                //var objProvider = ListProductProvider.Any(x => x.Nome == providerSelected) ? ListProductProvider.Single(x => x.Nome == providerSelected) : null;
                var objProvider = ListProductProvider.SingleOrDefault(x => x.Nome == providerSelected);

                if (objProvider != null)
                {
                    var productSelected = ProductPicker.SelectedIndex != -1 ? ProductPicker.Items[ProductPicker.SelectedIndex] : "";

                    //Cria objeto de produtos
                    //var objProduct = ListProduct.Any(x => x.Nome == productSelected) ? ListProduct.First(x => x.Nome == productSelected) : null;
                    var objProduct = ListProduct.FirstOrDefault(x => x.Nome == productSelected);

                    //Faz a checagem de acordo com as regras do fornecedor do serviço (RV)
                    switch (objProvider.TipoOperadora)
                    {
                        case "TV":
                            {
                                SubscriberCodeLabel.IsVisible = SubscriberCodeInput.IsVisible = true;
                                PhoneLabel.IsVisible = PhoneInput.IsVisible = false;

                                break;
                            }
                        case "NORMAL":
                            {
                                if (objProduct != null)
                                {
                                    if (objProduct.ModeloRecargaNome == "PIN")
                                    {
                                        SubscriberCodeLabel.IsVisible = SubscriberCodeInput.IsVisible = PhoneLabel.IsVisible = PhoneInput.IsVisible = false;
                                    }
                                    else
                                    {
                                        PhoneLabel.IsVisible = PhoneInput.IsVisible = true;
                                        PhoneInput.Text = TelefoneCadastro;
                                    }
                                }
                                break;
                            }
                    }
                    PasswordLabel.IsVisible = PasswordInput.IsVisible = true;
                }
            }
        }

        private async void ProviderChangeSelect(object sender, FocusEventArgs e)
        {
            var selectedProduct = CreditProviderPicker.Items[CreditProviderPicker.SelectedIndex];//Pega o nome da operadora selecionada para comparação
            var productCode = ListProductProvider.Any(x => x.Nome == selectedProduct) ? ListProductProvider.First(x => x.Nome == selectedProduct).Codigo : "";

            //Executa a chamada do webservice
            var products = await GeneralApi.GetProviderCreditAsync(_client, productCode);

            //"Limpa" o picker
            ProductPicker.Items.Clear();

            if (products != null)
            {
                if (!products.Error.Any())
                {
                    foreach (var product in products.ListaOperadora.First().Produtos)
                    {
                        //Preenche o picker de produtos
                        ProductPicker.Items.Add(product.Nome);
                        var productList = new Product
                        {
                            CodigoProduto = product.Codigo,
                            Nome = product.Nome,
                            ModeloRecargaNome = product.ModeloRecargaNome,
                            ModeloRecargaPin = product.ModeloRecargaPin,
                            PrecoVenda = product.PrecoVenda,
                        };
                        //Preenche a lista de produtos
                        ListProduct.Add(productList);
                    }

                    ProductLabel.IsEnabled = true;
                    ProductPicker.IsEnabled = true;
                }
                else
                {
                    await DisplayAlert("Atenção", "Sem produtos cadastrados", "Ok");
                }
            }
            else
            {
                await DisplayAlert("Atenção", "Sem produtos cadastrados", "Ok");
            }

            CloseAllPopup();
        }

        private void ProductChangeSelect(object sender, EventArgs e)
        {
            HideShowInputs();
        }

        //Método que efetua a recarga
        private async void DoRecharge(object sender, EventArgs e)
        {
            var loadingPage = new LoadingPage();
            await PopupNavigation.PushAsync(loadingPage);

            if (CrossConnectivity.Current.IsConnected)
            {
                var isValid = true;//Váriavel de controle

                //Tratamento e declaração do objeto de operadoras
                var providerSelected = CreditProviderPicker.SelectedIndex != -1 ? CreditProviderPicker.Items[CreditProviderPicker.SelectedIndex] : "";

                //var objOperadora = ListProductProvider.Any(x => x.Nome == providerSelected) ? ListProductProvider.Single(x => x.Nome == providerSelected) : null;
                var objOperadora = ListProductProvider.SingleOrDefault(x => x.Nome == providerSelected);

                //Tratamento e declaração do objeto de produtos
                var productSelected = ProductPicker.SelectedIndex != -1 ? ProductPicker.Items[ProductPicker.SelectedIndex] : "";

                //var objProduto = ListProduct.Any(x => x.Nome == productSelected) ? ListProduct.First(x => x.Nome == productSelected) : null;
                var objProduto = ListProduct.FirstOrDefault(x => x.Nome == productSelected);

                #region Valida recarga

                if (providerSelected == "" || objOperadora == null)
                {
                    await DisplayAlert("Atenção", " Selecione uma Operadora ", "Fechar");
                    isValid = false;
                }
                else
                {
                    if (!(productSelected == "" || objProduto == null))
                    {
                        switch (objOperadora.TipoOperadora)
                        {
                            case "TV":
                                {
                                    if (string.IsNullOrEmpty(SubscriberCodeInput.Text))
                                    {
                                        await DisplayAlert("Atenção", " Preencha o campo código de assinante ", "Fechar");
                                        isValid = false;
                                    }

                                    break;
                                }
                            case "NORMAL":
                                {
                                    if (objProduto.ModeloRecargaNome == "ONLINE" && string.IsNullOrEmpty(PhoneInput.Text))
                                    {
                                        await DisplayAlert("Atenção", " Preencha o campo de celular ", "Fechar");
                                        isValid = false;
                                    }
                                    break;
                                }
                        }
                    }
                    else
                    {
                        await DisplayAlert("Atenção", " Selecione um produto ", "Fechar");
                        isValid = false;
                    }
                    if (PasswordInput.Text != Settings.UserPassword && isValid)
                    {
                        await DisplayAlert("Atenção", "Senha incorreta", "Ok");
                        isValid = false;
                    }
                }

                #endregion Valida recarga

                if (isValid)
                {
                    if (await DisplayAlert("Atenção", "Tem certeza que deseja efetuar esta recarga?", "Confirmar", "Cancelar"))
                    {
                        //Preenche a classe de recarga com os dados requisitados pelo webservice
                        var recharge = new Models.WsRvRecargaCelular
                        {
                            CodigoAssinante = SubscriberCodeInput.Text,
                            CodigoOperadora = objOperadora.Codigo,
                            CodigoProduto = objProduto.CodigoProduto,
                            Ddd = "",
                            CpfCnpj = Settings.UserDocument,
                            Telefone = PhoneInput.Text,
                            UfTerminal = "",
                            ValorRecarga = 0m
                        };

                        //Executa o método de recarga
                        var rechargeReturn = await GeneralApi.Recharge(_client, recharge);

                        if (rechargeReturn != null)
                        {
                            if (rechargeReturn.Success)
                            {
                                //Monta e preenche o recibo de acordo com a recarga efetuada
                                if (rechargeReturn.TipoRecargaOnline)
                                {
                                    //Tipo recarga ONLINE
                                    TypeRechargeLabel.Text = "RECARGA ONLINE";

                                    PhonePINHeaderLabel.Text = "Fone Cliente";
                                    PhonePINDataLabel.Text = rechargeReturn.Fone;

                                    NSULoteSerieHeaderLabel.Text = "NSU TRANSAÇÂO";
                                    NSULoteSerieDataLabel.Text = rechargeReturn.NsuTransacao;
                                }
                                else
                                {
                                    //Tipo recarga por PIN
                                    TypeRechargeLabel.Text = "PIN";

                                    PhonePINHeaderLabel.Text = "PIN";
                                    PhonePINDataLabel.Text = rechargeReturn.PIN;

                                    NSULoteSerieHeaderLabel.Text = "LOTE/SÉRIE";
                                    NSULoteSerieDataLabel.Text = rechargeReturn.Lote;
                                }

                                DateHourLabel.Text = rechargeReturn.DataHora.ToString();
                                StoreName.Text = rechargeReturn.NomeLoja;
                                PurchaseCode.Text = rechargeReturn.CodigoCompraOnline;
                                ProviderName.Text = rechargeReturn.NomeOperadora;
                                RechargeValue.Text = rechargeReturn.ValorRecarga.ToString("C");
                                PromotionalMessage.Text = rechargeReturn.MensagemPromo;

                                RechargeStack.IsVisible = false;
                                ReceiptStack.IsVisible = true;
                            }
                            else
                            {
                                ReceiptStack.IsVisible = false;
                                await DisplayAlert("Erro", rechargeReturn.MsgErro, "Fechar");
                            }
                            CloseAllPopup();
                        }
                        else
                        {
                            await DisplayAlert("Erro", "Erro ao efetuar recarga.", "Fechar");
                            CloseAllPopup();
                        }
                    }
                }
                CloseAllPopup();
            }
            else
            {
                CloseAllPopup();
                Util.NotConnectedMessage(this);
            }
        }

        private async void ConfirmReceiptButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopToRootAsync(true);
        }

        private async void CancelRecharge_Clicked(object sender, EventArgs e)
        {
            if (await this.DisplayAlert("Atenção", "Tem certeza que deseja cancelar essa recarga?", "Sair", "Cancelar"))
            {
                await Navigation.PopToRootAsync(true);
            }
        }
    }
}