﻿using AppNivel.GrialTheme.Views.Logins;
using AppNivel.Helpers;
using AppNivel.Resources;
using Plugin.Connectivity;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class RegistrationPage : ContentPage
    {
        private HttpClient _client = new HttpClient();
        private bool reloadCities = true;

        public RegistrationPage()
        {
            Settings.CurrentPage = "Registration";
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var loadingPage = new LoadingPage();
                await PopupNavigation.PushAsync(loadingPage);

                var states = await GeneralApi.GetStatesAsync(_client);

                if (states != null)
                {
                    if (!states.Error.Any())
                    {
                        foreach (var wsState in states.List)
                        {
                            StatePicker.Items.Add(wsState.Name);
                        }

                        StatePicker.SelectedIndex = 0;

                        var selectedState = StatePicker.Items[StatePicker.SelectedIndex];
                        var cities = await GeneralApi.GetCitiesAsync(_client, selectedState, "");

                        foreach (var wsCity in cities.List)
                        {
                            CityPicker.Items.Add(wsCity.Name);
                        }
                    }
                    else
                    {
                        await DisplayAlert("Atenção", "Sem estados cadastrados", "Ok");
                    }
                }
                else
                {
                    await DisplayAlert("Atenção", "Sem estados cadastrados", "Ok");
                }

                CloseAllPopup();
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
            base.OnAppearing();
        }

        private async void CloseAllPopup()
        {
            await Navigation.PopAllPopupAsync();
        }

        private void ConfigTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Navigation.PopAsync(true);
        }

        private async void SaveData_OnClicked(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var loadingPage = new LoadingPage();
                await PopupNavigation.PushAsync(loadingPage);
                var isValid = true;

                if (!DocumentValidator.IsValid)
                {
                    await DisplayAlert("Atenção", "O Cpf/Cnpj digitado não é válido.", "Fechar");
                    isValid = false;
                }

                if (!EmailValidator.IsValid && isValid)
                {
                    await DisplayAlert("Atenção", "O e-mail digitado não é válido.", "Fechar");
                    isValid = false;
                }

                if ((string.IsNullOrWhiteSpace(UserFirstname.Text) || UserFirstname.Text.Length < 2) && isValid)
                {
                    await DisplayAlert("Atenção", "Digite o seu nome.", "Fechar");
                    isValid = false;
                }

                if (!PostalCodeValidator.IsValid)
                {
                    await DisplayAlert("Atenção", "O CEP digitado não é válido.", "Fechar");
                    isValid = false;
                }

                if ((string.IsNullOrWhiteSpace(UserPassword.Text) || UserPassword.Text.Length < 6) && isValid)
                {
                    await DisplayAlert("Atenção", "Sua senha deve conter, no mínimo, 6 caracteres.", "Fechar");
                    isValid = false;
                }

                if (CityPicker.SelectedIndex == -1 && isValid)
                {
                    await DisplayAlert("Atenção", "Escolha uma cidade.", "Fechar");
                    isValid = false;
                }

                if (isValid)
                {
                    var customer =
                        await
                            CustomerApi.SaveData(_client, UserDocument.Text.Replace(".", "").Replace("-", ""), UserPassword.Text, UserFirstname.Text, "", UserEmail.Text,
                                UserPhone.Text, CityPicker.Items[CityPicker.SelectedIndex], PostalCode.Text);

                    if (customer != null)
                    {
                        if (customer.Error.Any())
                        {
                            var errorText = Util.GetWsErrorDescription(customer.Error.First());
                            await DisplayAlert("Atenção", errorText, "Fechar");
                        }
                        else
                        {
                            await
                                DisplayAlert("Sucesso",
                                    "Cadastrado realizado! Confirme seu registro no link enviado para seu e-mail.", "Ok");
                            Settings.Logout();
                            Application.Current.MainPage = new NavigationPage(new LoginPage());
                            await Navigation.PopToRootAsync(true);
                        }
                    }
                    else
                    {
                        await
                                DisplayAlert("Atenção",
                                    "Não foi possível realizar seu cadastro. Tente novamente mais tarde.", "Ok");
                    }
                }

                await PopupNavigation.PopAllAsync();
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }

        private void UserDocument_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            switch (CultureInfo.CurrentCulture.Name)
            {
                case "pt-PT":
                    Util.NifMask(sender, e);
                    break;

                default:
                    if (e.NewTextValue.Length > 14)
                    {
                        Util.CnpjMask(sender, e);
                    }
                    else
                    {
                        Util.CpfMask(sender, e);
                    }
                    break;
            }
        }

        private void UserPhone_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            Util.CelPhoneMask(sender, e);
        }

        private void ReadTermsOfUse(object sender, EventArgs e)
        {
            PopupNavigation.PushAsync(new TermsOfUse());
        }

        private async void StatePicker_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (!reloadCities) return;
            var loadingPage = new LoadingPage();
            await PopupNavigation.PushAsync(loadingPage);
            var selectedState = StatePicker.Items[StatePicker.SelectedIndex];
            var cities = await GeneralApi.GetCitiesAsync(_client, selectedState, "");
            CityPicker.Items.Clear();
            foreach (var wsCity in cities.List)
            {
                CityPicker.Items.Add(wsCity.Name);
            }
            CloseAllPopup();
        }

        private void PostalCode_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            switch (CultureInfo.CurrentCulture.Name)
            {
                case "pt-PT":
                    Util.CodigoPostalMask(sender, e);
                    break;

                default:
                    Util.CepMask(sender, e);
                    break;
            }
        }

        private async void PostalCode_Tapped(object sender, EventArgs e)
        {
            var isValid = true;
            if (!PostalCodeValidator.IsValid)
            {
                await DisplayAlert("Atenção", "O CEP digitado não é válido.", "Fechar");
                isValid = false;
            }

            if (isValid)
            {
                var loadingPage = new LoadingPage();
                await PopupNavigation.PushAsync(loadingPage);
                var postalCode = await GeneralApi.GetPostalCode(_client, PostalCode.Text);

                if (postalCode != null)
                {
                    if (!postalCode.Error.Any())
                    {
                        reloadCities = false;
                        for (var i = 0; i < StatePicker.Items.Count; i++)
                        {
                            var name = Util.GetStateCode(StatePicker.Items[i]);
                            if (name == postalCode.Estado)
                                StatePicker.SelectedIndex = i;
                        }

                        var selectedState = StatePicker.Items[StatePicker.SelectedIndex];
                        var cities = await GeneralApi.GetCitiesAsync(_client, selectedState, "");

                        foreach (var wsCity in cities.List)
                        {
                            CityPicker.Items.Add(wsCity.Name);
                        }

                        for (var i = 0; i < CityPicker.Items.Count; i++)
                        {
                            var name = CityPicker.Items[i];
                            if (name == postalCode.Municipio + " - " + postalCode.Estado)
                                CityPicker.SelectedIndex = i;
                        }
                        reloadCities = true;
                    }
                    else
                    {
                        var errorText = Util.GetWsErrorDescription(postalCode.Error.First());
                        await DisplayAlert("Atenção", errorText, "Fechar");
                    }
                }
                else
                {
                    await DisplayAlert("Atenção", "Não foi possível consultar seu CEP.", "Fechar");
                }
                CloseAllPopup();
            }
        }
    }
}