﻿using Amoenus.PclTimer;
using AppNivel.GrialTheme.Views.Logins;
using AppNivel.Helpers;
using AppNivel.Models;
using AppNivel.Resources;
using FAB.Forms;
using ImageCircle.Forms.Plugin.Abstractions;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using Plugin.Share;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class MainPage : ContentPage
    {
        private readonly HttpClient _client = new HttpClient();
        private ObservableCollection<WSEstablishment> _establishments;
        private int _currentPage = 1;
        private int _currentPageHot = 1;
        private bool _paginate = true;
        private ObservableCollection<WSEstablishment> _establishmentsSpecialList;
        private int _currentPageSpecialList = 1;
        private bool _paginateSpecialList = true;
        //private bool _dismissAlert = false;

        public MainPage()
        {
            InitializeComponent();
            Settings.CurrentPage = "Main";
            //Reset page view
            Settings.CurrentView = (int)ViewPages.Offers;
            //EstablishmentListImage.Source = Settings.EstablishmentListActiveImage;
            FavoritesImage.Source = Settings.FavoritesListInactiveImage;
            PositionListImage.Source = Settings.PositionListInactiveImage;
            OffersImage.Source = Settings.OffersListActiveImage;
            HotOffersImage.Source = Settings.HotOffersListInactiveImage;
            TourismImage.Source = Settings.TourismListInactiveImage;
            _currentPage = 1;
            _currentPageHot = 1;
            _currentPageSpecialList = 1;
            var factor = (Settings.DeviceWidth <= 480 && Settings.DeviceWidth > 0 ? 0.76 : 1);

            if (Settings.UserBalance > 999M)
            {
                UserBalance.FontSize = 38 * factor;
            }

            if (Settings.UserBalance > 9999M)
            {
                UserBalance.FontSize = 28 * factor;
            }

            if (Settings.UserBalance > 99999M)
            {
                UserBalance.FontSize = 18 * factor;
            }
            CrossConnectivity.Current.ConnectivityChanged += CurrentOnConnectivityChanged;
        }

        protected override async void OnAppearing()
        {
            YourBonifications.Text = Language.YourBonifications;
            if (Settings.CurrentPage != "Establishment")
            {
                Settings.EmailBeneficiary = string.Empty;

                try
                {
                    if (!Settings.TotalAccess && Settings.IsChildUser)
                    {
                        BalanceLayout.IsVisible = false;
                    }
                }
                catch (Exception ex)
                {
                    //resume...
                }

                _currentPage = 1;
                _currentPageHot = 1;
                _currentPageSpecialList = 1;

                try
                {
                    if (CrossConnectivity.Current.IsConnected)
                    {
                        var fabRippleColor = Color.FromHex("#213655");
                        var fabNormalColor = Color.FromHex("#458ccc");

                        try
                        {
                            if (!Settings.VerifiedUser)
                            {
                                var loadingPage = new LoadingPage();
                                await Navigation.PushPopupAsync(loadingPage);
                                var isExecutiveCommissioner = false;
                                isExecutiveCommissioner = await CustomerApi.GetIsExecutive(_client, Settings.UserId);
                                Settings.UserIsExecutiveCommissioner = isExecutiveCommissioner;
                                Settings.VerifiedUser = true;
                            }
                            if (Settings.UserIsExecutiveCommissioner)
                            {
                                //Troca cor do app
                                Application.Current.Resources["ToolbarColor"] = Color.FromHex("#CAA360");
                                Application.Current.Resources["BackgroundColor"] = Color.FromHex("#062C6A");
                                Application.Current.Resources["PrimaryChangeableColor"] = Color.FromHex("#fcd48e");
                                //Apply the colors to the main menu
                                //MenuHeader.BackgroundColor = Color.FromHex("#062C6A");
                                //navigationDrawer.BackgroundColor = Color.FromHex("#062C6A");
                                //MenuContent.BackgroundColor = Color.FromHex("#062C6A");
                                //MenuContentView.BackgroundColor = Color.FromHex("#062C6A");
                                //TxtUsername.TextColor = Color.FromHex("#fcd48e");
                                //LabelTransfer.TextColor = Color.FromHex("#fcd48e");
                                //LabelIndicate.TextColor = Color.FromHex("#fcd48e");
                                //LabelExecutive.TextColor = Color.FromHex("#fcd48e");
                                //LabelHowTo.TextColor = Color.FromHex("#fcd48e");
                                //LabelForgotPassword.TextColor = Color.FromHex("#fcd48e");
                                //LabelGenerateQr.TextColor = Color.FromHex("#fcd48e");
                                //LabelReward.TextColor = Color.FromHex("#fcd48e");
                                //LabelReceive.TextColor = Color.FromHex("#fcd48e");
                                //LabelPanel.TextColor = Color.FromHex("#fcd48e");
                                //LabelLogout.TextColor = Color.FromHex("#fcd48e");
                                //AppVersionLabel.TextColor = Color.FromHex("#fcd48e");
                                fabRippleColor = Color.FromHex("#CAA360");
                                fabNormalColor = Color.FromHex("#EABF70");
                                ToolBar.BackgroundColor = Color.FromHex("#CAA360");
                                BalanceLayout.BackgroundColor = Color.FromHex("#062C6A");
                                establishmentsLoading.Color = Color.FromHex("#062C6A");
                                establishmentsLoadingSpecialList.Color = Color.FromHex("#062C6A");
                                YourBonifications.TextColor = Color.FromHex("#fcd48e");
                                UserBalance.TextColor = Color.FromHex("#fcd48e");
                                LoadingBalance.Color = Color.FromHex("#fcd48e");
                            }
                            else
                            {
                                Application.Current.Resources["ToolbarColor"] = Color.FromHex("#458ccc");
                                Application.Current.Resources["BackgroundColor"] = Color.FromHex("#213655");
                                Application.Current.Resources["PrimaryChangeableColor"] = Color.FromHex("#FFFFFF");
                                //MenuHeader.BackgroundColor = Color.FromHex("#213655");
                                //navigationDrawer.BackgroundColor = Color.FromHex("#213655");
                                //MenuContent.BackgroundColor = Color.FromHex("#213655");
                                //MenuContentView.BackgroundColor = Color.FromHex("#213655");
                                //TxtUsername.TextColor = Color.FromHex("#FFFFFF");
                                //LabelTransfer.TextColor = Color.FromHex("#FFFFFF");
                                //LabelIndicate.TextColor = Color.FromHex("#FFFFFF");
                                //LabelExecutive.TextColor = Color.FromHex("#FFFFFF");
                                //LabelHowTo.TextColor = Color.FromHex("#FFFFFF");
                                //LabelForgotPassword.TextColor = Color.FromHex("#FFFFFF");
                                //LabelGenerateQr.TextColor = Color.FromHex("#FFFFFF");
                                //LabelReward.TextColor = Color.FromHex("#FFFFFF");
                                //LabelReceive.TextColor = Color.FromHex("#FFFFFF");
                                //LabelPanel.TextColor = Color.FromHex("#FFFFFF");
                                //LabelLogout.TextColor = Color.FromHex("#FFFFFF");
                                //AppVersionLabel.TextColor = Color.FromHex("#FFFFFF");
                                ToolBar.BackgroundColor = Color.FromHex("#458ccc");
                                BalanceLayout.BackgroundColor = Color.FromHex("#213655");
                                establishmentsLoading.Color = Color.FromHex("#213655");
                                establishmentsLoadingSpecialList.Color = Color.FromHex("#213655");
                                YourBonifications.TextColor = Color.FromHex("#FFFFFF");
                                UserBalance.TextColor = Color.FromHex("#FFFFFF");
                                LoadingBalance.Color = Color.FromHex("#FFFFFF");
                            }
                            await PopupNavigation.PopAllAsync();
                        }
                        catch (Exception ex)
                        {
                            Settings.VerifiedUser = false;
                            await
                                DisplayAlert("Atenção", "Não foi possível verificar alguns dados.",
                                    "Ok");
                        }

                        CityName.Text = Settings.CityFilter > 0 ? Settings.CityFilterName : "Todas as cidades";
                        try
                        {
                            var balance = await CustomerApi.GetBalance(_client);
                            if (balance != null)
                            {
                                //Verifica se cliente tem endereço e foi bonificado. Se ele já foi bonificado e não tem endereço, obrigar a preencher
                                if (balance.TotalBalance > 0 && !balance.UserHasAddress)
                                {
                                    await Navigation.PushModalAsync(new AddressRegistration());
                                }
                                Settings.UserBalance = balance.TotalBalance;
                                Settings.UserCashback = balance.TotalCashBack;
                                Settings.UserPoints = Convert.ToInt32(balance.TotalPoints);
                                Settings.UserPointsValue = balance.TotalPointsValue;
                                Settings.UserTotalTransaction = balance.TotalTransactions;
                                UserBalance.Text = Settings.UserBalance.ToString("C2");

                                if (Device.OS == TargetPlatform.Android)
                                {
                                    if (Settings.DeviceVersionCode >= 21)
                                    {
                                        try
                                        {
                                            var fabBtn = new FloatingActionButton()
                                            {
                                                Source = "config.png",
                                                IsVisible = true,
                                                Size = FabSize.Normal,
                                                RippleColor = fabRippleColor,
                                                NormalColor = fabNormalColor,
                                                HasShadow = true,
                                            };
                                            MainLayout.Children.Add(
                                                fabBtn,
                                                yConstraint:
                                                Constraint.RelativeToParent((parent) => parent.Height - 70),
                                                xConstraint: Constraint.RelativeToParent((parent) => parent.Width - 70)
                                            );

                                            fabBtn.Clicked += ConfigTapGestureRecognizer_OnTapped;
                                            fabBtn.IsVisible = true;
                                            configMenu.IsVisible = false;
                                        }
                                        catch (Exception ex)
                                        {
                                            configMenu.IsVisible = true;
                                            CitiesSL.Margin = new Thickness(10, 0, 0, 0);
                                        }
                                    }
                                    else
                                    {
                                        configMenu.IsVisible = true;
                                        CitiesSL.Margin = new Thickness(10, 0, 0, 0);
                                    }
                                }
                                else
                                {
                                    CitiesSL.Margin = new Thickness(10, 0, 0, 0);
                                    configMenu.IsVisible = true;
                                }
                            }
                            else
                            {
                                await
                                    DisplayAlert("Atenção",
                                        "Não foi possível consultar seu saldo. Tente novamente mais tarde.",
                                        "Ok");
                                Settings.UserBalance = 0;
                                Settings.UserCashback = 0;
                                Settings.UserPoints = 0;
                                Settings.UserTotalTransaction = 0;
                            }
                        }
                        catch (Exception ex)
                        {
                            await
                                DisplayAlert("Atenção",
                                    "Não foi possível consultar seu saldo. Tente novamente mais tarde.",
                                    "Ok");
                            Settings.UserBalance = 0;
                            Settings.UserCashback = 0;
                            Settings.UserPoints = 0;
                            Settings.UserTotalTransaction = 0;
                        }

                        //LabelHeader.Text = Settings.BusinessActivityFilter > 0 ? Settings.BusinessActivityFilterName : "Estabelecimentos";

                        LoadingBalance.IsRunning = false;
                        LoadingBalance.IsVisible = false;
                        UserBalance.IsVisible = true;
                        //UserBalanceCashback.IsVisible = true;
                        //UserBalancePoints.IsVisible = true;

                        if (Settings.CurrentView == (int)ViewPages.Tourism)
                        {
                            ShowSpecialList(false);
                            try
                            {
                                var establishmentList =
                                    await
                                        EstablishmentApi.GetSpecialList(_client, page: _currentPageSpecialList,
                                            businessActivityId: Settings.BusinessActivityFilter,
                                            cityId: Settings.CityFilter,
                                            userLat: Settings.UserLatitude, userLong: Settings.UserLongitude);
                                LoadSpecialList(establishmentList);
                            }
                            catch (Exception ex)
                            {
                                ShowSpecialList(false);
                            }
                        }
                        else if (Settings.CurrentView == (int)ViewPages.HotOffer)
                        {
                            ShowHotList(false);
                            try
                            {
                                //Trazendo ofertas primeiro
                                if (CrossConnectivity.Current.IsConnected)
                                {
                                    _currentPageHot = 1;
                                    SpecialListTab.IsVisible = false;
                                    EstablishmentsTab.IsVisible = false;
                                    EstablishmentsHotOfferTab.IsVisible = true;
                                    if ((EstablishmentListImage.Source as FileImageSource)?.File !=
                                        Settings.EstablishmentListInactiveImage)
                                        EstablishmentListImage.Source = Settings.EstablishmentListInactiveImage;
                                    if ((FavoritesImage.Source as FileImageSource)?.File !=
                                        Settings.FavoritesListInactiveImage)
                                        FavoritesImage.Source = Settings.FavoritesListInactiveImage;
                                    if ((OffersImage.Source as FileImageSource)?.File !=
                                        Settings.OffersListInactiveImage)
                                        OffersImage.Source = Settings.OffersListInactiveImage;
                                    if ((HotOffersImage.Source as FileImageSource)?.File !=
                                        Settings.HotOffersListActiveImage)
                                        HotOffersImage.Source = Settings.HotOffersListActiveImage;
                                    if ((PositionListImage.Source as FileImageSource)?.File !=
                                        Settings.PositionListInactiveImage)
                                        PositionListImage.Source = Settings.PositionListInactiveImage;
                                    if ((TourismImage.Source as FileImageSource)?.File !=
                                        Settings.TourismListInactiveImage)
                                        TourismImage.Source = Settings.TourismListInactiveImage;
                                    Settings.CurrentView = (int)ViewPages.HotOffer;
                                    var establishmentList = await EstablishmentApi.GetAsyncNative(_client,
                                        page: _currentPageHot,
                                        businessActivityId: Settings.BusinessActivityFilter, isOffer: true,
                                        cityId: Settings.CityFilter,
                                        userLat: Settings.UserLatitude,
                                        userLong: Settings.UserLongitude, isHotOffer: true);
                                    LoadHotList(establishmentList);
                                }
                                else
                                {
                                    Util.NotConnectedMessage(this);
                                }
                            }
                            catch (Exception ex)
                            {
                                ShowHotList(false);
                            }
                        }
                        else
                        {
                            ShowList(false);
                            try
                            {
                                //Trazendo ofertas primeiro
                                if (CrossConnectivity.Current.IsConnected)
                                {
                                    _currentPage = 1;
                                    SpecialListTab.IsVisible = false;
                                    EstablishmentsHotOfferTab.IsVisible = false;
                                    EstablishmentsTab.IsVisible = true;
                                    if ((EstablishmentListImage.Source as FileImageSource)?.File !=
                                        Settings.EstablishmentListInactiveImage)
                                        EstablishmentListImage.Source = Settings.EstablishmentListInactiveImage;
                                    if ((FavoritesImage.Source as FileImageSource)?.File !=
                                        Settings.FavoritesListInactiveImage)
                                        FavoritesImage.Source = Settings.FavoritesListInactiveImage;
                                    if ((OffersImage.Source as FileImageSource)?.File != Settings.OffersListActiveImage)
                                        OffersImage.Source = Settings.OffersListActiveImage;
                                    if ((HotOffersImage.Source as FileImageSource)?.File !=
                                        Settings.HotOffersListInactiveImage)
                                        HotOffersImage.Source = Settings.HotOffersListInactiveImage;
                                    if ((PositionListImage.Source as FileImageSource)?.File !=
                                        Settings.PositionListInactiveImage)
                                        PositionListImage.Source = Settings.PositionListInactiveImage;
                                    if ((TourismImage.Source as FileImageSource)?.File !=
                                        Settings.TourismListInactiveImage)
                                        TourismImage.Source = Settings.TourismListInactiveImage;
                                    Settings.CurrentView = (int)ViewPages.Offers;
                                    var establishmentList = await EstablishmentApi.GetAsyncNative(_client,
                                        page: _currentPage,
                                        businessActivityId: Settings.BusinessActivityFilter, isOffer: true,
                                        search: searchText.Text, cityId: Settings.CityFilter,
                                        userLat: Settings.UserLatitude,
                                        userLong: Settings.UserLongitude);
                                    LoadList(establishmentList);
                                }
                                else
                                {
                                    Util.NotConnectedMessage(this);
                                }
                            }
                            catch (Exception ex)
                            {
                                ShowList(false);
                            }
                        }

                        //Get App Version
                        //var appVersion = await GeneralApi.GetAppVersion(_client);

                        //if (appVersion != null)
                        //{
                        //    if (appVersion.Version != CrossVersion.Current.Version)
                        //    {
                        //        if (appVersion.Notify)
                        //        {
                        //            if (appVersion.Mandatory)
                        //            {
                        //                var response = await DisplayAlert("Atenção", appVersion.Message, "Atualizar", "Sair");
                        //                if (response)
                        //                {
                        //                    OpenStoretoUpdate(appVersion);
                        //                }
                        //                else
                        //                {
                        //                    Settings.Logout();
                        //                    Application.Current.MainPage = new NavigationPage(new Presentation());
                        //                    await Navigation.PopToRootAsync(true);
                        //                }
                        //            }
                        //            else
                        //            {
                        //                if (!_dismissAlert)
                        //                {
                        //                    var response =
                        //                        await DisplayAlert("Atenção", appVersion.Message, "Atualizar", "Depois");
                        //                    if (response)
                        //                    {
                        //                        OpenStoretoUpdate(appVersion);
                        //                    }
                        //                    else
                        //                    {
                        //                        _dismissAlert = true;
                        //                    }
                        //                }
                        //            }
                        //        }
                        //    }
                        //}
                    }
                    else
                    {
                        ShowList(false);
                        Util.NotConnectedMessage(this);
                    }
                }
                catch (Exception ex)
                {
                    //resume...
                }
            }

            //MENU SETTINGS
            //LineReward.IsVisible = PanelReward.IsVisible = Settings.UserType == 2;
            //LineReceive.IsVisible = PanelReceive.IsVisible = Settings.UserType == 2;
            //TxtUsername.Text = Settings.Username;
            //AppVersionLabel.Text = "Versão " + CrossVersion.Current.Version;
            //if (!string.IsNullOrEmpty(Settings.UserPicture))
            //{
            //    ProfileImage.Source = "https://www.nivel.com.br/" + Settings.UserPicture;
            //}
            //if (!Settings.TotalAccess && Settings.IsChildUser)
            //{
            //    LineForgot.IsVisible = false;
            //    Forgot.IsVisible = false;
            //    UsernameTgr.Tapped -= Username_OnTapped;
            //    UsernameTgr.Tapped += UsernameTgr_Tapped;
            //}

            //if (Settings.UserIsExecutiveCommissioner)
            //{
            //    MenuWannaBeExecutive.IsVisible = false;
            //    WannaBeExecutiveLine.IsVisible = false;
            //    TransferLine.IsVisible = true;
            //    Transfer.IsVisible = true;
            //}

            //END MENU SETTINGS
            base.OnAppearing();
        }

        private void CurrentOnConnectivityChanged(object sender, ConnectivityChangedEventArgs connectivityChangedEventArgs)
        {
            //if (!connectivityChangedEventArgs.IsConnected)
            //{
            //    Application.Current.MainPage = new NavigationPage(new NotConnectedPage());
            //}
            //else
            //{
            //    Application.Current.MainPage = new NavigationPage(new MainPage());
            //}
        }

        private void ShowList(bool show = true)
        {
            EstablishmentsListView.IsVisible = show;
        }

        private void ShowHotList(bool show = true)
        {
            EstablishmentsHotOfferListView.IsVisible = show;
        }

        private void OpenStoretoUpdate(WSAppVersion appVersion)
        {
            switch (Device.OS)
            {
                case TargetPlatform.iOS:
                    Device.OpenUri(new Uri(appVersion.AppStoreLink));
                    break;

                case TargetPlatform.Android:
                    Device.OpenUri(new Uri(appVersion.GooglePlayStoreLink));
                    break;

                case TargetPlatform.Other:
                    break;

                case TargetPlatform.WinPhone:
                    break;

                case TargetPlatform.Windows:
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private async void ListView_OnRefreshing(object sender, EventArgs e)
        {
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    _currentPage = 1;
                    ShowList(false);
                    var establishmentList = await EstablishmentApi.GetAsyncNative(_client,
                        businessActivityId: Settings.BusinessActivityFilter,
                        isFavorite: (Settings.CurrentView == (int)ViewPages.Favorites),
                        isOffer: (Settings.CurrentView == (int)ViewPages.Offers),
                        search: searchText.Text,
                        cityId: Settings.CityFilter,
                        userLat: Settings.UserLatitude,
                        userLong: Settings.UserLongitude);
                    LoadList(establishmentList);
                }
                else
                {
                    ShowList(false);
                    Util.NotConnectedMessage(this);
                }
                EstablishmentsListView.EndRefresh();
            }
            catch (Exception ex)
            {
                EstablishmentsListView.EndRefresh();
            }
        }

        private async void HotListView_OnRefreshing(object sender, EventArgs e)
        {
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    _currentPageHot = 1;
                    ShowHotList(false);
                    var establishmentList = await EstablishmentApi.GetAsyncNative(_client,
                        businessActivityId: Settings.BusinessActivityFilter,
                        isFavorite: (Settings.CurrentView == (int)ViewPages.Favorites),
                        isOffer: (Settings.CurrentView == (int)ViewPages.Offers),
                        cityId: Settings.CityFilter,
                        userLat: Settings.UserLatitude,
                        userLong: Settings.UserLongitude, isHotOffer: true);
                    LoadHotList(establishmentList);
                }
                else
                {
                    ShowHotList(false);
                    Util.NotConnectedMessage(this);
                }
                EstablishmentsHotOfferListView.EndRefresh();
            }
            catch (Exception ex)
            {
                EstablishmentsHotOfferListView.EndRefresh();
            }
        }

        private void EstablishmentsListView_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            EstablishmentsListView.SelectedItem = null;
        }

        private void EstablishmentsHotOfferListView_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            EstablishmentsHotOfferListView.SelectedItem = null;
        }

        private async void EstablishmentsListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var eId = e.Item as WSEstablishment;
                var loadingPage = new LoadingPage();
                await Navigation.PushPopupAsync(loadingPage);
                var establishmentAsync = await EstablishmentApi.GetAsync(_client, id: eId.Id);
                var establishment = establishmentAsync.List.FirstOrDefault();
                var establishmentDetailPage = new Pages.EstablishmentDetail(establishment);
                //NavigationPage.SetHasNavigationBar(establishmentDetailPage, false);
                await PopupNavigation.PopAllAsync();
                await this.Navigation.PushAsync(establishmentDetailPage, true);
            }
            catch (Exception ex)
            {
                var errorForDebug = ex.Message;
            }
        }

        private void DetalheSaldoTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Navigation.PushAsync(new BalancePage());
        }

        private async void EstablishmentsListView_OnItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            try
            {
                if (establishmentsLoading.IsRunning)
                    return;
                if (!_paginate) return;

                if (e.Item as WSEstablishment != _establishments[_establishments.Count - 1] ||
                    _establishments.Count < Settings.ItensPerPage) return;
                if (CrossConnectivity.Current.IsConnected)
                {
                    var loadingPage = new LoadingPage();
                    await Navigation.PushPopupAsync(loadingPage);
                    _currentPage += 1;
                    var establishmentList = await EstablishmentApi.GetAsyncNative(_client, page: _currentPage,
                        businessActivityId: Settings.BusinessActivityFilter,
                        isFavorite: (Settings.CurrentView == (int)ViewPages.Favorites),
                        isOffer: (Settings.CurrentView == (int)ViewPages.Offers),
                        search: searchText.Text,
                        cityId: Settings.CityFilter,
                        userLat: Settings.UserLatitude,
                        userLong: Settings.UserLongitude);
                    _paginate = establishmentList.TotalPages > _currentPage;
                    await PopupNavigation.PopAllAsync();
                    LoadPageList(establishmentList);
                }
                else
                {
                    Util.NotConnectedMessage(this);
                }
            }
            catch (Exception ex)
            {
                //resume...
            }
        }

        private async void EstablishmentsHotOfferListView_OnItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            try
            {
                if (establishmentsLoading.IsRunning)
                    return;
                if (!_paginate) return;

                if (e.Item as WSEstablishment != _establishments[_establishments.Count - 1] ||
                    _establishments.Count < Settings.ItensPerPage) return;
                if (CrossConnectivity.Current.IsConnected)
                {
                    var loadingPage = new LoadingPage();
                    await Navigation.PushPopupAsync(loadingPage);
                    _currentPageHot += 1;
                    var establishmentList = await EstablishmentApi.GetAsyncNative(_client, page: _currentPageHot,
                        businessActivityId: Settings.BusinessActivityFilter,
                        isFavorite: (Settings.CurrentView == (int)ViewPages.Favorites),
                        isOffer: (Settings.CurrentView == (int)ViewPages.Offers),
                        search: searchText.Text,
                        cityId: Settings.CityFilter,
                        userLat: Settings.UserLatitude,
                        userLong: Settings.UserLongitude);
                    _paginate = establishmentList.TotalPages > _currentPageHot;
                    await PopupNavigation.PopAllAsync();
                    LoadPageList(establishmentList);
                }
                else
                {
                    Util.NotConnectedMessage(this);
                }
            }
            catch (Exception ex)
            {
                //resume...
            }
        }

        private async void LoadList(EstablishmentList establishmentList)
        {
            try
            {
                if (establishmentList != null)
                {
                    if (string.IsNullOrEmpty(establishmentList.ErrorText))
                    {
                        _paginate = establishmentList.TotalPages > 1 && _currentPage < establishmentList.TotalPages;
                        _establishments = new ObservableCollection<WSEstablishment>(establishmentList.List);
                        EstablishmentsListView.ItemsSource = _establishments;
                        panelIndicate.IsVisible = !_establishments.Any() && Settings.CurrentView == (int)ViewPages.Offers;
                        TextEstabelecimentos.IsVisible = !_establishments.Any() && !panelIndicate.IsVisible;
                        ShowList();
                    }
                    else
                    {
                        ShowList(false);
                        await DisplayAlert("Atenção", "Sem estabelecimentos cadastrados", "Ok");
                    }
                }
                else
                {
                    ShowList(false);
                    await DisplayAlert("Atenção", "Sem estabelecimentos cadastrados", "Ok");
                }
            }
            catch (Exception ex)
            {
                //resume...
            }
        }

        private async void LoadHotList(EstablishmentList establishmentList)
        {
            try
            {
                if (establishmentList != null)
                {
                    if (string.IsNullOrEmpty(establishmentList.ErrorText))
                    {
                        _paginate = establishmentList.TotalPages > 1 && _currentPageHot < establishmentList.TotalPages;
                        _establishments = new ObservableCollection<WSEstablishment>(establishmentList.List);
                        EstablishmentsHotOfferListView.ItemsSource = _establishments;
                        TextEstabelecimentosHotOffer.IsVisible = !_establishments.Any();
                        ShowHotList();
                    }
                    else
                    {
                        ShowHotList(false);
                        await DisplayAlert("Atenção", "Sem estabelecimentos cadastrados", "Ok");
                    }
                }
                else
                {
                    ShowHotList(false);
                    await DisplayAlert("Atenção", "Sem estabelecimentos cadastrados", "Ok");
                }
            }
            catch (Exception ex)
            {
                //resume...
            }
        }

        private async void LoadPageList(EstablishmentList establishmentList)
        {
            try
            {
                if (establishmentList != null)
                {
                    if (string.IsNullOrEmpty(establishmentList.ErrorText))
                    {
                        foreach (var establishment in new ObservableCollection<WSEstablishment>(establishmentList.List))
                        {
                            _establishments.Add(establishment);
                        }
                        EstablishmentsListView.ItemsSource = _establishments;
                        ShowList();
                    }
                    else
                    {
                        ShowList(false);
                        await DisplayAlert("Atenção", "Sem estabelecimentos cadastrados", "Ok");
                    }
                }
                else
                {
                    ShowList(false);
                    await DisplayAlert("Atenção", "Sem estabelecimentos cadastrados", "Ok");
                }
            }
            catch (Exception ex)
            {
                //resume...
            }
        }

        private void ConfigTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            try
            {
                //navigationDrawer.ToggleDrawer();
                Navigation.PushAsync(new SettingsPage(), true);
            }
            catch (Exception ex)
            {
                //resume...
            }
        }

        private async void EstablishmentListTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    if (Settings.CurrentView == (int)ViewPages.Establishments) return;
                    _currentPage = 1;
                    SpecialListTab.IsVisible = false;
                    EstablishmentsTab.IsVisible = true;
                    EstablishmentsHotOfferTab.IsVisible = false;
                    if ((EstablishmentListImage.Source as FileImageSource)?.File != Settings.EstablishmentListActiveImage)
                        EstablishmentListImage.Source = Settings.EstablishmentListActiveImage;
                    if ((FavoritesImage.Source as FileImageSource)?.File != Settings.FavoritesListInactiveImage)
                        FavoritesImage.Source = Settings.FavoritesListInactiveImage;
                    if ((OffersImage.Source as FileImageSource)?.File != Settings.OffersListInactiveImage)
                        OffersImage.Source = Settings.OffersListInactiveImage;
                    if ((HotOffersImage.Source as FileImageSource)?.File != Settings.HotOffersListInactiveImage)
                        HotOffersImage.Source = Settings.HotOffersListInactiveImage;
                    if ((PositionListImage.Source as FileImageSource)?.File != Settings.PositionListInactiveImage)
                        PositionListImage.Source = Settings.PositionListInactiveImage;
                    if ((TourismImage.Source as FileImageSource)?.File != Settings.TourismListInactiveImage)
                        TourismImage.Source = Settings.TourismListInactiveImage;
                    await EstablishmentListImage.ScaleTo(0.5, 100, Easing.BounceOut);
                    await EstablishmentListImage.ScaleTo(1, 100, Easing.BounceOut);
                    Settings.CurrentView = (int)ViewPages.Establishments;
                    ShowList(false);
                    var establishmentList = await EstablishmentApi.GetAsyncNative(_client, page: _currentPage,
                        businessActivityId: Settings.BusinessActivityFilter, search: searchText.Text, cityId: Settings.CityFilter,
                        userLat: Settings.UserLatitude,
                        userLong: Settings.UserLongitude);
                    LoadList(establishmentList);
                }
                else
                {
                    Util.NotConnectedMessage(this);
                }
            }
            catch (Exception ex)
            {
                //resume...
            }
        }

        private async void FavoritesTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    if (Settings.CurrentView == (int)ViewPages.Favorites) return;
                    _currentPage = 1;
                    SpecialListTab.IsVisible = false;
                    EstablishmentsTab.IsVisible = true;
                    EstablishmentsHotOfferTab.IsVisible = false;
                    if ((EstablishmentListImage.Source as FileImageSource)?.File != Settings.EstablishmentListInactiveImage)
                        EstablishmentListImage.Source = Settings.EstablishmentListInactiveImage;
                    if ((FavoritesImage.Source as FileImageSource)?.File != Settings.FavoritesListActiveImage)
                        FavoritesImage.Source = Settings.FavoritesListActiveImage;
                    if ((OffersImage.Source as FileImageSource)?.File != Settings.OffersListInactiveImage)
                        OffersImage.Source = Settings.OffersListInactiveImage;
                    if ((HotOffersImage.Source as FileImageSource)?.File != Settings.HotOffersListInactiveImage)
                        HotOffersImage.Source = Settings.HotOffersListInactiveImage;
                    if ((PositionListImage.Source as FileImageSource)?.File != Settings.PositionListInactiveImage)
                        PositionListImage.Source = Settings.PositionListInactiveImage;
                    if ((TourismImage.Source as FileImageSource)?.File != Settings.TourismListInactiveImage)
                        TourismImage.Source = Settings.TourismListInactiveImage;
                    await FavoritesImage.ScaleTo(0.5, 100, Easing.BounceOut);
                    await FavoritesImage.ScaleTo(1, 100, Easing.BounceOut);
                    Settings.CurrentView = (int)ViewPages.Favorites;
                    ShowList(false);
                    var establishmentList = await EstablishmentApi.GetAsyncNative(_client, page: _currentPage,
                        businessActivityId: Settings.BusinessActivityFilter, isFavorite: true, search: searchText.Text, cityId: Settings.CityFilter,
                        userLat: Settings.UserLatitude,
                        userLong: Settings.UserLongitude);
                    LoadList(establishmentList);
                }
                else
                {
                    Util.NotConnectedMessage(this);
                }
            }
            catch (Exception ex)
            {
                //resume...
            }
        }

        private async void OffersTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    if (Settings.CurrentView == (int)ViewPages.Offers) return;
                    _currentPage = 1;
                    SpecialListTab.IsVisible = false;
                    EstablishmentsTab.IsVisible = true;
                    EstablishmentsHotOfferTab.IsVisible = false;
                    if ((EstablishmentListImage.Source as FileImageSource)?.File != Settings.EstablishmentListInactiveImage)
                        EstablishmentListImage.Source = Settings.EstablishmentListInactiveImage;
                    if ((FavoritesImage.Source as FileImageSource)?.File != Settings.FavoritesListInactiveImage)
                        FavoritesImage.Source = Settings.FavoritesListInactiveImage;
                    if ((OffersImage.Source as FileImageSource)?.File != Settings.OffersListActiveImage)
                        OffersImage.Source = Settings.OffersListActiveImage;
                    if ((HotOffersImage.Source as FileImageSource)?.File != Settings.HotOffersListInactiveImage)
                        HotOffersImage.Source = Settings.HotOffersListInactiveImage;
                    if ((PositionListImage.Source as FileImageSource)?.File != Settings.PositionListInactiveImage)
                        PositionListImage.Source = Settings.PositionListInactiveImage;
                    if ((TourismImage.Source as FileImageSource)?.File != Settings.TourismListInactiveImage)
                        TourismImage.Source = Settings.TourismListInactiveImage;
                    await OffersImage.ScaleTo(0.5, 100, Easing.BounceOut);
                    await OffersImage.ScaleTo(1, 100, Easing.BounceOut);
                    Settings.CurrentView = (int)ViewPages.Offers;
                    ShowList(false);
                    var establishmentList = await EstablishmentApi.GetAsyncNative(_client, page: _currentPage,
                        businessActivityId: Settings.BusinessActivityFilter, isOffer: true, search: searchText.Text, cityId: Settings.CityFilter,
                        userLat: Settings.UserLatitude,
                        userLong: Settings.UserLongitude);
                    LoadList(establishmentList);
                }
                else
                {
                    Util.NotConnectedMessage(this);
                }
            }
            catch (Exception ex)
            {
                //resume...
            }
        }

        private async void HotOffersTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    if (Settings.CurrentView == (int)ViewPages.HotOffer) return;
                    _currentPageHot = 1;
                    SpecialListTab.IsVisible = false;
                    EstablishmentsTab.IsVisible = false;
                    EstablishmentsHotOfferTab.IsVisible = true;
                    if ((EstablishmentListImage.Source as FileImageSource)?.File != Settings.EstablishmentListInactiveImage)
                        EstablishmentListImage.Source = Settings.EstablishmentListInactiveImage;
                    if ((FavoritesImage.Source as FileImageSource)?.File != Settings.FavoritesListInactiveImage)
                        FavoritesImage.Source = Settings.FavoritesListInactiveImage;
                    if ((OffersImage.Source as FileImageSource)?.File != Settings.OffersListInactiveImage)
                        OffersImage.Source = Settings.OffersListInactiveImage;
                    if ((HotOffersImage.Source as FileImageSource)?.File != Settings.HotOffersListActiveImage)
                        HotOffersImage.Source = Settings.HotOffersListActiveImage;
                    if ((PositionListImage.Source as FileImageSource)?.File != Settings.PositionListInactiveImage)
                        PositionListImage.Source = Settings.PositionListInactiveImage;
                    if ((TourismImage.Source as FileImageSource)?.File != Settings.TourismListInactiveImage)
                        TourismImage.Source = Settings.TourismListInactiveImage;
                    await HotOffersImage.ScaleTo(0.5, 100, Easing.BounceOut);
                    await HotOffersImage.ScaleTo(1, 100, Easing.BounceOut);
                    Settings.CurrentView = (int)ViewPages.HotOffer;
                    ShowHotList(false);
                    var establishmentList = await EstablishmentApi.GetAsyncNative(_client, page: _currentPageHot,
                        businessActivityId: Settings.BusinessActivityFilter, isOffer: true, cityId: Settings.CityFilter,
                        userLat: Settings.UserLatitude,
                        userLong: Settings.UserLongitude, isHotOffer: true);
                    LoadHotList(establishmentList);
                }
                else
                {
                    Util.NotConnectedMessage(this);
                }
            }
            catch (Exception ex)
            {
                //resume...
            }
        }

        private void EstablishmentsListView_OnItemDisappearing(object sender, ItemVisibilityEventArgs e)
        {
            if (establishmentsLoading.IsRunning)
                return;
        }

        private void EstablishmentsHotOfferListView_OnItemDisappearing(object sender, ItemVisibilityEventArgs e)
        {
            if (establishmentsHotOfferLoading.IsRunning)
                return;
        }

        private void BalanceLayout_OnTapped(object sender, EventArgs e)
        {
            Navigation.PushAsync(new BalancePage());
        }

        private void CitiesTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            var citiesPage = new EstablishmentFilterCitySearch();
            Navigation.PushAsync(citiesPage, true);
        }

        private void IndicateGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.nivel.com.br/site/executivo-nivel/"));
        }

        private void ShowCategories_OnTaped(object sender, EventArgs e)
        {
            var categoriesPage = new EstablishmentFilter();
            //NavigationPage.SetHasNavigationBar(establishmentDetailPage, false);
            this.Navigation.PushAsync(categoriesPage, true);
        }

        private async void PositionListTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    if (Settings.CurrentView == (int)ViewPages.PositionFilter) return;
                    var loadingPage = new LoadingPage();
                    await Navigation.PushPopupAsync(loadingPage);
                    SpecialListTab.IsVisible = false;
                    EstablishmentsTab.IsVisible = true;
                    EstablishmentsHotOfferTab.IsVisible = false;
                    var orderByPosition = false;
#if DEBUG
                    Settings.UserLatitude = -20.8011502D;
                    Settings.UserLongitude = -49.3971176D;
#else
                    try
                    {
                        var locator = CrossGeolocator.Current;
                        if (locator.IsGeolocationAvailable)
                        {
                            locator.DesiredAccuracy = 50;
                            var position = await locator.GetPositionAsync(10000);
                            if (!position.Latitude.Equals(0D) || !position.Longitude.Equals(0D))
                            {
                                orderByPosition = true;
                                Settings.UserLatitude = position.Latitude;
                                Settings.UserLongitude = position.Longitude;
                            }
                            else
                            {
                                await DisplayAlert("Atenção", "Ative sua localização para pesquisar estabelecimentos próximos!", "Ok");
                            }
                        }
                        else
                        {
                            await DisplayAlert("Atenção", "Ative sua localização para pesquisar estabelecimentos próximos!", "Ok");
                        }
                    }
                    catch (Exception ex)
                    {
                        await DisplayAlert("Atenção", "Ative sua localização para pesquisar estabelecimentos próximos!", "Ok");
                        Settings.UserLatitude = 0D;
                        Settings.UserLongitude = 0D;
                    }
#endif

                    await PopupNavigation.PopAllAsync();

                    _currentPage = 1;
                    if (orderByPosition)
                    {
                        if ((PositionListImage.Source as FileImageSource)?.File != Settings.PositionListActiveImage)
                            PositionListImage.Source = Settings.PositionListActiveImage;
                        if ((EstablishmentListImage.Source as FileImageSource)?.File !=
                            Settings.EstablishmentListInactiveImage)
                            EstablishmentListImage.Source = Settings.EstablishmentListInactiveImage;
                        if ((FavoritesImage.Source as FileImageSource)?.File != Settings.FavoritesListInactiveImage)
                            FavoritesImage.Source = Settings.FavoritesListInactiveImage;
                        if ((OffersImage.Source as FileImageSource)?.File != Settings.OffersListInactiveImage)
                            OffersImage.Source = Settings.OffersListInactiveImage;
                        if ((HotOffersImage.Source as FileImageSource)?.File != Settings.HotOffersListInactiveImage)
                            HotOffersImage.Source = Settings.HotOffersListInactiveImage;
                        if ((TourismImage.Source as FileImageSource)?.File != Settings.TourismListInactiveImage)
                            TourismImage.Source = Settings.TourismListInactiveImage;
                        await PositionListImage.ScaleTo(0.5, 100, Easing.BounceOut);
                        await PositionListImage.ScaleTo(1, 100, Easing.BounceOut);
                        Settings.CurrentView = (int)ViewPages.PositionFilter;
                        ShowList(false);
                        var establishmentList = await EstablishmentApi.GetAsyncNative(_client, page: _currentPage,
                            businessActivityId: Settings.BusinessActivityFilter, search: searchText.Text,
                            cityId: Settings.CityFilter,
                            userLat: Settings.UserLatitude,
                            userLong: Settings.UserLongitude);
                        LoadList(establishmentList);
                    }
                    else
                    {
                        if ((EstablishmentListImage.Source as FileImageSource)?.File != Settings.EstablishmentListInactiveImage)
                            EstablishmentListImage.Source = Settings.EstablishmentListInactiveImage;
                        if ((FavoritesImage.Source as FileImageSource)?.File != Settings.FavoritesListInactiveImage)
                            FavoritesImage.Source = Settings.FavoritesListInactiveImage;
                        if ((OffersImage.Source as FileImageSource)?.File != Settings.OffersListActiveImage)
                            OffersImage.Source = Settings.OffersListActiveImage;
                        if ((HotOffersImage.Source as FileImageSource)?.File != Settings.HotOffersListInactiveImage)
                            HotOffersImage.Source = Settings.HotOffersListInactiveImage;
                        if ((PositionListImage.Source as FileImageSource)?.File != Settings.PositionListInactiveImage)
                            PositionListImage.Source = Settings.PositionListInactiveImage;
                        if ((TourismImage.Source as FileImageSource)?.File != Settings.TourismListInactiveImage)
                            TourismImage.Source = Settings.TourismListInactiveImage;
                        await OffersImage.ScaleTo(0.5, 100, Easing.BounceOut);
                        await OffersImage.ScaleTo(1, 100, Easing.BounceOut);
                        Settings.CurrentView = (int)ViewPages.Offers;
                        ShowList(false);
                        var establishmentList = await EstablishmentApi.GetAsyncNative(_client, page: _currentPage,
                            businessActivityId: Settings.BusinessActivityFilter, search: searchText.Text, cityId: Settings.CityFilter,
                            userLat: Settings.UserLatitude,
                            userLong: Settings.UserLongitude);
                        LoadList(establishmentList);
                    }
                }
                else
                {
                    Util.NotConnectedMessage(this);
                }
            }
            catch (Exception ex)
            {
                //resume...
            }
        }

        //Turismo e Lazer

        private void EstablishmentsListViewSpecialList_OnItemDisappearing(object sender, ItemVisibilityEventArgs e)
        {
            if (establishmentsLoadingSpecialList.IsRunning)
                return;
        }

        private async void LoadSpecialList(EstablishmentList establishmentListSpecialList)
        {
            try
            {
                if (establishmentListSpecialList != null)
                {
                    if (string.IsNullOrEmpty(establishmentListSpecialList.ErrorText))
                    {
                        _paginateSpecialList = establishmentListSpecialList.TotalPages > 1 && _currentPageSpecialList < establishmentListSpecialList.TotalPages;
                        _establishmentsSpecialList = new ObservableCollection<WSEstablishment>(establishmentListSpecialList.List);
                        EstablishmentsListViewSpecialList.ItemsSource = _establishmentsSpecialList;
                        TextEstabelecimentosSpecialList.IsVisible = !_establishmentsSpecialList.Any();
                        ShowSpecialList();
                    }
                    else
                    {
                        ShowSpecialList(false);
                        await DisplayAlert("Atenção", "Sem pontos turísticos cadastrados", "Ok");
                    }
                }
                else
                {
                    ShowSpecialList(false);
                    await DisplayAlert("Atenção", "Sem pontos turísticos cadastrados", "Ok");
                }
            }
            catch (Exception ex)
            {
                //resume...
            }
        }

        private async void LoadPageSpecialList(EstablishmentList establishmentSpecialList)
        {
            try
            {
                if (establishmentSpecialList != null)
                {
                    if (string.IsNullOrEmpty(establishmentSpecialList.ErrorText))
                    {
                        foreach (var establishment in new ObservableCollection<WSEstablishment>(establishmentSpecialList.List))
                        {
                            _establishmentsSpecialList.Add(establishment);
                        }
                        EstablishmentsListViewSpecialList.ItemsSource = _establishmentsSpecialList;
                        ShowSpecialList();
                    }
                    else
                    {
                        ShowSpecialList(false);
                        await DisplayAlert("Atenção", "Sem pontos turísticos cadastrados", "Ok");
                    }
                }
                else
                {
                    ShowSpecialList(false);
                    await DisplayAlert("Atenção", "Sem pontos turísticos cadastrados", "Ok");
                }
            }
            catch (Exception ex)
            {
                //resume...
            }
        }

        private void ShowSpecialList(bool show = true)
        {
            EstablishmentsListViewSpecialList.IsVisible = show;
        }

        private async void SearchBarSpecialList_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    _currentPageSpecialList = 1;
                    var establishmentList = await EstablishmentApi.GetSpecialList(_client, search: e.NewTextValue,
                        businessActivityId: Settings.BusinessActivityFilter,
                        isFavorite: false,
                        isOffer: false,
                        cityId: Settings.CityFilter,
                        userLat: Settings.UserLatitude,
                        userLong: Settings.UserLongitude);
                    LoadSpecialList(establishmentList);
                }
                else
                {
                    Util.NotConnectedMessage(this);
                }
            }
            catch (Exception ex)
            {
                //resume...
            }
        }

        private async void ListViewSpecialList_OnRefreshing(object sender, EventArgs e)
        {
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    _currentPageSpecialList = 1;
                    ShowSpecialList(false);
                    var establishmentList = await EstablishmentApi.GetSpecialList(_client,
                        businessActivityId: Settings.BusinessActivityFilter,
                        search: searchTextSpecialList.Text,
                        cityId: Settings.CityFilter,
                        userLat: Settings.UserLatitude,
                        userLong: Settings.UserLongitude);
                    LoadSpecialList(establishmentList);
                }
                else
                {
                    ShowSpecialList(false);
                    Util.NotConnectedMessage(this);
                }
                EstablishmentsListViewSpecialList.EndRefresh();
            }
            catch (Exception ex)
            {
                EstablishmentsListViewSpecialList.EndRefresh();
            }
        }

        private void EstablishmentsListViewSpecialList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            EstablishmentsListViewSpecialList.SelectedItem = null;
        }

        private async void EstablishmentsListViewSpecialList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var eId = e.Item as WSEstablishment;
                var loadingPage = new LoadingPage();
                await Navigation.PushPopupAsync(loadingPage);
                var establishmentAsync = await EstablishmentApi.GetAsync(_client, id: eId.Id);
                var establishment = establishmentAsync.List.FirstOrDefault();
                var establishmentDetailPage = new Pages.TourismDetail(establishment);
                //NavigationPage.SetHasNavigationBar(establishmentDetailPage, false);
                await PopupNavigation.PopAllAsync();
                await this.Navigation.PushAsync(establishmentDetailPage, true);
            }
            catch (Exception ex)
            {
                //resume...
            }
        }

        private async void EstablishmentsListViewSpecialList_OnItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            try
            {
                if (establishmentsLoadingSpecialList.IsRunning)
                    return;
                if (!_paginateSpecialList) return;

                if (e.Item as WSEstablishment != _establishmentsSpecialList[_establishmentsSpecialList.Count - 1] ||
                    _establishmentsSpecialList.Count < Settings.ItensPerPage) return;
                if (CrossConnectivity.Current.IsConnected)
                {
                    var loadingPage = new LoadingPage();
                    await Navigation.PushPopupAsync(loadingPage);
                    _currentPageSpecialList += 1;
                    var establishmentList = await EstablishmentApi.GetSpecialList(_client, page: _currentPageSpecialList,
                        businessActivityId: Settings.BusinessActivityFilter,
                        search: searchTextSpecialList.Text,
                        cityId: Settings.CityFilter,
                        userLat: Settings.UserLatitude,
                        userLong: Settings.UserLongitude);
                    _paginateSpecialList = establishmentList.TotalPages > _currentPageSpecialList;
                    await PopupNavigation.PopAllAsync();
                    LoadPageSpecialList(establishmentList);
                }
                else
                {
                    Util.NotConnectedMessage(this);
                }
            }
            catch (Exception ex)
            {
                //resume...
            }
        }

        private async void TourismTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    if (Settings.CurrentView == (int)ViewPages.Tourism) return;
                    SpecialListTab.IsVisible = true;
                    EstablishmentsTab.IsVisible = false;
                    EstablishmentsHotOfferTab.IsVisible = false;
                    var loadingPage = new LoadingPage();
                    await Navigation.PushPopupAsync(loadingPage);
                    try
                    {
#if DEBUG
                        Settings.UserLatitude = -20.8011502D;
                        Settings.UserLongitude = -49.3971176D;
#else
                        var locator = CrossGeolocator.Current;
                        if (locator.IsGeolocationAvailable)
                        {
                            locator.DesiredAccuracy = 50;
                            var position = await locator.GetPositionAsync(10000);
                            if (!position.Latitude.Equals(0D) || !position.Longitude.Equals(0D))
                            {
                                Settings.UserLatitude = position.Latitude;
                                Settings.UserLongitude = position.Longitude;
                            }
                            else
                            {
                                await DisplayAlert("Atenção", "Ative sua localização para pesquisar lugares próximos!", "Ok");
                            }
                        }
                        else
                        {
                            await DisplayAlert("Atenção", "Ative sua localização para pesquisar lugares próximos!", "Ok");
                        }
#endif
                    }
                    catch (Exception ex)
                    {
                        await DisplayAlert("Atenção", "Ative sua localização para pesquisar lugares próximos!", "Ok");
                        Settings.UserLatitude = 0D;
                        Settings.UserLongitude = 0D;
                    }

                    await PopupNavigation.PopAllAsync();

                    _currentPageSpecialList = 1;

                    if ((PositionListImage.Source as FileImageSource)?.File != Settings.PositionListInactiveImage)
                        PositionListImage.Source = Settings.PositionListInactiveImage;
                    if ((EstablishmentListImage.Source as FileImageSource)?.File !=
                        Settings.EstablishmentListInactiveImage)
                        EstablishmentListImage.Source = Settings.EstablishmentListInactiveImage;
                    if ((FavoritesImage.Source as FileImageSource)?.File != Settings.FavoritesListInactiveImage)
                        FavoritesImage.Source = Settings.FavoritesListInactiveImage;
                    if ((OffersImage.Source as FileImageSource)?.File != Settings.OffersListInactiveImage)
                        OffersImage.Source = Settings.OffersListInactiveImage;
                    if ((HotOffersImage.Source as FileImageSource)?.File != Settings.HotOffersListInactiveImage)
                        HotOffersImage.Source = Settings.HotOffersListInactiveImage;
                    if ((TourismImage.Source as FileImageSource)?.File != Settings.TourismListActiveImage)
                        TourismImage.Source = Settings.TourismListActiveImage;
                    await TourismImage.ScaleTo(0.5, 100, Easing.BounceOut);
                    await TourismImage.ScaleTo(1, 100, Easing.BounceOut);
                    Settings.CurrentView = (int)ViewPages.Tourism;
                    ShowSpecialList(false);
                    var establishmentList = await EstablishmentApi.GetSpecialList(_client, page: _currentPageSpecialList,
                        businessActivityId: Settings.BusinessActivityFilter, search: searchTextSpecialList.Text,
                        cityId: Settings.CityFilter,
                        userLat: Settings.UserLatitude,
                        userLong: Settings.UserLongitude);
                    LoadSpecialList(establishmentList);
                }
                else
                {
                    Util.NotConnectedMessage(this);
                }
            }
            catch (Exception ex)
            {
                //resume...
            }
        }

        private async void SearchText_OnCompleted(object sender, EventArgs e)
        {
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    _currentPage = 1;
                    var establishmentList = await EstablishmentApi.GetAsyncNative(_client, search: searchText.Text,
                        businessActivityId: Settings.BusinessActivityFilter,
                        isFavorite: (Settings.CurrentView == (int)ViewPages.Favorites),
                        isOffer: (Settings.CurrentView == (int)ViewPages.Offers),
                        cityId: Settings.CityFilter,
                        userLat: Settings.UserLatitude,
                        userLong: Settings.UserLongitude);
                    LoadList(establishmentList);
                }
                else
                {
                    Util.NotConnectedMessage(this);
                }
            }
            catch (Exception ex)
            {
                //resume...
            }
        }

        #region Menu Methods

        private async void Indicate_OnTapped(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                await Navigation.PushAsync(new IndicatePage());
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }

        private async void GenerateQr_OnTapped(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var loadingPage = new LoadingPage();
                await Navigation.PushPopupAsync(loadingPage);
                var qrCode = await TransactionApi.GetAuthorizationQr(_client);
                if (qrCode != null)
                {
                    await PopupNavigation.PopAllAsync();
                    await Navigation.PushModalAsync(new QRCodePage(qrCode));
                }
                else
                {
                    await PopupNavigation.PopAllAsync();
                }
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }

        private void ForgotPassword_OnTapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri(Settings.UrlForgotPassword));
        }

        public async Task<bool> Share(bool establishment = false)
        {
            var loadingPage = new LoadingPage();
            await Navigation.PushPopupAsync(loadingPage);
            await CrossShare.Current.ShareLink("Esta é a sua indicação para ter Nível, o aplicativo que dá dinheiro. Com ele você tem retorno em dinheiro das compras que você faz nos estabelecimentos credenciados. Não é ótimo você receber de volta uma parte do que você gasta comprando ao longo do mês? Baixe agora, é só clicar aqui: " +
                                               (establishment ? Settings.IndicateEstablishmentLink : Settings.IndicateCustomerLink),
                "NivelCard - Cashback a toda compra",
                "NivelCard - Cashback a toda compra");
            await PopupNavigation.PopAllAsync();
            return true;
        }

        private async void Reward_OnTapped(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                await this.Navigation.PushAsync(new RewardPage());
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }

        private async void Receive_OnTapped(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                await this.Navigation.PushAsync(new RewardPage(true));
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }

        private async void Panel_OnTapped(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                await this.Navigation.PushAsync(new PanelPage());
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }

        private void HowTo_OnTapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.nivel.com.br/"));
        }

        private void Executive_OnTapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.nivel.com.br/site/executivo-nivel/"));
        }

        private async void Transfer_OnTapped(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                await this.Navigation.PushAsync(new TransferPage());
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }

        private async void UsernameTgr_Tapped(object sender, EventArgs e)
        {
            await DisplayAlert("Mensagem", "Função não habilitada", "Ok");
        }

        private async void Logout_OnTapped(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var loadingPage = new LoadingPage();
                await Navigation.PushPopupAsync(loadingPage);
                Settings.VerifiedUser = false;
                Settings.UserIsExecutiveCommissioner = false;
                var customer = await CustomerApi.Logout(new HttpClient(), Settings.UserDocument, Settings.DeviceToken);
                if (customer != null)
                {
                    if (customer.Error.Any())
                    {
                        var errorText = Util.GetWsErrorDescription(customer.Error.First());
                        await DisplayAlert("Atenção", errorText, "Fechar");
                        await PopupNavigation.PopAllAsync();
                    }
                    else
                    {
                        Settings.Logout();
                        Application.Current.MainPage = new NavigationPage(new LoginPage());
                        await PopupNavigation.PopAllAsync();
                        await Navigation.PopToRootAsync(true);
                    }
                }
                else
                {
                    await DisplayAlert("Erro", "Erro ao desconectar", "Ok");
                    await PopupNavigation.PopAllAsync();
                }
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }

        private async void Username_OnTapped(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                await this.Navigation.PushAsync(new ProfilePage());
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }

        //protected override bool OnBackButtonPressed()
        //{
        //    if (navigationDrawer.IsOpen)
        //    {
        //        navigationDrawer.ToggleDrawer();
        //        return true;
        //    }
        //    else
        //    {
        //        return base.OnBackButtonPressed();
        //    }
        //}

        #endregion Menu Methods
    }

    public class CustomCell : ViewCell
    {
        private StackLayout masterStackLayout = null;
        private StackLayout childStackLayout = null;
        private StackLayout secondChildStackLayout = null;
        private StackLayout thirdChildStackLayout = null;
        private Image cachedImage = null;
        private CircleImage circleImage = null;
        private Label displayNameLabel = null;
        private Label discountLabel = null;
        private Label businessActivityLabel = null;
        private Image imgMarker = null;
        private Label distanceToUserLabel = null;

        public CustomCell()
        {
            masterStackLayout = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Padding = new Thickness(5)
            };
            circleImage = new CircleImage
            {
                WidthRequest = 50,
                HeightRequest = 50,
                Aspect = Aspect.AspectFit,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                BackgroundColor = Color.Transparent,
                BorderColor = (Color)Application.Current.Resources["ImageBorderColor"],
                BorderThickness = 1
            };
            childStackLayout = new StackLayout
            {
                HorizontalOptions = LayoutOptions.StartAndExpand,
                Margin = new Thickness(5, 0, 0, 0)
            };
            displayNameLabel = new Label
            {
                TextColor = (Color)Application.Current.Resources["SecondaryColor"],
                VerticalOptions = LayoutOptions.Start,
                FontSize = 15,
                FontAttributes = FontAttributes.Bold
            };
            childStackLayout.Children.Add(displayNameLabel);
            secondChildStackLayout = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Center
            };
            discountLabel = new Label
            {
                TextColor = (Color)Application.Current.Resources["PrimaryColor"],
                BackgroundColor = (Color)Application.Current.Resources["FeaturedBackgroundColor"],
                FontSize = 14,
            };
            businessActivityLabel = new Label
            {
                TextColor = (Color)Application.Current.Resources["PrimaryColor"],
                BackgroundColor = (Color)Application.Current.Resources["PlaceholderColor"],
                FontSize = 14,
            };
            secondChildStackLayout.Children.Add(discountLabel);
            secondChildStackLayout.Children.Add(businessActivityLabel);
            childStackLayout.Children.Add(secondChildStackLayout);
            thirdChildStackLayout = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.End
            };
            imgMarker = new Image
            {
                Source = "marker.png",
                HeightRequest = 12,
                BackgroundColor = Color.Transparent
            };
            distanceToUserLabel = new Label
            {
                FontSize = 14,
                TextColor = (Color)Application.Current.Resources["TertiaryColor"],
            };
            thirdChildStackLayout.Children.Add(imgMarker);
            thirdChildStackLayout.Children.Add(distanceToUserLabel);
            childStackLayout.Children.Add(thirdChildStackLayout);
            cachedImage = new Image
            {
                BackgroundColor = Color.Transparent,
                HeightRequest = 30
            };
            masterStackLayout.Children.Add(circleImage);
            masterStackLayout.Children.Add(childStackLayout);
            masterStackLayout.Children.Add(cachedImage);
            View = masterStackLayout;
        }

        protected override void OnBindingContextChanged()
        {
            cachedImage.Source = null;
            circleImage.Source = null;
            displayNameLabel.Text = "";
            discountLabel.Text = "";
            businessActivityLabel.Text = "";
            distanceToUserLabel.Text = null;
            imgMarker.IsVisible = false;
            var item = BindingContext as WSEstablishment;
            cachedImage.GestureRecognizers.Clear();
            if (item == null)
                return;
            if (item.IsHotDiscount && Settings.CurrentView == (int)ViewPages.HotOffer)
            {
                masterStackLayout.Children.Clear();
                masterStackLayout.BackgroundColor = (Color)Application.Current.Resources["DefaultBackgroundColor"];
                var backgroundImage = new Image
                {
                    Source = item.BackgroundImage,
                    Aspect = Aspect.AspectFill,
                    Opacity = 0.75
                };
                var labelDiscount = new Label
                {
                    TextColor = (Color)Application.Current.Resources["PrimaryColor"],
                    BackgroundColor = (Color)Application.Current.Resources["FeaturedBackgroundColor"],
                    FontSize = 16,
                    Text = item.Discount.Replace("Ganhe ", "").Replace(" até ", ""),
                    FontAttributes = FontAttributes.Bold,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Center
                };

                var labelEstablishment = new Label
                {
                    TextColor = (Color)Application.Current.Resources["PrimaryColor"],
                    FontSize = 15,
                    Text = item.DisplayName,
                    FontAttributes = FontAttributes.Bold,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Start
                };

                var labelFinish = new Label
                {
                    TextColor = (Color)Application.Current.Resources["PrimaryColor"],
                    FontSize = 15,
                    FontAttributes = FontAttributes.Bold,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Start
                };

                var labelExpired = new Label
                {
                    IsVisible = false,
                    TextColor = (Color)Application.Current.Resources["FeaturedTimeColor"],
                    FontSize = 15,
                    FontAttributes = FontAttributes.Bold,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Start,
                    Text = "A OFERTA TERMINOU"
                };

                var labelTime = new Label
                {
                    TextColor = (Color)Application.Current.Resources["FeaturedTimeColor"],
                    FontSize = 15,
                    FontAttributes = FontAttributes.Bold,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Start
                };

                if (item.TimeLeft != 0)
                {
                    labelFinish.Text = "Termina em ";
                    var timeSpan = TimeSpan.FromTicks(item.TimeLeft);
                    ICountDownTimer timer = new CountDownTimer(timeSpan);
                    timer.IntervalPassed += (sender, args) =>
                    {
                        labelTime.Text = string.Format("{0}:{1:mm}:{1:ss}", (int)timer.CurrentTime.TotalHours, timer.CurrentTime);
                    };
                    timer.ReachedZero += (sender, args) =>
                    {
                        labelFinish.IsVisible = false;
                        labelTime.IsVisible = false;
                        labelExpired.IsVisible = true;
                    };
                    timer.Start();
                }

                var layout = new AbsoluteLayout
                {
                    HeightRequest = 150,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    Children =
                    {
                        {backgroundImage, new Rectangle(0.5, 0.5, 1, 1), AbsoluteLayoutFlags.All},
                        {labelDiscount, new Rectangle(0.05, 0.85, 40, 35), AbsoluteLayoutFlags.PositionProportional},
                        {
                            labelEstablishment, new Rectangle(60, 0.75, 0.8, 20),
                            AbsoluteLayoutFlags.YProportional | AbsoluteLayoutFlags.WidthProportional
                        },
                        {labelFinish, new Rectangle(60, 0.87, 80, 20), AbsoluteLayoutFlags.YProportional},
                        {labelExpired, new Rectangle(60, 0.87, 160, 20), AbsoluteLayoutFlags.YProportional},
                        {labelTime, new Rectangle(147, 0.87, 80, 20), AbsoluteLayoutFlags.YProportional}
                    }
                };
                masterStackLayout.Margin = new Thickness(0, 0, 0, 1);
                masterStackLayout.Padding = new Thickness(0);
                masterStackLayout.Children.Add(layout);
            }
            else
            {
                distanceToUserLabel.Text = Util.DistanceToUser(item.DistanceToUser);
                imgMarker.IsVisible = !(item.DistanceToUser <= 0 || item.DistanceToUser > 5000);
                circleImage.Source = item.Picture;
                cachedImage.Source = item.Favorite ? "favorite.png" : "_favorite.png";
                displayNameLabel.Text = item.DisplayName;
                discountLabel.Text = item.Discount;
                businessActivityLabel.Text = item.BusinessActivity;

                var tapGestureRecognizer = new TapGestureRecognizer();
                tapGestureRecognizer.Tapped += async (s, e) =>
                {
                    try
                    {
                        if (item == null) return;
                        if (item.Favorite)
                        {
                            item.Favorite = false;
                            cachedImage.Source = "_favorite.png";
                        }
                        else
                        {
                            item.Favorite = true;
                            cachedImage.Source = "favorite.png";
                        }
                        await CustomerApi.Favorite(new HttpClient(), item.Id, item.Favorite);
                    }
                    catch (Exception ex)
                    {
                        //resume...
                    }
                };
                cachedImage.GestureRecognizers.Add(tapGestureRecognizer);
            }
            base.OnBindingContextChanged();
        }
    }
}