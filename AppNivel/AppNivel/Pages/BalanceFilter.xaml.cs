﻿using AppNivel.Helpers;
using AppNivel.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class BalanceFilter : ContentPage
    {
        private ObservableCollection<BalanceFilterObj> _balanceFilter;

        public BalanceFilter()
        {
            Settings.CurrentPage = "BalanceFilter";
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
        }

        private void ShowList(bool show = true)
        {
            BalanceFilterView.IsVisible = show;
        }

        protected override void OnAppearing()
        {
            var balanceFilter = new List<BalanceFilterObj>()
            {
                new BalanceFilterObj() { Text = "Últimos 12 meses", Period = "year", IsSelected = (Settings.BalancePeriodSelected == "year")},
                new BalanceFilterObj() { Text = "Últimos 3 meses", Period = "3months", IsSelected = (Settings.BalancePeriodSelected == "3months")},
                new BalanceFilterObj() { Text = "Último mês", Period = "month", IsSelected = (Settings.BalancePeriodSelected == "month")},
                new BalanceFilterObj() { Text = "Mês Corrente", Period = "current", IsSelected = (Settings.BalancePeriodSelected == "current")},
                new BalanceFilterObj() { Text = "Janeiro", Period = "january", IsSelected = (Settings.BalancePeriodSelected == "january")},
                new BalanceFilterObj() { Text = "Fevereiro", Period = "february", IsSelected = (Settings.BalancePeriodSelected == "february")},
                new BalanceFilterObj() { Text = "Março", Period = "march", IsSelected = (Settings.BalancePeriodSelected == "march")},
                new BalanceFilterObj() { Text = "Abril", Period = "april", IsSelected = (Settings.BalancePeriodSelected == "april")},
                new BalanceFilterObj() { Text = "Maio", Period = "may", IsSelected = (Settings.BalancePeriodSelected == "may")},
                new BalanceFilterObj() { Text = "Junho", Period = "june", IsSelected = (Settings.BalancePeriodSelected == "june")},
                new BalanceFilterObj() { Text = "Julho", Period = "july", IsSelected = (Settings.BalancePeriodSelected == "july")},
                new BalanceFilterObj() { Text = "Agosto", Period = "august", IsSelected = (Settings.BalancePeriodSelected == "august")},
                new BalanceFilterObj() { Text = "Setembro", Period = "september", IsSelected = (Settings.BalancePeriodSelected == "september")},
                new BalanceFilterObj() { Text = "Outubro", Period = "october", IsSelected = (Settings.BalancePeriodSelected == "october")},
                new BalanceFilterObj() { Text = "Novembro", Period = "november", IsSelected = (Settings.BalancePeriodSelected == "november")},
                new BalanceFilterObj() { Text = "Dezembro", Period = "december", IsSelected = (Settings.BalancePeriodSelected == "december")},
            };

            _balanceFilter = new ObservableCollection<BalanceFilterObj>(balanceFilter);
            BalanceFilterView.ItemsSource = _balanceFilter;
            ShowList();

            base.OnAppearing();
        }

        private void TapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        private void BalanceFilter_OnTapped(object sender, ItemTappedEventArgs e)
        {
            var balanceFilter = e.Item as BalanceFilterObj;
            Settings.BalancePeriodSelected = balanceFilter.Period;
            Navigation.PopAsync();
        }

        private void BalanceFilter_OnSelected(object sender, SelectedItemChangedEventArgs e)
        {
            BalanceFilterView.SelectedItem = null;
        }
    }
}