﻿using AppNivel.Helpers;
using AppNivel.Models;
using AppNivel.Resources;
using Plugin.Connectivity;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class TransferConfirmationPage : ContentPage
    {
        private WSPerson _beneficiary;
        private WSPerson _sender;
        private string _transactionValue;
        private WSTransactionType _transactionType;
        private bool _isChildRewarding;
        private string _transactionDoc;

        public TransferConfirmationPage(WSPerson sender, WSPerson beneficiary, string transactionValue, WSTransactionType transactionType, string transactionDoc, bool isChildRewarding)
        {
            Settings.CurrentPage = "RewardConfirmation";
            InitializeComponent();
            _beneficiary = beneficiary;
            _sender = sender;
            _transactionValue = transactionValue;
            _transactionDoc = transactionDoc;
            _isChildRewarding = isChildRewarding;
            _transactionType = transactionType;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (_transactionType == WSTransactionType.Debit)
                TransactionValue.TextColor = Color.FromHex("#E66363");
            TransactionDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            TransactionValue.Text = _transactionValue;
            TransactionDoc.Text = _transactionDoc;
            Sender.Text = Util.CpfCnpjMask(_sender.CpfCnpj);
            Beneficiary.Text = Util.CpfCnpjMask(_beneficiary.CpfCnpj);
            TransactionType.Text = Util.GetTransactionType(_transactionType);
        }

        private async void Close()
        {
            await Navigation.PopAsync();
        }

        private void Cancel_OnClicked(object sender, EventArgs e)
        {
            Close();
        }

        private async void Confirm_OnClicked(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                if (UserPassword.Text == Settings.UserPassword)
                {
                    await Util.Reward(this, _sender, _beneficiary, Convert.ToDecimal(_transactionValue, CultureInfo.CurrentCulture), _transactionType, _transactionDoc, _isChildRewarding);
                }
                else
                {
                    await DisplayAlert("Atenção", "Senha incorreta", "Ok");
                }
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }
    }
}