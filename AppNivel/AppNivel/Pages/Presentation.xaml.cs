﻿using AppNivel.GrialTheme.Views.Logins;
using System;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class Presentation : CarouselPage
    {
        public Presentation()
        {
            InitializeComponent();
        }

        private void Button_OnClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new LoginPage());
        }

        private void Cadastrar_OnClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new RegistrationPage());
        }
    }
}