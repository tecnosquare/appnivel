﻿using AppNivel.GrialTheme.Views.Logins;
using AppNivel.Helpers;
using AppNivel.Resources;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class CountrySelectionPage : ContentPage
    {
        public CountrySelectionPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            ChooseCountry.Text = Language.ChooseCountry;
            if (Settings.IsLogged)
            {
                var loadingPage = new LoadingPage();
                await Navigation.PushPopupAsync(loadingPage);
                Settings.VerifiedUser = false;
                Settings.UserIsExecutiveCommissioner = false;
                var customer = await CustomerApi.Logout(new HttpClient(), Settings.UserDocument, Settings.DeviceToken);
                if (customer != null)
                {
                    if (customer.Error.Any())
                    {
                        var errorText = Util.GetWsErrorDescription(customer.Error.First());
                        await DisplayAlert("Atenção", errorText, "Fechar");
                    }
                    else
                    {
                        Settings.Logout();
                    }
                }
                else
                {
                    await DisplayAlert("Erro", "Erro ao carregar página", "Ok");
                }
                await PopupNavigation.PopAllAsync();
            }
            base.OnAppearing();
        }

        private void Portugal_OnTapped(object sender, EventArgs e)
        {
            Settings.SelectedCulture = "pt-PT";
            LoadLoginPage();
        }

        private void Brasil_OnTapped(object sender, EventArgs e)
        {
            Settings.SelectedCulture = "pt-BR";
            LoadLoginPage();
        }

        private void LoadLoginPage()
        {
            var ci = new CultureInfo(Settings.SelectedCulture);
            Language.Culture = ci;
            DependencyService.Get<ILocalize>().SetLocale(ci);
            Settings.AppWsUrl = Language.WSUrl;

            switch (ci.Name)
            {
                case "pt-PT":
                    Settings.CityFilter = 5830;
                    Settings.CityFilterName = "Lisboa - Lisboa";
                    break;

                default:
                    Settings.CityFilter = 5264;
                    Settings.CityFilterName = "São José do Rio Preto - SP";
                    break;
            }
            Application.Current.MainPage = new NavigationPage(new LoginPage());
        }
    }
}