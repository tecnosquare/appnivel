﻿using AppNivel.Helpers;
using AppNivel.Models;
using AppNivel.Resources;
using Plugin.Connectivity;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class EstablishmentFilter : ContentPage
    {
        private HttpClient _client = new HttpClient();
        private ObservableCollection<WSBusinessActivity> _categories;

        public EstablishmentFilter()
        {
            Settings.CurrentPage = "EstablishmentFilter";
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
        }

        private void ShowList(bool show = true)
        {
            BusinessActivityView.IsVisible = show;
        }

        protected override async void OnAppearing()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var categories = await BusinessActivityApi.GetAsync(_client);

                if (categories != null)
                {
                    if (!categories.Error.Any())
                    {
                        _categories = new ObservableCollection<WSBusinessActivity>(categories.List);
                        BusinessActivityView.ItemsSource = _categories;
                        ShowList();
                    }
                    else
                    {
                        ShowList(false);
                        await DisplayAlert("Atenção", "Sem categorias cadastrados", "Ok");
                    }
                }
                else
                {
                    ShowList(false);
                    await DisplayAlert("Atenção", "Sem categorias cadastrados", "Ok");
                }
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
            base.OnAppearing();
        }

        private void TapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        private void Categoria_OnTapped(object sender, ItemTappedEventArgs e)
        {
            var businessActivity = e.Item as WSBusinessActivity;
            Settings.BusinessActivityFilter = businessActivity.Id;
            Settings.BusinessActivityFilterName = businessActivity.Name;
            this.Navigation.PopToRootAsync(true);
        }

        private void Categoria_OnSelected(object sender, SelectedItemChangedEventArgs e)
        {
            BusinessActivityView.SelectedItem = null;
        }
    }
}