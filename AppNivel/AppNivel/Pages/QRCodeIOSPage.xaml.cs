﻿using AppNivel.Models;
using AppNivel.Resources;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class QRCodeIOSPage : PopupPage
    {
        private HttpClient _client = new HttpClient();

        public QRCodeIOSPage(WSQrCode qrCode)
        {
            InitializeComponent();
            BindingContext = qrCode;
            RunTask(qrCode);
        }

        private void RunTask(WSQrCode qrCode)
        {
            Device.StartTimer(TimeSpan.FromTicks(qrCode.Remaining), () =>
            {
                Task.Factory.StartNew(async () =>
                {
                    var code = await UpdateQrCode();
                    // Switch back to the UI thread to update the UI
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        BindingContext = code;
                        RunTask(code);
                    });
                });
                return false;
            });
        }

        private async Task<WSQrCode> UpdateQrCode()
        {
            return await TransactionApi.GetAuthorizationQr(_client);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            CloseImage.Rotation = 30;
            CloseImage.Scale = 0.3;
            CloseImage.Opacity = 0;
        }

        protected override async Task OnAppearingAnimationEnd()
        {
            await Task.WhenAll(
                CloseImage.FadeTo(1),
                CloseImage.ScaleTo(1, easing: Easing.SpringOut),
                CloseImage.RotateTo(0));
        }

        private void OnCloseButtonTapped(object sender, EventArgs e)
        {
            CloseAllPopup();
        }

        protected override bool OnBackgroundClicked()
        {
            CloseAllPopup();

            return false;
        }

        private async void CloseAllPopup()
        {
            await Navigation.PopAllPopupAsync();
        }
    }
}