﻿using AppNivel.Helpers;
using AppNivel.Resources;
using Plugin.Share;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class IndicatePage : ContentPage
    {
        private HttpClient _client = new HttpClient();

        public IndicatePage()
        {
            Settings.CurrentPage = "Indicate";
            Settings.CurrentPage = "Establishment";
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            var loadingPage = new LoadingPage();
            await Navigation.PushPopupAsync(loadingPage);
            var person = await CustomerApi.GetPerson(_client, Settings.UserId);
            //Settings.UserRemainingCustomerIndications = person.RemainingIndicationCustomer;
            //Settings.UserRemainingEstablishmentIndications = person.RemainingIndicationEstablishment;
            Settings.IndicateEstablishmentLink = person.EstablishmentIndicationLink;
            Settings.IndicateCustomerLink = person.CustomerIndicationLink;
            //lblFriends.Text = person.RemainingIndicationCustomer.ToString();
            ////lblEstablishments.Text = person.RemainingIndicationEstablishment.ToString();
            //lblFriends.IsVisible = (person.RemainingIndicationCustomer != 0);
            if (person.IsCommissioned && person.PersonAccepted)
            {
                lblFriends.IsVisible = false;
                btnFriendsNotAccepted.IsVisible = false;
                lblFriendsNotAccepted.IsVisible = false;
                btnIndicateFriend.IsEnabled = true;
                btnIndicateEstablishment.IsEnabled = true;
                if (Settings.UserIsExecutiveCommissioner)
                {
                    lblWannaBeExecutive.IsVisible = false;
                }
                lblAcceptTerms.IsVisible = false;
                AcceptTerms.IsVisible = false;
                lblAcceptTerm.IsVisible = false;
            }
            await PopupNavigation.PopAllAsync();
            base.OnAppearing();
        }

        public async Task<bool> Share(bool establishment = false)
        {
            var loadingPage = new LoadingPage();
            await Navigation.PushPopupAsync(loadingPage);

            var txtIndication = (establishment ? Settings.IndicateEstablishmentLink : Settings.IndicateCustomerLink);

            //await CrossShare.Current.ShareLink("Olá! Eu baixei um aplicativo de cashback para a nossa cidade, e estou amando. Recebo dinheiro de volta toda vez que compro, " +
            //                                   "um percentual, claro, mas é fantástico! É gratuito, sabia? baixa com este link que te mandei. Se chama Nível. Faça como estou fazendo, " +
            //                                   "ganhe comprando nos estabelecimentos cadastrados no Nível e ganhe também quando indicar. Por isso quis te passar este boa notícia. ;) " + txtIndication
            //                                   , "NivelCard - Sua Indicação"
            //                                   , "NivelCard - Cashback a toda compra");

            await CrossShare.Current.Share("Olá! Eu baixei um aplicativo de cashback para a nossa cidade, e estou amando. Recebo dinheiro de volta toda vez que compro, " +
                                               "um percentual, claro, mas é fantástico! É gratuito, sabia? baixa com este link que te mandei. Se chama Nível. Faça como estou fazendo, " +
                                               "ganhe comprando nos estabelecimentos cadastrados no Nível e ganhe também quando indicar. Por isso quis te passar este boa notícia. ;)\n\n " + txtIndication);

            await PopupNavigation.PopAllAsync();
            return true;
        }

        private async void IndicateShare(object sender, EventArgs e)
        {
            await Share();
        }

        private async void Indicate_OnClicked(object sender, EventArgs e)
        {
            //if (CrossConnectivity.Current.IsConnected)
            //{
            //    var loadingPage = new LoadingPage();
            //    await PopupNavigation.PushAsync(loadingPage);
            //    var isValid = true;

            //    if (!EmailValidator.IsValid)
            //    {
            //        await DisplayAlert("Atenção", "O e-mail digitado não é válido.", "Fechar");
            //        isValid = false;
            //    }

            //    if ((string.IsNullOrWhiteSpace(IndicateName.Text) || IndicateName.Text.Length < 3) && isValid)
            //    {
            //        await DisplayAlert("Atenção", "Nome não é válido.", "Fechar");
            //        isValid = false;
            //    }

            //    if (isValid)
            //    {
            //        var ret =
            //            await
            //                GeneralApi.Indicate(_client, IndicateEmail.Text, IndicateName.Text,
            //                    IndicateLastname.Text, _indicationType);
            //        if (ret != null)
            //        {
            //            if (ret.Error.Any())
            //            {
            //                var errorText = Util.GetWsErrorDescription(ret.Error.First());
            //                await DisplayAlert("Atenção", errorText, "Fechar");
            //            }
            //            else
            //            {
            //                Settings.UserRemainingEstablishmentIndications = ret.RemainingIndicationEstablishment;
            //                Settings.UserRemainingCustomerIndications = ret.RemainingIndicationCustomer;
            //                await DisplayAlert("Sucesso", "Indicação realizada com sucesso!", "Ok");
            //                await PopupNavigation.PopAllAsync();
            //                await Navigation.PopAsync(true);
            //            }
            //        }
            //        else
            //        {
            //            await DisplayAlert("Erro", "Erro ao realizar indicação. Tente novamente mais tarde.", "Ok");
            //        }
            //    }

            //    await PopupNavigation.PopAllAsync();
            //}
            //else
            //{
            //    Util.NotConnectedMessage(this);
            //}
        }

        private void TapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Navigation.PopAsync(true);
        }

        private async void IndicateEstablishment(object sender, EventArgs e)
        {
            await Share(true);
        }

        private void PanelGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("http://www.nivel.com.br/Post/Ver/29842"));
        }

        private void TermsTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            PopupNavigation.PushAsync(new CommissionerTerms());
        }

        private async void AcceptTermsTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            var loadingPage = new LoadingPage();
            await Navigation.PushPopupAsync(loadingPage);

            var customer =
                       await
                           CustomerApi.AcceptTerms(_client);

            if (customer != null)
            {
                if (customer.Error.Any())
                {
                    var errorText = Util.GetWsErrorDescription(customer.Error.First());
                    await DisplayAlert("Atenção", errorText, "Fechar");
                }
                else
                {
                    lblFriends.IsVisible = false;
                    btnFriendsNotAccepted.IsVisible = false;
                    lblFriendsNotAccepted.IsVisible = false;
                    btnIndicateFriend.IsEnabled = true;
                    if (Settings.UserIsExecutiveCommissioner)
                    {
                        btnIndicateEstablishment.IsEnabled = true;
                        lblWannaBeExecutive.IsVisible = false;
                    }
                    lblAcceptTerms.IsVisible = false;
                    lblAcceptTerm.IsVisible = false;
                    AcceptTerms.IsVisible = false;
                }
            }
            else
            {
                await
                        DisplayAlert("Atenção",
                            "Ocorreu um erro ao aceitar os termos. Tente novamente mais tarde.", "Ok");
            }

            await PopupNavigation.PopAllAsync();
        }

        private void WannaBeExecutive(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.nivel.com.br/Painel/QueroSerComissarioExecutivo"));
        }
    }
}