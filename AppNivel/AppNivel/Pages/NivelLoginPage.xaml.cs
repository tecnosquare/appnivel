﻿using AppNivel.Helpers;
using AppNivel.Models;
using AppNivel.Resources;
using Plugin.Connectivity;
using Rg.Plugins.Popup.Services;
using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class NivelLoginPage : ContentPage
    {
        public NivelLoginPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            ChangeCountry.Text = Language.ChangeCountry;
            UserDocument.Placeholder = Language.PlaceholderDocument;
            UserPassword.Placeholder = Language.PlaceholderPassword;
            InfoLogin.Text = Language.InfoLogin;
            base.OnAppearing();
        }

        private async void SignIn_OnClicked(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var loadingPage = new LoadingPage();
                await PopupNavigation.PushAsync(loadingPage);
                var isValid = true;

                if (!ExtendedDocumentValidator.IsValid)
                {
                    await DisplayAlert("Atenção", "O documento digitado não é válido.", "Fechar");
                    isValid = false;
                }

                if ((string.IsNullOrWhiteSpace(UserPassword.Text) || UserPassword.Text.Length < 6) && isValid)
                {
                    await DisplayAlert("Atenção", "Sua senha deve conter, no mínimo, 6 caracteres.", "Fechar");
                    isValid = false;
                }

                if (isValid)
                {
                    var customer = await CustomerApi.Login(new HttpClient(), UserDocument.Text, UserPassword.Text,
                            Settings.DeviceToken, Regex.Matches(UserDocument.Text, @"[a-zA-Z]").Count > 0,
                            Regex.Matches(UserDocument.Text, @"[a-zA-Z]").Count > 0 ? UserDocument.Text : "");
                    if (customer != null)
                    {
                        if (customer.Error.Any())
                        {
                            var errorText = Util.GetWsErrorDescription(customer.Error.First());
                            await DisplayAlert("Atenção", errorText, "Fechar");
                            Settings.IsLogged = false;
                        }
                        else
                        {
                            Settings.UserId = customer.Id;
                            Settings.Username = customer.Firstname + " " + customer.Lastname;
                            Settings.UserFirstname = customer.Firstname;
                            Settings.UserLastname = customer.Lastname;
                            Settings.UserDocument = customer.CpfCnpj;
                            Settings.UserPicture = customer.Picture;
                            Settings.UserRemainingCustomerIndications = customer.RemainingIndicationCustomer;
                            Settings.UserRemainingEstablishmentIndications = customer.RemainingIndicationEstablishment;
                            Settings.IndicateEstablishmentLink = customer.EstablishmentIndicationLink;
                            Settings.IndicateCustomerLink = customer.CustomerIndicationLink;
                            Settings.UserType = customer.PersonTypeId;
                            Settings.UserPassword = UserPassword.Text;
                            Settings.BusinessActivityFilter = 0;
                            Settings.UserHasAddress = customer.Address.Count > 0;

                            if (customer.LoggedAsUser)
                            {
                                Settings.IsChildUser = customer.LoggedAsUser;
                                Settings.Username = customer.Username;
                                Settings.UserChildId = customer.UserId;
                                Settings.TotalAccess = customer.TotalAccess;
                                Settings.TransactionValueLimit = customer.TransactionValueLimit;
                                Settings.TransactionLimit = customer.TransactionLimit;
                            }
                            else
                            {
                                Settings.IsChildUser = false;
                                Settings.UserChildId = 0;
                                Settings.TotalAccess = true;
                                Settings.TransactionValueLimit = 0;
                                Settings.TransactionLimit = 0;
                            }

                            var contactTypeCel = (int)ContactType.Celular;
                            var contactTypeEmail = (int)ContactType.Email;

                            if (customer.Contact.Any(x => x.ContactTypeId == contactTypeEmail))
                            {
                                var email = customer.Contact.First(x => x.ContactTypeId == contactTypeEmail && x.Main);
                                Settings.UserEmail = email.ContactValue;
                            }

                            if (customer.Contact.Any(x => x.ContactTypeId == contactTypeCel))
                            {
                                var phone = customer.Contact.First(x => x.ContactTypeId == contactTypeCel && x.Main);
                                Settings.UserPhone = phone.ContactValue;
                            }

                            Settings.IsLogged = true;

                            Application.Current.MainPage = new NavigationPage(new MainPage());
                            await Navigation.PopToRootAsync(true);
                        }
                    }
                    else
                    {
                        await DisplayAlert("Erro", "Erro ao inciar o aplicativo - Realize o login novamente", "Ok");
                    }
                }

                await PopupNavigation.PopAllAsync();
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }

        private void UserDocument_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (Regex.Matches(e.NewTextValue, @"[a-zA-Z]").Count > 0 || string.IsNullOrWhiteSpace(e.NewTextValue))
            {
                //UserDocument.Keyboard = Keyboard.Url;
                return;
            }
            else
            {
                //UserDocument.Keyboard = Keyboard.Numeric;
            }

            switch (CultureInfo.CurrentCulture.Name)
            {
                case "pt-PT":
                    Util.NifMask(sender, e);
                    break;

                default:
                    if (e.NewTextValue.Length > 14)
                    {
                        Util.CnpjMask(sender, e);
                    }
                    else
                    {
                        Util.CpfMask(sender, e);
                    }
                    break;
            }
        }

        private void RegisterTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Navigation.PushAsync(new RegistrationPage());
        }

        private void PassRecoveryTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri(Settings.UrlForgotPassword));
        }

        private void WorkerTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            UserDocument.Keyboard = Keyboard.Email;
            InfoLogin.IsVisible = true;
        }

        private void InfoLoginTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            UserDocument.Keyboard = Keyboard.Numeric;
            InfoLogin.IsVisible = false;
        }

        private void ChangeCountryTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new CountrySelectionPage());
        }
    }
}