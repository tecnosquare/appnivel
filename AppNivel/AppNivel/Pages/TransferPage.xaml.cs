﻿using AppNivel.Helpers;
using AppNivel.Models;
using AppNivel.Resources;
using Plugin.Connectivity;
using System;
using System.Globalization;
using System.Net.Http;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class TransferPage : ContentPage
    {
        private bool isChildRewarding = Settings.IsChildUser;
        private bool _receive = false;

        public TransferPage(bool receive = false)
        {
            _receive = receive;
            Settings.CurrentPage = "Reward";
            Settings.CurrentPage = "Establishment";
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            LabelUserDocument.Text = Language.FriendDocument;
            if (_receive)
            {
                SaveData.Text = "Receber";
                BonifyLabel.Text = "Receber";
            }
            TransactionDoc.Text = "";
            TransactionValue.Text = "";
            UserDocument.Text = "";

            LabelValueAvaible.Text = "Saldo disponível : " + Settings.UserCashback.ToString("C2", CultureInfo.CurrentCulture);
            base.OnAppearing();
        }

        private async void Reward_OnClicked(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var isValid = true;

                if (!ExtendedDocumentValidator.IsValid)
                {
                    await DisplayAlert("Atenção", "O Cpf/Cnpj digitado não é válido.", "Fechar");
                    isValid = false;
                }

                if (!TransactionValueValidator.IsValid && isValid)
                {
                    await DisplayAlert("Atenção", "O valor da transferência não é válido.", "Fechar");
                    isValid = false;
                }

                if (isValid)
                {
                    var userExists = await EstablishmentApi.PersonExists(new HttpClient(), UserDocument.Text);

                    if (userExists == null)
                    {
                        await DisplayAlert("Atenção", "Amigo não encontrado.", "Fechar");
                    }
                    else
                    {
                        await
                            Navigation.PushAsync(
                                new TransferConfirmationPage(new WSPerson() { CpfCnpj = Settings.UserDocument },
                                    new WSPerson() { CpfCnpj = UserDocument.Text }, TransactionValue.Text,
                                    WSTransactionType.Transfer, TransactionDoc.Text, isChildRewarding));
                    }
                }
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }

        private void UserDocument_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (e.NewTextValue.Length > 14)
            {
                Util.CnpjMask(sender, e);
            }
            else
            {
                Util.CpfMask(sender, e);
            }
        }

        private void TransactionValue_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            Util.DecimalMask(sender, e);
        }

        private void QuestionsTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.nivel.com.br/Conteudo/Ver/15819"));
        }

        private void TapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Navigation.PopAsync(true);
        }
    }
}