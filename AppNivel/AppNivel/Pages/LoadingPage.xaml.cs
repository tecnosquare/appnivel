﻿using Rg.Plugins.Popup.Pages;

namespace AppNivel.Pages
{
    public partial class LoadingPage : PopupPage
    {
        public LoadingPage()
        {
            InitializeComponent();
        }

        protected override bool OnBackButtonPressed()
        {
            return true;
        }
    }
}