﻿using AppNivel.Resources;
using Plugin.Connectivity;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class AddressRegistration : ContentPage
    {
        private HttpClient _client = new HttpClient();
        private bool reloadCities = true;

        public AddressRegistration()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var loadingPage = new LoadingPage();
                await PopupNavigation.PushAsync(loadingPage);

                var states = await GeneralApi.GetStatesAsync(_client);

                if (states != null)
                {
                    if (!states.Error.Any())
                    {
                        foreach (var wsState in states.List)
                        {
                            StatePicker.Items.Add(wsState.Name);
                        }
                        StatePicker.SelectedIndex = 24;

                        var selectedState = StatePicker.Items[StatePicker.SelectedIndex];
                        var cities = await GeneralApi.GetCitiesAsync(_client, selectedState);

                        if (cities != null)
                        {
                            if (!cities.Error.Any())
                            {
                                CityPicker.Items.Clear();
                                foreach (var city in cities.List)
                                {
                                    CityPicker.Items.Add(city.Name);
                                }
                            }
                            else
                            {
                                await DisplayAlert("Atenção", "Sem cidades cadastradas", "Ok");
                            }
                        }
                        else
                        {
                            await DisplayAlert("Atenção", "Sem cidades cadastradas", "Ok");
                        }
                    }
                    else
                    {
                        await DisplayAlert("Atenção", "Sem estados cadastrados", "Ok");
                    }
                }
                else
                {
                    await DisplayAlert("Atenção", "Sem estados cadastrados", "Ok");
                }

                CloseAllPopup();
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
            base.OnAppearing();
        }

        private async void CloseAllPopup()
        {
            await Navigation.PopAllPopupAsync();
        }

        private async void StatePicker_OnUnfocused(object sender, FocusEventArgs e)
        {
            if (reloadCities)
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    var loadingPage = new LoadingPage();
                    await Navigation.PushPopupAsync(loadingPage);
                    var selectedState = StatePicker.Items[StatePicker.SelectedIndex];
                    var cities = await GeneralApi.GetCitiesAsync(_client, selectedState);

                    if (cities != null)
                    {
                        if (!cities.Error.Any())
                        {
                            CityPicker.Items.Clear();
                            foreach (var city in cities.List)
                            {
                                CityPicker.Items.Add(city.Name);
                            }
                        }
                        else
                        {
                            await DisplayAlert("Atenção", "Sem cidades cadastradas", "Ok");
                        }
                    }
                    else
                    {
                        await DisplayAlert("Atenção", "Sem cidades cadastradas", "Ok");
                    }
                    CloseAllPopup();
                }
                else
                {
                    Util.NotConnectedMessage(this);
                }
            }
        }

        private void PostalCode_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            switch (CultureInfo.CurrentCulture.Name)
            {
                case "pt-PT":
                    Util.CodigoPostalMask(sender, e);
                    break;

                default:
                    Util.CepMask(sender, e);
                    break;
            }
        }

        private async void PostalCode_Tapped(object sender, EventArgs e)
        {
            var isValid = true;
            if (!PostalCodeValidator.IsValid)
            {
                await DisplayAlert("Atenção", "O CEP digitado não é válido.", "Fechar");
                isValid = false;
            }

            if (isValid)
            {
                var loadingPage = new LoadingPage();
                await PopupNavigation.PushAsync(loadingPage);
                var postalCode = await GeneralApi.GetPostalCode(_client, PostalCode.Text);

                if (postalCode != null)
                {
                    if (!postalCode.Error.Any())
                    {
                        reloadCities = false;
                        for (var i = 0; i < StatePicker.Items.Count; i++)
                        {
                            var name = Util.GetStateCode(StatePicker.Items[i]);
                            if (name == postalCode.Estado)
                                StatePicker.SelectedIndex = i;
                        }

                        var selectedState = StatePicker.Items[StatePicker.SelectedIndex];
                        var cities = await GeneralApi.GetCitiesAsync(_client, selectedState, "");

                        foreach (var wsCity in cities.List)
                        {
                            CityPicker.Items.Add(wsCity.Name);
                        }

                        for (var i = 0; i < CityPicker.Items.Count; i++)
                        {
                            var name = CityPicker.Items[i];
                            if (name == postalCode.Municipio + " - " + postalCode.Estado)
                                CityPicker.SelectedIndex = i;
                        }
                        reloadCities = true;
                    }
                    else
                    {
                        var errorText = Util.GetWsErrorDescription(postalCode.Error.First());
                        await DisplayAlert("Atenção", errorText, "Fechar");
                    }
                }
                else
                {
                    await DisplayAlert("Atenção", "Não foi possível consultar seu CEP.", "Fechar");
                }
                CloseAllPopup();
            }
        }

        private async void SaveData_OnClicked(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var loadingPage = new LoadingPage();
                await PopupNavigation.PushAsync(loadingPage);
                var isValid = true;

                if (!PostalCodeValidator.IsValid)
                {
                    await DisplayAlert("Atenção", "O CEP digitado não é válido.", "Fechar");
                    isValid = false;
                }

                if ((string.IsNullOrWhiteSpace(AddressLine1.Text) || AddressLine1.Text.Length < 3) && isValid)
                {
                    await DisplayAlert("Atenção", "Digite o seu endereço.", "Fechar");
                    isValid = false;
                }

                if (string.IsNullOrWhiteSpace(AddressNumber.Text) && isValid)
                {
                    await DisplayAlert("Atenção", "Digite o número.", "Fechar");
                    isValid = false;
                }

                if ((string.IsNullOrWhiteSpace(AddressLine3.Text) || AddressLine3.Text.Length < 3) && isValid)
                {
                    await DisplayAlert("Atenção", "Digite o seu bairro.", "Fechar");
                    isValid = false;
                }

                if (StatePicker.SelectedIndex == -1 && isValid)
                {
                    await DisplayAlert("Atenção", "Escolha seu estado.", "Fechar");
                    isValid = false;
                }

                if (CityPicker.SelectedIndex == -1 && isValid)
                {
                    await DisplayAlert("Atenção", "Escolha sua cidade.", "Fechar");
                    isValid = false;
                }

                if (isValid)
                {
                    var saveDataReturn =
                        await
                            CustomerApi.AddAddress(_client, AddressLine1.Text, AddressLine2.Text, AddressLine3.Text,
                                PostalCode.Text, AddressNumber.Text, StatePicker.Items[StatePicker.SelectedIndex],
                                CityPicker.Items[CityPicker.SelectedIndex]);

                    if (!saveDataReturn.Success)
                    {
                        if (saveDataReturn.ErrorText != null)
                            await DisplayAlert("Atenção", Util.Decrypt(saveDataReturn.ErrorText, Util.OpenK()),
                                "Fechar");
                        else
                            await DisplayAlert("Atenção",
                                "Não foi possível salvar os dados. Tente novamente mais tarde.", "Fechar");
                    }
                    else
                    {
                        await DisplayAlert("Sucesso", "Cadastro atualizado!", "Ok");
                        try
                        {
                            await PopupNavigation.PopAllAsync();
                            await Navigation.PopModalAsync(true);
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
                else
                {
                    await PopupNavigation.PopAllAsync();
                }
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }
    }
}