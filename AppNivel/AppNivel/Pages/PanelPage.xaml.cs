﻿using AppNivel.Helpers;
using System;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class PanelPage : ContentPage
    {
        public PanelPage()
        {
            Settings.CurrentPage = "Panel";
            Settings.CurrentPage = "Establishment";
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            PanelLabel.Text = Language.UrlPanel;
            base.OnAppearing();
        }

        private void TapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Navigation.PopAsync(true);
        }

        private void OpenPanel_OnClicked(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri(Language.UrlPanel));
        }
    }
}