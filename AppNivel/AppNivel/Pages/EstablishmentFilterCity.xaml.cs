﻿using AppNivel.Helpers;
using AppNivel.Models;
using AppNivel.Resources;
using Plugin.Connectivity;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class EstablishmentFilterCity : ContentPage
    {
        private HttpClient _client = new HttpClient();
        private ObservableCollection<WSCity> _cities;

        public EstablishmentFilterCity()
        {
            Settings.CurrentPage = "FilterCity";
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
        }

        private void ShowList(bool show = true)
        {
            CitiesView.IsVisible = show;
        }

        protected override async void OnAppearing()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var cities = await GeneralApi.GetCitiesAsync(_client);

                if (cities != null)
                {
                    if (!cities.Error.Any())
                    {
                        _cities = new ObservableCollection<WSCity>(cities.List);
                        CitiesView.ItemsSource = _cities;
                        ShowList();
                    }
                    else
                    {
                        ShowList(false);
                        await DisplayAlert("Atenção", "Sem cidades cadastradas", "Ok");
                    }
                }
                else
                {
                    ShowList(false);
                    await DisplayAlert("Atenção", "Sem cidades cadastradas", "Ok");
                }
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
            base.OnAppearing();
        }

        private void TapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        private void City_OnTapped(object sender, ItemTappedEventArgs e)
        {
            var city = e.Item as WSCity;
            Settings.CityFilter = city.Id;
            Settings.CityFilterName = city.Name;
            this.Navigation.PopToRootAsync(true);
        }

        private void City_OnSelected(object sender, SelectedItemChangedEventArgs e)
        {
            CitiesView.SelectedItem = null;
        }
    }
}