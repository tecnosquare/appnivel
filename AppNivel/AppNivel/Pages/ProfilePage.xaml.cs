﻿using AppNivel.Helpers;
using AppNivel.Resources;
using Plugin.Connectivity;
using Rg.Plugins.Popup.Services;
using System;
using System.Globalization;
using System.Net.Http;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class ProfilePage : ContentPage
    {
        private HttpClient _client = new HttpClient();

        public ProfilePage()
        {
            Settings.CurrentPage = "Profile";
            Settings.CurrentPage = "Establishment";
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            LabelUserDocument.Text = Language.UserDocument;
            CellphoneLabel.Text = Language.Cellphone;
            UserPhone.Placeholder = Language.Cellphone;

            Username.Text = Settings.Username;
            UserFirstname.Text = Settings.UserFirstname;
            //UserLastname.Text = Settings.UserLastname;
            UserEmail.Text = Settings.UserEmail;
            UserPhone.Text = Settings.UserPhone;
            UserDocument.Text = Settings.UserDocument;
            base.OnAppearing();
        }

        private void ConfigTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Navigation.PopAsync(true);
        }

        private async void SaveData_OnClicked(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var loadingPage = new LoadingPage();
                await PopupNavigation.PushAsync(loadingPage);
                var isValid = true;
                if (!EmailValidator.IsValid)
                {
                    await DisplayAlert("Atenção", "O e-mail digitado não é válido.", "Fechar");
                    isValid = false;
                }

                if ((string.IsNullOrWhiteSpace(UserFirstname.Text) || UserFirstname.Text.Length < 2) && isValid)
                {
                    await DisplayAlert("Atenção", "Digite o seu nome.", "Fechar");
                    isValid = false;
                }

                //if ((string.IsNullOrWhiteSpace(UserLastname.Text) || UserLastname.Text.Length < 2) && isValid)
                //{
                //    await DisplayAlert("Atenção", "Digite o seu sobrenome.", "Fechar");
                //    isValid = false;
                //}

                if ((string.IsNullOrWhiteSpace(UserPhone.Text) || UserPhone.Text.Length < 5) && isValid)
                {
                    await DisplayAlert("Atenção", "Digite o seu telefone.", "Fechar");
                    isValid = false;
                }

                if (isValid)
                {
                    var saveDataReturn =
                        await
                            CustomerApi.UpdateData(_client, UserFirstname.Text, "", UserEmail.Text,
                                UserPhone.Text);

                    if (!saveDataReturn.Success)
                    {
                        if (saveDataReturn.ErrorText != null)
                            await DisplayAlert("Atenção", Util.Decrypt(saveDataReturn.ErrorText, Util.OpenK()), "Fechar");
                        else
                            await DisplayAlert("Atenção", "Não foi possível salvar os dados. Tente novamente mais tarde.", "Fechar");
                    }
                    else
                    {
                        Settings.Username = UserFirstname.Text;
                        Settings.UserFirstname = UserFirstname.Text;
                        //Settings.UserLastname = UserLastname.Text;
                        Settings.UserEmail = UserEmail.Text;
                        Settings.UserPhone = UserPhone.Text;
                        await DisplayAlert("Sucesso", "Cadastro atualizado!", "Ok");
                    }
                }

                await PopupNavigation.PopAllAsync();
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }

        private void UserPhone_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (CultureInfo.CurrentCulture.Name == "pt-BR")
            {
                Util.CelPhoneMask(sender, e);
            }
        }
    }
}