﻿using AppNivel.Helpers;
using AppNivel.Models;
using AppNivel.Resources;
using Plugin.Connectivity;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Globalization;
using System.Net.Http;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class RewardPage : ContentPage
    {
        private bool isChildRewarding = Settings.IsChildUser;
        private bool _receive = false;

        public RewardPage(bool receive = false)
        {
            _receive = receive;
            Settings.CurrentPage = "Reward";
            Settings.CurrentPage = "Establishment";
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            if (_receive)
            {
                SaveData.Text = "Receber";
                BonifyLabel.Text = "Receber";
            }
            TransactionDoc.Text = "";
            TransactionValue.Text = "";
            UserDocument.Text = "";
            if (Settings.IsChildUser)
            {
                if (Settings.TransactionValueLimit > 0 || Settings.TransactionLimit > 0)
                {
                    Limits.IsVisible = true;
                    LimitsValues.IsVisible = true;
                    LimitsValues.Text = Settings.TransactionValueLimit > 0 ? "Valor limite para transações diárias:\n" + Settings.TransactionValueLimit.ToString("C2", CultureInfo.CurrentCulture) : "";
                    LimitsValues.Text += Settings.TransactionLimit > 0 ? "\n\nLimite de transações diárias:\n" + Settings.TransactionLimit : "";
                }

                if (!Settings.TotalAccess)
                    LabelTransactionDoc.Text = "Documento da Transação (Obrigatório)";
            }
            base.OnAppearing();
        }

        private async void Reward_OnClicked(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var isValid = true;

                if (!ExtendedDocumentValidator.IsValid)
                {
                    await DisplayAlert("Atenção", "O Cpf/Cnpj digitado não é válido.", "Fechar");
                    isValid = false;
                }

                if (!TransactionValueValidator.IsValid && isValid)
                {
                    await DisplayAlert("Atenção", "O valor da venda não é válido.", "Fechar");
                    isValid = false;
                }

                if (string.IsNullOrWhiteSpace(TransactionDoc.Text) && !Settings.TotalAccess && isValid)
                {
                    await DisplayAlert("Atenção", "O número do documento é obrigatório.", "Fechar");
                    isValid = false;
                }

                if (isValid)
                {
                    if (!_receive)
                    {
                        var userExists = await EstablishmentApi.PersonExists(new HttpClient(), UserDocument.Text);

                        if (userExists == null)
                        {
                            await Navigation.PushPopupAsync(new NewBeneficiaryConfirmationPage());
                            await
                                Navigation.PushAsync(
                                    new RewardConfirmationPage(new WSPerson() { CpfCnpj = Settings.UserDocument },
                                        new WSPerson() { CpfCnpj = UserDocument.Text }, TransactionValue.Text,
                                        WSTransactionType.Credit, TransactionDoc.Text, isChildRewarding));
                        }
                        else
                        {
                            await
                                Navigation.PushAsync(
                                    new RewardConfirmationPage(new WSPerson() { CpfCnpj = Settings.UserDocument },
                                        new WSPerson() { CpfCnpj = UserDocument.Text }, TransactionValue.Text,
                                        WSTransactionType.Credit, TransactionDoc.Text, isChildRewarding));
                        }
                    }
                    else
                    {
                        var transactionId = await Util.Reward(this, new WSPerson() { CpfCnpj = UserDocument.Text },
                                        new WSPerson() { CpfCnpj = Settings.UserDocument },
                                        Convert.ToDecimal(TransactionValue.Text, CultureInfo.CurrentCulture), WSTransactionType.Debit, TransactionDoc.Text, isChildRewarding, true);

                        if (transactionId > 0)
                        {
                            await
                                Navigation.PushAsync(
                                    new ReceiveConfirmationPage(new WSPerson() { CpfCnpj = UserDocument.Text },
                                        new WSPerson() { CpfCnpj = Settings.UserDocument }, TransactionValue.Text,
                                        WSTransactionType.Debit, TransactionDoc.Text, isChildRewarding, transactionId));
                        }
                    }
                }
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }

        private void UserDocument_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (e.NewTextValue.Length > 14)
            {
                Util.CnpjMask(sender, e);
            }
            else
            {
                Util.CpfMask(sender, e);
            }
        }

        private void TransactionValue_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            Util.DecimalMask(sender, e);
        }

        private void QuestionsTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.nivel.com.br/"));
        }

        private void TapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Navigation.PopAsync(true);
        }
    }
}