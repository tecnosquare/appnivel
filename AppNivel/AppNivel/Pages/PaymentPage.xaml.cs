﻿using AppNivel.Helpers;
using AppNivel.Models;
using AppNivel.Resources;
using Plugin.Connectivity;
using System;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class PaymentPage : ContentPage
    {
        private WSEstablishment _establishment;

        public PaymentPage(WSEstablishment establishment)
        {
            _establishment = establishment;
            Settings.CurrentPage = "Payment";
            Settings.CurrentPage = "Establishment";
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            UserPassword.Placeholder = Language.PlaceholderPassword;
            EnterPwdLabel.Text = Language.EnterYourPasswordLabel;
            LabelEstablishmentDocument.Text = Language.UserDocument;
            TransactionDoc.Text = "";
            TransactionValue.Text = "";
            LabelEstablishmentDocument.Text = Util.CpfCnpjMask(_establishment.CpfCnpj);
            LabelEstablishmentName.Text = _establishment.Firstname;
            base.OnAppearing();
        }

        private async void Pay_OnClicked(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var isValid = true;

                if (!TransactionValueValidator.IsValid && isValid)
                {
                    await DisplayAlert("Atenção", "O valor da venda não é válido.", "Fechar");
                    isValid = false;
                }

                if (string.IsNullOrWhiteSpace(TransactionDoc.Text) && isValid)
                {
                    await DisplayAlert("Atenção", "O número do documento é obrigatório.", "Fechar");
                    isValid = false;
                }

                if (UserPassword.Text != Settings.UserPassword && isValid)
                {
                    await DisplayAlert("Atenção", "Senha incorreta", "Ok");
                    isValid = false;
                }

                if (isValid)
                {
                    await
                        Navigation.PushAsync(
                            new PaymentConfirmationPage(new WSPerson() { CpfCnpj = Settings.UserDocument },
                                new WSPerson() { CpfCnpj = _establishment.CpfCnpj }, TransactionValue.Text,
                                WSTransactionType.Debit, TransactionDoc.Text));
                }
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }

        private void TransactionValue_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            Util.DecimalMask(sender, e);
        }

        private void QuestionsTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.nivel.com.br/Conteudo/Ver/15819"));
        }

        private void TapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Navigation.PopAsync(true);
        }
    }
}