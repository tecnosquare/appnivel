﻿using AppNivel.Helpers;
using AppNivel.Models;
using AppNivel.Resources;
using Plugin.Connectivity;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class EstablishmentFilterCitySearch : ContentPage
    {
        private HttpClient _client = new HttpClient();
        private ObservableCollection<WSCity> _cities;

        public EstablishmentFilterCitySearch()
        {
            Settings.CurrentPage = "EstablishmentFilterCity";
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
        }

        private void ShowList(bool show = true)
        {
            CitiesView.IsVisible = show;
        }

        protected override async void OnAppearing()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                StateLabel.Text = Language.State;
                CityLabel.Text = Language.City;
                statePicker.Title = Language.State;
                HeaderBarLabel.Text = Language.SelectCity;
                var loadingPage = new LoadingPage();
                await PopupNavigation.PushAsync(loadingPage);

                var states = await GeneralApi.GetStatesAsync(_client);

                if (states != null)
                {
                    if (!states.Error.Any())
                    {
                        foreach (var wsState in states.List)
                        {
                            statePicker.Items.Add(wsState.Name);
                        }
                        statePicker.SelectedIndex = CultureInfo.CurrentCulture.Name == "pt-BR" ? 24 : 0;
                    }
                    else
                    {
                        await DisplayAlert("Atenção", "Sem estados cadastrados", "Ok");
                    }
                }
                else
                {
                    await DisplayAlert("Atenção", "Sem estados cadastrados", "Ok");
                }

                CloseAllPopup();
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
            base.OnAppearing();
        }

        private void TapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        private void City_OnTapped(object sender, ItemTappedEventArgs e)
        {
            var city = e.Item as WSCity;
            Settings.CityFilter = city.Id;
            Settings.CityFilterName = city.Name;
            this.Navigation.PopToRootAsync(true);
        }

        private void City_OnSelected(object sender, SelectedItemChangedEventArgs e)
        {
            CitiesView.SelectedItem = null;
        }

        private async void CloseAllPopup()
        {
            await Navigation.PopAllPopupAsync();
        }

        private async void SearchText_OnSearchButtonPressed(object sender, EventArgs e)
        {
            ShowList(false);
            if (CrossConnectivity.Current.IsConnected)
            {
                var selectedState = statePicker.Items[statePicker.SelectedIndex];
                var cities = await GeneralApi.GetCitiesAsync(_client, selectedState, searchText.Text);

                if (cities != null)
                {
                    if (!cities.Error.Any())
                    {
                        _cities = new ObservableCollection<WSCity>(cities.List);
                        CitiesView.ItemsSource = _cities;
                        ShowList();
                    }
                    else
                    {
                        ShowList(false);
                        await DisplayAlert("Atenção", "Sem cidades cadastradas", "Ok");
                    }
                }
                else
                {
                    ShowList(false);
                    await DisplayAlert("Atenção", "Sem cidades cadastradas", "Ok");
                }
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }

        private async void MyLocation_OnTapped(object sender, EventArgs e)
        {
            var loadingPage = new LoadingPage();
            await Navigation.PushPopupAsync(loadingPage);
#if DEBUG
            try
            {
                Settings.UserLatitude = -20.8011502D;
                Settings.UserLongitude = -49.3971176D;
                var closestCity =
                    await GeneralApi.GetClosestCity(_client, Settings.UserLatitude, Settings.UserLongitude, false);
                if (closestCity != null)
                {
                    Settings.CityFilter = closestCity.Id;
                    Settings.CityFilterName = closestCity.Name;
                }
                else
                {
                    await DisplayAlert("Atenção", "Não foi possível encontrar sua localização", "Ok");
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Atenção", "Não foi possível encontrar sua localização", "Ok");
            }
#else
            try
            {
                var locator = CrossGeolocator.Current;
                if (!locator.IsGeolocationAvailable)
                {
                    await DisplayAlert("Atenção", "Você precisar ativar sua localização para utilizar esta funcionalidade!", "Ok");
                }
                else
                {
                    locator.DesiredAccuracy = 50;
                    var position = await locator.GetPositionAsync(timeoutMilliseconds: 20000);
                    if (!position.Latitude.Equals(0D) || !position.Longitude.Equals(0D))
                    {
                        Settings.UserLatitude = position.Latitude;
                        Settings.UserLongitude = position.Longitude;
                        var closestCity =
                            await GeneralApi.GetClosestCity(_client, position.Latitude, position.Longitude, false);
                        if (closestCity != null)
                        {
                            Settings.CityFilter = closestCity.Id;
                            Settings.CityFilterName = closestCity.Name;
                        }
                        else
                        {
                            await DisplayAlert("Atenção", "Não foi possível encontrar sua localização", "Ok");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Atenção", "Não foi possível encontrar sua localização", "Ok");
                Settings.UserLatitude = 0D;
                Settings.UserLongitude = 0D;
            }
#endif

            CloseAllPopup();

            await Navigation.PopToRootAsync(true);
        }
    }
}