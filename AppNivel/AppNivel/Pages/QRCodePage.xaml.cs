﻿using AppNivel.Helpers;
using AppNivel.Models;
using AppNivel.Resources;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class QRCodePage : LandscapePage
    {
        private HttpClient _client = new HttpClient();

        public QRCodePage(WSQrCode qrCode)
        {
            Settings.CurrentPage = "QRCode";
            Settings.CurrentPage = "Establishment";
            InitializeComponent();
            Settings.GenerateQr = true;
            BindingContext = qrCode;
            //Code.Text = "Token " + qrCode.Code;
            UserBalance.Text = Settings.UserBalance.ToString("N2", CultureInfo.CurrentCulture); ;
            UserDisplayName.Text = Settings.UserFirstname.Trim() + " " + Settings.UserLastname.Trim();
            UserDocument.Text = Settings.UserDocument.Trim().PadLeft(16, '0').Insert(4, ".").Insert(9, ".").Insert(14, ".");
            RunTask(qrCode);
        }

        private void RunTask(WSQrCode qrCode)
        {
            Device.StartTimer(TimeSpan.FromTicks(qrCode.Remaining), () =>
            {
                if (Settings.GenerateQr)
                {
                    Task.Factory.StartNew(async () =>
                    {
                        var code = await UpdateQrCode();
                        // Switch back to the UI thread to update the UI
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            BindingContext = code;
                            RunTask(code);
                        });
                    });
                }
                return false;
            });
        }

        private async Task<WSQrCode> UpdateQrCode()
        {
            return await TransactionApi.GetAuthorizationQr(_client);
        }

        protected override async void OnAppearing()
        {
            MessagingCenter.Send(this, "LandscapeOnly");
            var wsReturn = await UpdateQrCode();

            if (wsReturn != null)
            {
                if (!(string.IsNullOrEmpty(wsReturn.EstablishmentLogoPath)))
                {
                    LogoEstablishment.Source =
                        new UriImageSource { CachingEnabled = true, Uri = new Uri(wsReturn.EstablishmentLogoPath), CacheValidity = TimeSpan.FromDays(2) };
                }
                else
                {
                    LogoEstablishment.Source = "logowhite.png";
                }
            }
            else
            {
                LogoEstablishment.Source = "logowhite.png";
            }

            base.OnAppearing();
        }

        //protected override void OnAppearing()
        //{
        //    MessagingCenter.Send(this, "LandscapeOnly");
        //    base.OnAppearing();

        //    //CloseImage.Rotation = 30;
        //    //CloseImage.Scale = 0.3;
        //    //CloseImage.Opacity = 0;
        //}

        private void OnCloseButtonTapped(object sender, EventArgs e)
        {
            Navigation.PopModalAsync(true);
        }

        private async void CloseAllPopup()
        {
            await Navigation.PopAllPopupAsync();
        }

        protected override void OnDisappearing()
        {
            Settings.GenerateQr = false;
            MessagingCenter.Send(this, "PortraitOnly");
            base.OnDisappearing();
        }
    }
}