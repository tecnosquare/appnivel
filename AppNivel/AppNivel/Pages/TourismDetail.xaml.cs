﻿using AppNivel.Helpers;
using AppNivel.Models;
using AppNivel.Resources;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using Xamarin.Forms;

namespace AppNivel.Pages
{
    public partial class TourismDetail : ContentPage
    {
        private HttpClient _client = new HttpClient();
        private WSAddress _wsAddress;
        private string _address;
        private string _completeAddress;
        private WSEstablishment _establishment;
        private WSContact _telephone;

        private string _mondayDiscount;
        private string _tuesdayDiscount;
        private string _wednesdayDiscount;
        private string _thursdayDiscount;
        private string _fridayDiscount;
        private string _saturdayDiscount;
        private string _sundayDiscount;

        private string _mondayDiscountCashback;
        private string _tuesdayDiscountCashback;
        private string _wednesdayDiscountCashback;
        private string _thursdayDiscountCashback;
        private string _fridayDiscountCashback;
        private string _saturdayDiscountCashback;
        private string _sundayDiscountCashback;

        private string _mondayDiscountPes;
        private string _tuesdayDiscountPes;
        private string _wednesdayDiscountPes;
        private string _thursdayDiscountPes;
        private string _fridayDiscountPes;
        private string _saturdayDiscountPes;
        private string _sundayDiscountPes;

        public TourismDetail(WSEstablishment establishment)
        {
            Settings.CurrentPage = "Establishment";
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            BindingContext = establishment;
            _establishment = establishment;
        }

        protected override void OnAppearing()
        {
            //BusinessActivity.IsVisible = !string.IsNullOrWhiteSpace(_establishment.BusinessActivity);
            _wsAddress = _establishment.Address.FirstOrDefault();
            if (_wsAddress != null)
            {
                _address = Util.GetAddressMin(_wsAddress);
                _completeAddress = Util.GetAddress(_wsAddress);
                Address.Text = _completeAddress;
            }
            var typePhone = (int)ContactType.Telefone;
            var telephone = _establishment.Contact.FirstOrDefault(x => x.ContactTypeId == typePhone && x.Main);
            _telephone = telephone;
            if (telephone != null)
            {
                phone.IsVisible = true;
                phoneLabel.IsVisible = true;
                phone.Text = telephone.ContactValue;
            }
            imageHeader.Source = string.IsNullOrWhiteSpace(_establishment.BackgroundImage) ? "semimagem.jpg" : _establishment.BackgroundImage;

            base.OnAppearing();
        }

        private void TapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        private void Address_OnTapped(object sender, EventArgs e)
        {
            switch (Device.OS)
            {
                case TargetPlatform.iOS:
                    Device.OpenUri(new Uri($"http://maps.apple.com/?q={WebUtility.UrlEncode(_completeAddress)}"));
                    break;

                case TargetPlatform.Android:
                    Device.OpenUri(new Uri($"geo:0,0?q={WebUtility.UrlEncode(_completeAddress)}"));
                    break;

                case TargetPlatform.Other:
                    break;

                case TargetPlatform.WinPhone:
                    break;

                case TargetPlatform.Windows:
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void Phone_OnTapped(object sender, EventArgs e)
        {
            if (_telephone != null)
                Device.OpenUri(new Uri($"tel:{_telephone.ContactValue}"));
        }
    }
}