﻿using AppNivel.Helpers;
using AppNivel.Models;
using AppNivel.Pages;
using AppNivel.Resources;
using Newtonsoft.Json;
using PushNotification.Plugin;
using System;
using System.Globalization;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

namespace AppNivel
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            Resources["DefaultStringResources"] = new AppNivel.GrialTheme.Resx.AppResources();
            var date = Convert.ToDateTime("19/10/2016 08:52:00.000", new CultureInfo("pt-BR"));
            var appToken = Settings.AppToken;
            if (string.IsNullOrEmpty(appToken))
            {
                Settings.AppToken = Util.Encrypt(JsonConvert.SerializeObject(new TokenModel { AppId = 1, Date = date.Ticks }), Util.OpenK());
            }

            Settings.CurrentView = (int)ViewPages.Offers;
            Settings.BusinessActivityFilter = 0;
            Settings.UserLatitude = 0D;
            Settings.UserLongitude = 0D;
            Settings.EmailBeneficiary = string.Empty;
            Settings.CurrentPage = "Main";
            if (string.IsNullOrEmpty(Settings.SelectedCulture) && Settings.IsLogged)
            {
                //Setar pt-BR para usuários já logados
                Settings.SelectedCulture = "pt-BR";
            }

            var ci = new CultureInfo(Settings.SelectedCulture);

            if (Settings.CityFilter == 0)
            {
                switch (ci.Name)
                {
                    case "pt-PT":
                        Settings.CityFilter = 5830;
                        Settings.CityFilterName = "Lisboa - Lisboa";
                        break;

                    default:
                        Settings.CityFilter = 5264;
                        Settings.CityFilterName = "São José do Rio Preto - SP";
                        break;
                }
            }

            if (string.IsNullOrEmpty(Settings.SelectedCulture) && !Settings.IsLogged)
            {
                MainPage = new NavigationPage(new CountrySelectionPage());
                ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
            }
            else
            {
                //MainPage = Settings.IsLogged ? new NavigationPage(new MainPage()) : new NavigationPage(new AppNivel.GrialTheme.Views.Logins.LoginPage());
                MainPage = Settings.IsLogged ? new NavigationPage(new MainPage()) : new NavigationPage(new AppNivel.GrialTheme.Views.Logins.LoginPage());
            }
            SampleData.Initialize();
            SamplesDefinition.Initialize();

            CrossPushNotification.Current.Register();
            Language.Culture = ci;
            DependencyService.Get<ILocalize>().SetLocale(ci);
            Settings.AppWsUrl = Language.WSUrl;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}