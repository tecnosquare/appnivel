﻿namespace AppNivel.Resources
{
    public interface ICloseApplication
    {
        void QuitApplication();
    }
}