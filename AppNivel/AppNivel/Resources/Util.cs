﻿using AppNivel.Helpers;
using AppNivel.Models;
using AppNivel.Pages;
using PCLCrypto;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppNivel.Resources
{
    public enum ViewPages
    {
        Establishments = 1,
        Favorites = 2,
        Offers = 3,
        PositionFilter = 4,
        Tourism = 5,
        HotOffer = 6,
    }

    public class FavoriteImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? "favorite.png" : "_favorite.png";
        }

        // No need to implement converting back on a one-way binding
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? "favorite.png" : "_favorite.png";
        }
    }

    public class DistanceToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var ret = "";
            var km = (decimal)value;
            if (km <= 0 || km > 5000) return ret;
            if (km > 1)
                ret = km.ToString("N2", CultureInfo.CurrentCulture) + " km";
            else
                ret = (km * 100).ToString("N2", CultureInfo.CurrentCulture) + " m";
            return ret;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var ret = "";
            var km = (decimal)value;
            if (km <= 0 || km > 5000) return ret;
            if (km > 1)
                ret = km.ToString("N2", CultureInfo.CurrentCulture) + " km";
            else
                ret = (km * 100).ToString("N2", CultureInfo.CurrentCulture) + " m";
            return ret;
        }
    }

    public class DistanceToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var km = (decimal)value;
            return !(km <= 0 || km > 5000);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var km = (decimal)value;
            return !(km <= 0 || km > 5000);
        }
    }

    public class BalanceSelectedConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? "selected.png" : "_selected.png";
        }

        // No need to implement converting back on a one-way binding
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? "selected.png" : "_selected.png";
        }
    }

    public class InverseBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }
    }

    public class DecimalToMoneyString : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((decimal)value).ToString("C2", CultureInfo.CurrentCulture);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((decimal)value).ToString("C2", CultureInfo.CurrentCulture);
        }
    }

    public class DecimalToMoney4String : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((decimal)value).ToString("C4", CultureInfo.CurrentCulture);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((decimal)value).ToString("C4", CultureInfo.CurrentCulture);
        }
    }

    public class TicksToDate : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new DateTime((long)value).ToString("dd/MM/yyyy");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new DateTime((long)value).ToString("dd/MM/yyyy");
        }
    }

    public class TicksToDateTime : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new DateTime((long)value).ToString("dd/MM/yyyy HH:mm:ss");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new DateTime((long)value).ToString("dd/MM/yyyy HH:mm:ss");
        }
    }

    public class SignalLabel : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? "+" : "-";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? "+" : "-";
        }
    }

    public class CreditLabel : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? "Crédito" : "Débito";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? "Crédito" : "Débito";
        }
    }

    public class TransactionLabel : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Util.GetTransactionType((WSTransactionType)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Util.GetTransactionType((WSTransactionType)value);
        }
    }

    internal class Util
    {
        public static string OpenK()
        {
            return "8023y49823rtg237rg723f2!@!%Ë$@&¨@(*#$)*335f/*-*";
        }

        public static string Base64Decode(string data)
        {
            byte[] toDecodeByte = Convert.FromBase64String(data);

            UTF8Encoding encoder = new System.Text.UTF8Encoding();
            Decoder utf8Decode = encoder.GetDecoder();

            int charCount = utf8Decode.GetCharCount(toDecodeByte, 0, toDecodeByte.Length);

            char[] decodedChar = new char[charCount];
            utf8Decode.GetChars(toDecodeByte, 0, toDecodeByte.Length, decodedChar, 0);
            string result = new String(decodedChar);
            return result;
        }

        public static string Encrypt(string plainText, string pass)
        {
            var crip = EncryptAes(plainText, pass, GetSalt());
            return Convert.ToBase64String(crip);
        }

        public static string Decrypt(string encryptedText, string pass)
        {
            encryptedText = encryptedText.Replace("||", "+");
            byte[] cipherTextBytes = Convert.FromBase64String(encryptedText);
            return Util.DecryptAes(cipherTextBytes, pass, GetSalt());
        }

        /// <summary>
        /// Creates Salt with given length in bytes.
        /// </summary>
        /// <param name="lengthInBytes">No. of bytes</param>
        /// <returns></returns>
        public static byte[] CreateSalt(int lengthInBytes)
        {
            return WinRTCrypto.CryptographicBuffer.GenerateRandom(lengthInBytes);
        }

        /// <summary>
        /// Creates a derived key from a comnination
        /// </summary>
        /// <param name="password"></param>
        /// <param name="salt"></param>
        /// <param name="keyLengthInBytes"></param>
        /// <param name="iterations"></param>
        /// <returns></returns>
        public static byte[] CreateDerivedKey(string password, byte[] salt, int keyLengthInBytes = 32, int iterations = 1000)
        {
            byte[] key = NetFxCrypto.DeriveBytes.GetBytes(password, salt, iterations, keyLengthInBytes);
            return key;
        }

        /// <summary>
        /// Encrypts given data using symmetric algorithm AES
        /// </summary>
        /// <param name="data">Data to encrypt</param>
        /// <param name="password">Password</param>
        /// <param name="salt">Salt</param>
        /// <returns>Encrypted bytes</returns>
        public static byte[] EncryptAes(string data, string password, byte[] salt)
        {
            byte[] key = CreateDerivedKey(password, salt);

            ISymmetricKeyAlgorithmProvider aes = WinRTCrypto.SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithm.AesCbcPkcs7);
            ICryptographicKey symetricKey = aes.CreateSymmetricKey(key);
            var bytes = WinRTCrypto.CryptographicEngine.Encrypt(symetricKey, Encoding.UTF8.GetBytes(data));
            return bytes;
        }

        /// <summary>
        /// Decrypts given bytes using symmetric alogrithm AES
        /// </summary>
        /// <param name="data">data to decrypt</param>
        /// <param name="password">Password used for encryption</param>
        /// <param name="salt">Salt used for encryption</param>
        /// <returns></returns>
        public static string DecryptAes(byte[] data, string password, byte[] salt)
        {
            byte[] key = CreateDerivedKey(password, salt);

            ISymmetricKeyAlgorithmProvider aes = WinRTCrypto.SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithm.AesCbcPkcs7);
            ICryptographicKey symetricKey = aes.CreateSymmetricKey(key);
            var bytes = WinRTCrypto.CryptographicEngine.Decrypt(symetricKey, data);
            return Encoding.UTF8.GetString(bytes, 0, bytes.Length);
        }

        public static string Saudacao()
        {
            string saudacao = string.Empty;
            if ((DateTime.Now.Hour < 12))
                saudacao = "Bom dia";
            if (((DateTime.Now.Hour > 12) && (DateTime.Now.Hour < 18)) || DateTime.Now.Hour == 12)
                saudacao = "Boa tarde";
            if ((DateTime.Now.Hour > 18))
                saudacao = "Boa noite";
            return saudacao;
        }

        public static byte[] GetSalt()
        {
            return Encoding.UTF8.GetBytes("357i!&(36b789#");
        }

        public static string GetAddressMin(WSAddress wsAddress)
        {
            return wsAddress.AddressLine1 +
                   (!string.IsNullOrWhiteSpace(wsAddress.Number) ? ", " + wsAddress.Number : "") +
                   (!string.IsNullOrWhiteSpace(wsAddress.AddressLine2) ? ", " + wsAddress.AddressLine2 : "") +
                   (!string.IsNullOrWhiteSpace(wsAddress.AddressLine3) ? ", " + wsAddress.AddressLine3 : "");
        }

        public static string GetAddress(WSAddress wsAddress)
        {
            var address = (!string.IsNullOrWhiteSpace(wsAddress.AddressLine1) ? wsAddress.AddressLine1 + ", " : "") +
                          (!string.IsNullOrWhiteSpace(wsAddress.Number) ? wsAddress.Number + ", " : "") +
                          (!string.IsNullOrWhiteSpace(wsAddress.AddressLine2) ? wsAddress.AddressLine2 + ", " : "") +
                          (!string.IsNullOrWhiteSpace(wsAddress.AddressLine3) ? wsAddress.AddressLine3 + ", " : "") +
                          (!string.IsNullOrWhiteSpace(wsAddress.City) ? wsAddress.City + ", " : "");
            address = address.Substring(0, address.Length - 2);
            address += (!string.IsNullOrWhiteSpace(wsAddress.State) ? " - " + wsAddress.State : "");

            return address;
        }

        public static void SetInitialAndEndDates(ref DateTime initDate, ref DateTime endDate, int month)
        {
            initDate = new DateTime(DateTime.Now.Year, month, 1);
            endDate = new DateTime(initDate.Year, initDate.Month, DateTime.DaysInMonth(initDate.Year, initDate.Month));
        }

        public static void CpfMask(object sender, EventArgs e)
        {
            var ev = e as TextChangedEventArgs;

            if (ev.NewTextValue != ev.OldTextValue)
            {
                var entry = (Entry)sender;
                var text = Regex.Replace(ev.NewTextValue, @"[^0-9]", "");

                text = text.PadRight(11);

                // removendo todos os digitos excedentes
                if (text.Length > 11)
                {
                    text = text.Remove(11);
                }

                text = text.Insert(3, ".").Insert(7, ".").Insert(11, "-").TrimEnd(new char[] { ' ', '.', '-' });
                if (entry.Text != text)
                    entry.Text = text;
            }
        }

        public static void NifMask(object sender, EventArgs e)
        {
            var ev = e as TextChangedEventArgs;

            if (ev.NewTextValue != ev.OldTextValue)
            {
                var entry = (Entry)sender;
                var text = Regex.Replace(ev.NewTextValue, @"[^0-9]", "");

                text = text.PadRight(9);

                // removendo todos os digitos excedentes
                if (text.Length > 9)
                {
                    text = text.Remove(9);
                }

                text = text.TrimEnd(new char[] { ' ', '.', '-', '/' });
                if (entry.Text != text)
                    entry.Text = text;
            }
        }

        public static void CnpjMask(object sender, EventArgs e)
        {
            var ev = e as TextChangedEventArgs;

            if (ev.NewTextValue != ev.OldTextValue)
            {
                var entry = (Entry)sender;
                var text = Regex.Replace(ev.NewTextValue, @"[^0-9]", "");

                text = text.PadRight(14);

                // removendo todos os digitos excedentes
                if (text.Length > 14)
                {
                    text = text.Remove(14);
                }

                text = text.Insert(2, ".").Insert(6, ".").Insert(10, "/").Insert(15, "-").TrimEnd(new char[] { ' ', '.', '-', '/' });
                if (entry.Text != text)
                    entry.Text = text;
            }
        }

        public static string CpfCnpjMask(string cpfCnpj)
        {
            if (CultureInfo.CurrentCulture.ToString() == "pt-BR")
            {
                var text = Regex.Replace(cpfCnpj, @"[^0-9]", "");
                return text.Length == 11 ? text.Insert(3, ".").Insert(7, ".").Insert(11, "-").TrimEnd(new char[] { ' ', '.', '-' }) : text.Insert(2, ".").Insert(6, ".").Insert(10, "/").Insert(15, "-").TrimEnd(new char[] { ' ', '.', '-', '/' });
            }
            return cpfCnpj;
        }

        public static string CnpjMask(string cnpj)
        {
            var text = Regex.Replace(cnpj, @"[^0-9]", "");

            text = text.PadRight(14);

            // removendo todos os digitos excedentes
            if (text.Length > 14)
            {
                text = text.Remove(14);
            }

            return text.Insert(2, ".").Insert(6, ".").Insert(10, "/").Insert(15, "-").TrimEnd(new char[] { ' ', '.', '-', '/' });
        }

        public static string CpfMask(string cpf)
        {
            var text = Regex.Replace(cpf, @"[^0-9]", "");

            text = text.PadRight(14);

            text = text.PadRight(11);

            // removendo todos os digitos excedentes
            if (text.Length > 11)
            {
                text = text.Remove(11);
            }

            return text.Insert(3, ".").Insert(7, ".").Insert(11, "-").TrimEnd(new char[] { ' ', '.', '-' });
        }

        public static void DecimalMask(object sender, EventArgs e)
        {
            var ev = e as TextChangedEventArgs;

            if (ev.NewTextValue != ev.OldTextValue)
            {
                var entry = (Entry)sender;
                string text = ev.NewTextValue;
                text = Regex.Replace(text, @"[^0-9]", "");

                if (text.Length < 3)
                {
                    text = text.PadLeft(3, '0');
                }
                text = text.Insert(text.Length - 2, ",").TrimEnd(new char[] { ' ', '.', ',' });
                var leftToComma = text.Split(',')[0];
                if (leftToComma.Length > 1)
                {
                    text = leftToComma == "00" ? text.Substring(1, text.Length - 1) : text.TrimStart('0');
                }
                if (entry.Text != text)
                    entry.Text = text;
            }
        }

        public static void CelPhoneMask(object sender, EventArgs e)
        {
            var ev = e as TextChangedEventArgs;

            if (ev.NewTextValue != ev.OldTextValue)
            {
                var entry = (Entry)sender;
                string text = Regex.Replace(ev.NewTextValue, @"[^0-9]", "");
                text = text.PadRight(11);
                text = text.Insert(0, "(").Insert(3, ")").Insert(4, " ").TrimEnd(new char[] { ' ', '.', '-', '(', ')' });
                if (entry.Text != text)
                    entry.Text = text;
            }
        }

        public static void CepMask(object sender, EventArgs e)
        {
            var ev = e as TextChangedEventArgs;

            if (ev.NewTextValue != ev.OldTextValue)
            {
                var entry = (Entry)sender;
                string text = Regex.Replace(ev.NewTextValue, @"[^0-9]", "");
                text = text.PadRight(9);
                if (text.Length > 8)
                {
                    text = text.Remove(8);
                }
                text = text.Insert(5, "-").TrimEnd(new char[] { ' ', '.', '-', '(', ')' });
                if (entry.Text != text)
                    entry.Text = text;
            }
        }

        public static void CodigoPostalMask(object sender, EventArgs e)
        {
            var ev = e as TextChangedEventArgs;

            if (ev.NewTextValue != ev.OldTextValue)
            {
                var entry = (Entry)sender;
                string text = Regex.Replace(ev.NewTextValue, @"[^0-9]", "");
                text = text.PadRight(8);
                if (text.Length > 7)
                {
                    text = text.Remove(7);
                }
                text = text.Insert(4, "-").TrimEnd(new char[] { ' ', '.', '-', '(', ')' });
                if (entry.Text != text)
                    entry.Text = text;
            }
        }

        public static string GetWsErrorDescription(int error)
        {
            switch (error)
            {
                case 0:
                    return "Erro inesperado.";

                case 1:
                    return "Requisição inválida.";

                case 2:
                    return "Conteúdo JSON inválido.";

                case 3:
                    return "CPF inválido.";

                case 4:
                    return "Usuário e/ou senha inválido(s).";

                case 5:
                    return "Token inválido.";

                case 6:
                    return "Critério inválido.";

                case 7:
                    return "Código do dispositivo requerido.";

                case 8:
                    return "Muitos erros.";

                case 9:
                    return "Já registrado.";

                case 10:
                    return "E-mail inválido.";

                case 11:
                    return "Senha muito fraca.";

                case 12:
                    return "CPF já registrado.";

                case 13:
                    return "Uma atualização do aplicativo é necessária para continuar.";

                case 14:
                    return "Cedente inválido.";

                case 15:
                    return "Beneficiário inválido.";

                case 16:
                    return "Comissário inválido.";

                case 17:
                    return "Valores de transação inválidos.";

                case 18:
                    return "Percentual de PE$ inválido.";

                case 19:
                    return "Percentual de Cashback inválido.";

                case 20:
                    return "Data da transação inválida.";

                case 21:
                    return "Código de verificação inválido.";

                case 22:
                    return "Motivo de cancelamento inválido.";

                case 23:
                    return "Tipo de transação inválido.";

                case 24:
                    return "Taxa inválida.";

                case 25:
                    return "Parente de transação inválido.";

                case 26:
                    return "Comissário inválido.";

                case 27:
                    return "Transação idêntica.";

                case 28:
                    return "Cedente e Beneficiário são os mesmos.";

                case 29:
                    return "Endereço de IP inválido.";

                case 30:
                    return "Valor máximo diário de transferências atingido.";

                case 31:
                    return "Quantidade máxima diária de transferências atingida.";

                case 32:
                    return "Uma transação esta sendo executada.";

                case 33:
                    return "Não autorizada.";

                case 34:
                    return "Cliente inválido.";

                case 35:
                    return "Estabelecimento inválido.";

                case 36:
                    return "A soma dos bônus é inválida.";

                case 37:
                    return "Transação não encontrada.";

                case 38:
                    return "Transação inválida.";

                case 39:
                    return "Transação inválida.";

                case 40:
                    return "Operação negada.";

                case 41:
                    return "Transação inválida.";

                case 42:
                    return "Erro de arquivo.";

                case 43:
                    return "Telefone inválido";

                case 44:
                    return "O nome é necessário.";

                case 45:
                    return "O sobrenome é necessário.";

                case 46:
                    return "Usuário não encontrado.";

                case 47:
                    return "Nome inválido.";

                case 48:
                    return "Sobrenome inválido.";

                case 49:
                    return "Tipo de pessoa inválido.";

                case 50:
                    return "Sem indicações restantes.";

                case 51:
                    return "Erro ao enviar e-mail.";

                case 52:
                    return "Não pertence ao comissário.";

                case 53:
                    return "Erro ao desconectar.";

                case 54:
                    return "Usuário inválido.";

                case 55:
                    return "É necessário informar um número de documento.";

                case 56:
                    return "Limites de transação atingidos.";

                case 57:
                    return "Valor da comissão incorreto.";

                case 58:
                    return "Transação referenciada inválida.";

                case 59:
                    return "Valor do resgate de saldo inválido.";

                case 60:
                    return "Titular inválido.";

                case 61:
                    return "Banco inválido.";

                case 62:
                    return "Agência bancária inválida.";

                case 63:
                    return "Conta corrente inválida.";

                case 64:
                    return "Erro ao criar transação de resgate de saldo.";

                case 65:
                    return "Solicitação de resgate não encontrada.";

                case 66:
                    return "O valor da transação deve ser de no mínimo R$ 1,00 para bonificação.";

                case 67:
                    return "CEP inválido.";

                default:
                    return "Erro não especificado";
            }
        }

        public static bool VerifyNif(string nif)
        {
            nif = nif.Trim();
            nif = Regex.Replace(nif, "[^0-9]", string.Empty);
            if (nif.Length != 9)
                return false;
            var nifArray = nif.ToCharArray();
            var checkDigit = 0;
            for (var i = 0; i < 8; i++)
            {
                checkDigit += nifArray[i] * (10 - i - 1);
            }
            checkDigit = 11 - (checkDigit % 11);
            if (checkDigit >= 10) checkDigit = 0;
            return checkDigit == Convert.ToInt32(nifArray[8].ToString());
        }

        public static bool VerifyCpf(string cpf)
        {
            // Caso coloque todos os numeros iguais
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;
            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");
            if (cpf.Length != 11)
                return false;
            tempCpf = cpf.Substring(0, 9);
            soma = 0;
            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cpf.EndsWith(digito);
        }

        public static bool VerifyCnpj(string cnpj)
        {
            int[] multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int soma;
            int resto;
            string digito;
            string tempCnpj;
            cnpj = cnpj.Trim();
            cnpj = cnpj.Replace(".", "").Replace("-", "").Replace("/", "");
            if (cnpj.Length != 14)
                return false;
            tempCnpj = cnpj.Substring(0, 12);
            soma = 0;
            for (int i = 0; i < 12; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCnpj = tempCnpj + digito;
            soma = 0;
            for (int i = 0; i < 13; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cnpj.EndsWith(digito);
        }

        public static async void NotConnectedMessage(Page page)
        {
            await page.DisplayAlert("Atenção", "Você não está conectado! Conecte-se para navegar pelo aplicativo.", "Ok");
        }

        public static string GetTransactionStatus(WSTransactionStatus transactionTransactionStatus)
        {
            var transactionDescription = "";
            switch (transactionTransactionStatus)
            {
                case WSTransactionStatus.Authorized:
                    transactionDescription = "Transação autorizada";
                    break;

                case WSTransactionStatus.Received:
                    transactionDescription = "Transação recebida";
                    break;

                case WSTransactionStatus.Processing:
                    transactionDescription = "Transação em processamento";
                    break;

                case WSTransactionStatus.WaitingVerificationCode:
                    transactionDescription = "Aguardando código de verificação";
                    break;

                case WSTransactionStatus.Canceled:
                    transactionDescription = "Transação cancelada";
                    break;

                case WSTransactionStatus.NotAuthorized:
                    transactionDescription = "Transação não autorizada";
                    break;

                default:
                    transactionDescription = "Ocorreu um problema ao realizar sua transação. Entre em contato com a Nível.";
                    break;
            }

            return transactionDescription;
        }

        public static string GetTransactionType(WSTransactionType transactionType)
        {
            var transactionTypeDescription = "";
            switch (transactionType)
            {
                case WSTransactionType.Credit:
                    transactionTypeDescription = "Crédito";
                    break;

                case WSTransactionType.Debit:
                    transactionTypeDescription = "Débito";
                    break;

                case WSTransactionType.Transfer:
                    transactionTypeDescription = "Transferência";
                    break;

                case WSTransactionType.Buy:
                    transactionTypeDescription = "Compra de créditos";
                    break;

                case WSTransactionType.Commission:
                    transactionTypeDescription = "Premiação";
                    break;

                case WSTransactionType.Redeem:
                    transactionTypeDescription = "Resgate";
                    break;

                case WSTransactionType.RvRecarga:
                    transactionTypeDescription = "Recarga de celular";
                    break;

                default:
                    transactionTypeDescription = "";
                    break;
            }

            return transactionTypeDescription;
        }

        public static async Task<int> Reward(Page page, WSPerson sender, WSPerson beneficiary, decimal transactionValue, WSTransactionType transactionType, string transactionDoc, bool isChildRewarding, bool proceed = false)
        {
            var loadingPage = new LoadingPage();
            await page.Navigation.PushPopupAsync(loadingPage);

            var transaction = await EstablishmentApi.Transaction(new HttpClient(), sender.CpfCnpj, beneficiary.CpfCnpj, transactionValue, transactionType, transactionDoc, isChildRewarding, Settings.EmailBeneficiary, !string.IsNullOrEmpty(Settings.EmailBeneficiary));
            Settings.EmailBeneficiary = string.Empty;
            if (transaction != null)
            {
                if (transaction.Error.Any())
                {
                    var errorText = GetWsErrorDescription(transaction.Error.First());
                    await page.DisplayAlert("Atenção", errorText, "Fechar");
                }
                else
                {
                    if (proceed)
                    {
                        await page.Navigation.PopAllPopupAsync();
                        return transaction.Id;
                    }

                    await
                        page.DisplayAlert("Mensagem",
                            GetTransactionStatus((WSTransactionStatus)transaction.TransactionStatus), "Ok");

                    //Recibo
                    await page.Navigation.PushModalAsync(new ReceiptPage(sender, beneficiary, transactionValue.ToString("C2"), transactionType, transactionDoc, isChildRewarding));

                    if (transactionType == WSTransactionType.Transfer)
                    {
                        await page.Navigation.PopAllPopupAsync();
                        await page.Navigation.PopToRootAsync(true);
                    }
                    else
                    {
                        var answer =
                        await
                            page.DisplayAlert("Mensagem", "O que deseja fazer agora?", "Nova Bonificação", "Fechar");

                        if (answer)
                        {
                            await page.Navigation.PopAllPopupAsync();
                            await page.Navigation.PopAsync();
                        }
                        else
                        {
                            await page.Navigation.PopAllPopupAsync();
                            await page.Navigation.PopToRootAsync(true);
                        }
                    }

                    return transaction.Id;
                }
            }
            else
            {
                await page.DisplayAlert("Erro", "Erro ao realizar a transação. Tente novamente mais tarde.", "Ok");
            }
            await PopupNavigation.PopAllAsync();
            return 0;
        }

        public static async Task<int> Pay(Page page, WSPerson sender, WSPerson beneficiary, decimal transactionValue, WSTransactionType transactionType, string transactionDoc, bool isChildRewarding, bool proceed = false)
        {
            var loadingPage = new LoadingPage();
            await page.Navigation.PushPopupAsync(loadingPage);

            var transaction = await EstablishmentApi.Transaction(new HttpClient(), sender.CpfCnpj, beneficiary.CpfCnpj, transactionValue, transactionType, transactionDoc, isChildRewarding, Settings.EmailBeneficiary, !string.IsNullOrEmpty(Settings.EmailBeneficiary));
            Settings.EmailBeneficiary = string.Empty;
            if (transaction != null)
            {
                if (transaction.Error.Any())
                {
                    var errorText = GetWsErrorDescription(transaction.Error.First());
                    await page.DisplayAlert("Atenção", errorText, "Fechar");
                }
                else
                {
                    if (proceed)
                    {
                        await page.Navigation.PopAllPopupAsync();
                        return transaction.Id;
                    }

                    if ((WSTransactionStatus)transaction.TransactionStatus == WSTransactionStatus.WaitingVerificationCode)
                    {
                        var transactionConfirm = await EstablishmentApi.ConfirmTransaction(new HttpClient(), transaction.Id, "", true);

                        if (transactionConfirm.Error.Any())
                        {
                            var errorText = GetWsErrorDescription(transaction.Error.First());
                            await page.DisplayAlert("Atenção", errorText, "Fechar");
                        }
                        else
                        {
                            await
                                page.DisplayAlert("Mensagem", "Pagamento efetuado", "Ok");

                            //Recibo
                            await page.Navigation.PushModalAsync(new ReceiptPage(sender, beneficiary, transactionValue.ToString("C2"), transactionType, transactionDoc, isChildRewarding));
                        }
                    }
                    else
                    {
                        await
                            page.DisplayAlert("Mensagem",
                                GetTransactionStatus((WSTransactionStatus)transaction.TransactionStatus), "Ok");
                    }

                    await page.Navigation.PopAllPopupAsync();
                    await page.Navigation.PopToRootAsync(true);

                    return transaction.Id;
                }
            }
            else
            {
                await page.DisplayAlert("Erro", "Erro ao realizar a transação. Tente novamente mais tarde.", "Ok");
            }
            await PopupNavigation.PopAllAsync();
            return 0;
        }

        public static async Task<bool> ConfirmTransaction(Page page, int transactionId, string verificationCode)
        {
            var loadingPage = new LoadingPage();
            await page.Navigation.PushPopupAsync(loadingPage);

            var transaction = await EstablishmentApi.ConfirmTransaction(new HttpClient(), transactionId, verificationCode);
            Settings.EmailBeneficiary = string.Empty;
            if (transaction != null)
            {
                if (transaction.Error.Any())
                {
                    var errorText = GetWsErrorDescription(transaction.Error.First());
                    await page.DisplayAlert("Atenção", errorText, "Fechar");
                }
                else
                {
                    await
                        page.DisplayAlert("Mensagem", "Transação efetivada", "Ok");

                    var answer =
                        await
                            page.DisplayAlert("Mensagem", "O que deseja fazer agora?", "Nova Bonificação", "Fechar");

                    if (answer)
                    {
                        await page.Navigation.PopAllPopupAsync();
                        await page.Navigation.PopAsync();
                    }
                    else
                    {
                        await page.Navigation.PopAllPopupAsync();
                        await page.Navigation.PopToRootAsync(true);
                    }

                    return true;
                }
            }
            else
            {
                await page.DisplayAlert("Erro", "Erro ao realizar a transação. Tente novamente mais tarde.", "Ok");
            }
            await PopupNavigation.PopAllAsync();
            return false;
        }

        private static double DegreesToRadians(double degrees)
        {
            return degrees * Math.PI / 180.0;
        }

        public static double CalculateDistance(double location1_Latitude, double location1_Longitude, double location2_Latitude, double location2_Longitude)
        {
            double circumference = 40000.0; // Earth's circumference at the equator in km
            double distance = 0.0;

            //Calculate radians
            double latitude1Rad = DegreesToRadians(location1_Latitude);
            double longitude1Rad = DegreesToRadians(location1_Longitude);
            double latititude2Rad = DegreesToRadians(location2_Latitude);
            double longitude2Rad = DegreesToRadians(location2_Longitude);

            double logitudeDiff = Math.Abs(longitude1Rad - longitude2Rad);

            if (logitudeDiff > Math.PI)
            {
                logitudeDiff = 2.0 * Math.PI - logitudeDiff;
            }

            double angleCalculation =
                Math.Acos(
                    Math.Sin(latititude2Rad) * Math.Sin(latitude1Rad) +
                    Math.Cos(latititude2Rad) * Math.Cos(latitude1Rad) * Math.Cos(logitudeDiff));

            distance = circumference * angleCalculation / (2.0 * Math.PI);

            return distance;
        }

        public static string DistanceToUser(decimal value)
        {
            var ret = "";
            var km = value;
            if (km <= 0 || km > 5000) return ret;
            if (km > 1)
                ret = km.ToString("N2", CultureInfo.CurrentCulture) + " km";
            else
                ret = (km * 100).ToString("N2", CultureInfo.CurrentCulture) + " m";
            return ret;
        }

        public static string GetStateCode(string stateName)
        {
            switch (stateName)
            {
                case "Acre":
                    return "AC";

                case "Alagoas":
                    return "AL";

                case "Amazonas":
                    return "AM";

                case "Amapá":
                    return "AP";

                case "Bahia":
                    return "BA";

                case "Ceará":
                    return "CE";

                case "Distrito Federal":
                    return "DF";

                case "Espírito Santo":
                    return "ES";

                case "Goiás":
                    return "GO";

                case "Maranhão":
                    return "MA";

                case "Minas Gerais":
                    return "MG";

                case "Mato Grosso do Sul":
                    return "MS";

                case "Mato Grosso":
                    return "MT";

                case "Pará":
                    return "PA";

                case "Paraíba":
                    return "PB";

                case "Pernambuco":
                    return "PE";

                case "Piauí":
                    return "PI";

                case "Paraná":
                    return "PR";

                case "Rio de Janeiro":
                    return "RJ";

                case "Rio Grande do Norte":
                    return "RN";

                case "Rondônia":
                    return "RO";

                case "Roraima":
                    return "RR";

                case "Rio Grande do Sul":
                    return "RS";

                case "Santa Catarina":
                    return "SC";

                case "Sergipe":
                    return "SE";

                case "São Paulo":
                    return "SP";

                case "Tocantins":
                    return "TO";

                default:
                    return stateName;
            }
        }
    }
}