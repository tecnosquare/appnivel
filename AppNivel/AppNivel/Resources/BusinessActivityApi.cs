﻿using AppNivel.Helpers;
using AppNivel.Models;
using Newtonsoft.Json;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;

namespace AppNivel.Resources
{
    internal class BusinessActivityApi
    {
        public static async Task<BusinessActivityList> GetAsync(HttpClient _client)
        {
            var getBusinessActivity = new GetEstablishment();

            var jsonRequest = JsonConvert.SerializeObject(getBusinessActivity);

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.BusinessActivityList,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            if (requestReturn != null)
            {
                if (string.IsNullOrEmpty(requestReturn.ErrorText))
                {
                    return JsonConvert.DeserializeObject<BusinessActivityList>(requestReturn.Json);
                }
                else
                {
                    var erro = Util.Decrypt(requestReturn.ErrorText, Util.OpenK());
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
    }
}