﻿using AppNivel.Helpers;
using AppNivel.Models;
using Newtonsoft.Json;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;

namespace AppNivel.Resources
{
    internal class TransactionApi
    {
        public static async Task<WSQrCode> GetAuthorizationQr(HttpClient _client)
        {
            var QrCode = new WSQrCode()
            {
                PersonId = Settings.UserId
            };

            var jsonRequest = JsonConvert.SerializeObject(QrCode);

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.QrCode,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            if (requestReturn != null)
            {
                if (string.IsNullOrEmpty(requestReturn.ErrorText))
                {
                    return JsonConvert.DeserializeObject<WSQrCode>(requestReturn.Json);
                }
                else
                {
                    var erro = Util.Decrypt(requestReturn.ErrorText, Util.OpenK());
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
    }
}