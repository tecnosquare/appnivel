﻿using AppNivel.Helpers;
using AppNivel.Models;
using Newtonsoft.Json;
using System;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;

namespace AppNivel.Resources
{
    internal class EstablishmentApi
    {
        public static async Task<WSTransaction> Transaction(HttpClient _client, string senderCpfCnpj, string beneficiaryCpfCnpj, decimal transactionValue, WSTransactionType transactionType, string transactionDoc, bool isChildRewarding, string beneficiaryEmail = "", bool senderCommissioner = false)
        {
            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.Transaction,
                Json = JsonConvert.SerializeObject(new WSTransaction
                {
                    Token = Settings.AppToken,
                    CpfCnpjSender = senderCpfCnpj.Replace(".", "").Replace("-", "").Replace("/", ""),
                    CpfCnpjBeneficiary = beneficiaryCpfCnpj.Replace(".", "").Replace("-", "").Replace("/", ""),
                    TransactionValue = transactionValue,
                    TransactionTypeId = (int)transactionType,
                    IsChildRewarding = isChildRewarding,
                    ChildUserId = (isChildRewarding ? Settings.UserChildId : 0),
                    TransactionDocument = transactionDoc,
                    MailBeneficiary = beneficiaryEmail,
                    MakeSenderBeneficiaryCommissioner = senderCommissioner
                }),
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            return requestReturn?.Json != null ? JsonConvert.DeserializeObject<WSTransaction>(requestReturn.Json) : null;
        }

        public static async Task<TransactionConfirm> ConfirmTransaction(HttpClient _client, int transactionId, string verificationCode, bool isInternal = false)
        {
            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.TransactionConfirm
                ,
                Json = JsonConvert.SerializeObject(new TransactionConfirm
                {
                    TransactionId = transactionId,
                    VerificationCode = verificationCode,
                    IsInternal = isInternal
                }),
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            return requestReturn?.Json != null ? JsonConvert.DeserializeObject<TransactionConfirm>(requestReturn.Json) : null;
        }

        public static async Task<WSPerson> PersonExists(HttpClient _client, string cpfCnpj)
        {
            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.PersonExists,
                Json = JsonConvert.SerializeObject(new WSPerson()
                {
                    CpfCnpj = cpfCnpj.Replace(".", "").Replace("-", "").Replace("/", "")
                }),
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            return requestReturn?.Json != null ? JsonConvert.DeserializeObject<WSPerson>(requestReturn.Json) : null;
        }

        public static async Task<EstablishmentList> GetAsync(HttpClient _client,
            int stateId = 0, int cityId = 0, int page = 0, int id = 0,
            string search = "", int itensPerPage = Settings.ItensPerPage,
            int businessActivityId = 0, bool isOffer = false,
            bool isFavorite = false, double userLat = 0, double userLong = 0)
        {
            var getStablishment = new GetEstablishment
            {
                StateId = stateId,
                CityId = cityId,
                ItensByPage = itensPerPage,
                Page = page,
                Id = id,
                Search = search,
                BusinessActivityId = businessActivityId,
                CustomerId = Settings.UserId,
                IsOffer = isOffer,
                IsFavorite = isFavorite,
                UserLatitude = userLat,
                UserLongitude = userLong
            };

            var jsonRequest = JsonConvert.SerializeObject(getStablishment);

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.EstablishmentList,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            if (requestReturn != null)
            {
                if (string.IsNullOrEmpty(requestReturn.ErrorText))
                {
                    return JsonConvert.DeserializeObject<EstablishmentList>(requestReturn.Json);
                }
                else
                {
                    var erro = Util.Decrypt(requestReturn.ErrorText, Util.OpenK());
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public static async Task<EstablishmentList> GetAsyncNative(HttpClient _client,
            int stateId = 0, int cityId = 0, int page = 0, int id = 0,
            string search = "", int itensPerPage = Settings.ItensPerPage,
            int businessActivityId = 0, bool isOffer = false,
            bool isFavorite = false, double userLat = 0, double userLong = 0, bool isHotOffer = false)
        {
            try
            {
                var getStablishment = new GetEstablishment
                {
                    StateId = stateId,
                    CityId = cityId,
                    ItensByPage = itensPerPage,
                    Page = page,
                    Id = id,
                    Search = search,
                    BusinessActivityId = businessActivityId,
                    CustomerId = Settings.UserId,
                    IsOffer = isOffer,
                    IsFavorite = isFavorite,
                    UserLatitude = userLat,
                    UserLongitude = userLong,
                    IsHotOffer = isHotOffer
                };

                var jsonRequest = JsonConvert.SerializeObject(getStablishment);

                var request = new RequestModel
                {
                    Token = Settings.AppToken,
                    RequestType = (int)RequestType.EstablishmentNativeList,
                    Json = jsonRequest,
                    CurrentCultureName = CultureInfo.CurrentCulture.Name
                };

                var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

                var urlEndpoint = Settings.AppWsUrl;
                urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

                var content = await _client.GetStringAsync(urlEndpoint);
                var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
                var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
                var jsonReturn = Util.Decrypt(hash, Util.OpenK());
                var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
                if (requestReturn != null)
                {
                    if (string.IsNullOrEmpty(requestReturn.ErrorText))
                    {
                        return JsonConvert.DeserializeObject<EstablishmentList>(requestReturn.Json);
                    }
                    else
                    {
                        var erro = Util.Decrypt(requestReturn.ErrorText, Util.OpenK());
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static async Task<EstablishmentList> GetSpecialList(HttpClient _client,
            int stateId = 0, int cityId = 0, int page = 0, int id = 0,
            string search = "", int itensPerPage = Settings.ItensPerPage,
            int businessActivityId = 0, bool isOffer = false,
            bool isFavorite = false, double userLat = 0, double userLong = 0)
        {
            var getStablishment = new GetEstablishment
            {
                StateId = stateId,
                CityId = cityId,
                ItensByPage = itensPerPage,
                Page = page,
                Id = id,
                Search = search,
                BusinessActivityId = businessActivityId,
                CustomerId = Settings.UserId,
                IsOffer = isOffer,
                IsFavorite = isFavorite,
                UserLatitude = userLat,
                UserLongitude = userLong
            };

            var jsonRequest = JsonConvert.SerializeObject(getStablishment);

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.SpecialList,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            if (requestReturn != null)
            {
                if (string.IsNullOrEmpty(requestReturn.ErrorText))
                {
                    return JsonConvert.DeserializeObject<EstablishmentList>(requestReturn.Json);
                }
                else
                {
                    var erro = Util.Decrypt(requestReturn.ErrorText, Util.OpenK());
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
    }
}