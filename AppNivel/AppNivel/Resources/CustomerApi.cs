﻿using AppNivel.Helpers;
using AppNivel.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;

namespace AppNivel.Resources
{
    internal class CustomerApi
    {
        public static async Task Favorite(HttpClient _client, int EstablishmentId, bool isFavorite)
        {
            var favorite = new Favorite
            {
                PersonEstablishmentId = EstablishmentId,
                PersonId = Settings.UserId,
                IsFavorite = isFavorite
            };

            var jsonRequest = JsonConvert.SerializeObject(favorite);

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.Favorite,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            if (!string.IsNullOrEmpty(requestReturn?.ErrorText))
            {
                var erro = Util.Decrypt(requestReturn.ErrorText, Util.OpenK());
            }
        }

        public static async Task<WSCustomer> SaveData(HttpClient _client, string cpfcnpj, string password, string firstname, string lastname, string email, string phone, string city, string postalCode)
        {
            var customer = new WSCustomer()
            {
                CpfCnpj = cpfcnpj,
                Firstname = firstname,
                Lastname = lastname,
                Password = password,
                RegistrationCity = city,
                RegistrationPostalCode = postalCode,
                Contact = new List<WSContact>()
                {
                    new WSContact() { ContactTypeId = (int)ContactType.Email, PersonId = Settings.UserId, Main = true, ContactValue = email},
                    !string.IsNullOrWhiteSpace(phone) ? new WSContact() { ContactTypeId = (int)ContactType.Telefone, PersonId = Settings.UserId, Main = true, ContactValue = phone} : null
                }
            };

            var jsonRequest = JsonConvert.SerializeObject(customer);

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.CustomerRegister,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            return requestReturn?.Json != null ? JsonConvert.DeserializeObject<WSCustomer>(requestReturn.Json) : null;
        }

        public static async Task<RequestModel> AcceptTerms(HttpClient _client)
        {
            var customer = new WSCustomer()
            {
                Id = Settings.UserId
            };

            var jsonRequest = JsonConvert.SerializeObject(customer);

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.AcceptTerms,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            return requestReturn;
        }

        public static async Task<RequestModel> UpdateData(HttpClient _client, string firstname, string lastname, string email, string phone)
        {
            var customer = new WSCustomer()
            {
                Id = Settings.UserId,
                Firstname = firstname,
                Lastname = lastname,
                Contact = new List<WSContact>()
                {
                    new WSContact() { ContactTypeId = (int)ContactType.Email, PersonId = Settings.UserId, Main = true, ContactValue = email},
                    new WSContact() { ContactTypeId = (int)ContactType.Telefone, PersonId = Settings.UserId, Main = true, ContactValue = phone}
                }
            };

            var jsonRequest = JsonConvert.SerializeObject(customer);

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.UpdateCustomer,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            return requestReturn;
        }

        public static async Task<RequestModel> AddAddress(HttpClient _client, string AddressLine1, string AddressLine2, string AddressLine3, string PostalCode, string AddressNumber, string StateName, string CityName)
        {
            var customer = new WSCustomer()
            {
                Id = Settings.UserId,
                Address = new List<WSAddress>
                {
                    new WSAddress()
                    {
                        City = CityName,
                        AddressLine1 = AddressLine1,
                        AddressLine2 = AddressLine2,
                        AddressLine3 = AddressLine3,
                        Main = true,
                        Name = "",
                        Number = AddressNumber,
                        PostalCode = PostalCode,
                        PersonId = Settings.UserId,
                        State = StateName
                    }
                }
            };

            var jsonRequest = JsonConvert.SerializeObject(customer);

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.UpdateCustomerAddress,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            return requestReturn;
        }

        public static async Task<WSPerson> GetPerson(HttpClient _client, int id)
        {
            var person = new WSPerson()
            {
                Id = id
            };

            var jsonRequest = JsonConvert.SerializeObject(person);

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.GetPerson,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            return requestReturn?.Json != null ? JsonConvert.DeserializeObject<WSPerson>(requestReturn.Json) : null;
        }

        public static async Task<bool> GetIsExecutive(HttpClient _client, int id)
        {
            var person = new WSPerson()
            {
                Id = id
            };

            var jsonRequest = JsonConvert.SerializeObject(person);

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.IsExecutiveCommissioner,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            return requestReturn?.Json != null && JsonConvert.DeserializeObject<bool>(requestReturn.Json);
        }

        public static async Task<RemainingIndication> GetIndications(HttpClient _client, int id)
        {
            var person = new WSPerson()
            {
                Id = id
            };

            var jsonRequest = JsonConvert.SerializeObject(person);

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.GetIndications,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            return requestReturn?.Json != null ? JsonConvert.DeserializeObject<RemainingIndication>(requestReturn.Json) : null;
        }

        public static async Task<WSPerson> RegisterCustomerWithCommissioner(HttpClient _client, WSTransaction transaction)
        {
            var jsonRequest = JsonConvert.SerializeObject(transaction);

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.RegisterCustomerWithCommissioner,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            return requestReturn?.Json != null ? JsonConvert.DeserializeObject<WSPerson>(requestReturn.Json) : null;
        }

        public static async Task<WSPerson> GetPerson(HttpClient _client, string cpfCnpj)
        {
            var person = new WSPerson()
            {
                CpfCnpj = cpfCnpj
            };

            var jsonRequest = JsonConvert.SerializeObject(person);

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.GetPerson,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            return requestReturn?.Json != null ? JsonConvert.DeserializeObject<WSPerson>(requestReturn.Json) : null;
        }

        public static async Task<WSCustomer> Login(HttpClient _client, string cpfCnpj, string password, string device, bool loginAsUser = false, string userEmail = "")
        {
            var Login = new Login
            {
                LoginAsUser = loginAsUser,
                UserEmail = userEmail,
                CpfCnpj = cpfCnpj,
                Password = password,
                Device = string.IsNullOrWhiteSpace(device) ? "NOT_REGISTERED" : device
            };

            var jsonRequest = JsonConvert.SerializeObject(Login);

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.Login,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            return requestReturn?.Json != null ? JsonConvert.DeserializeObject<WSCustomer>(requestReturn.Json) : null;
        }

        public static async Task<WSCustomer> Logout(HttpClient _client, string cpfCnpj, string device)
        {
            var Login = new Login
            {
                CpfCnpj = cpfCnpj,
                Password = "",
                Device = string.IsNullOrWhiteSpace(device) ? "NOT_REGISTERED" : device
            };

            var jsonRequest = JsonConvert.SerializeObject(Login);

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.Logout,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            return requestReturn?.Json != null ? JsonConvert.DeserializeObject<WSCustomer>(requestReturn.Json) : null;
        }

        public static async Task<Balance> GetBalance(HttpClient _client, long initialDate = 0, long endDate = 0, int page = 0, int itensPerPage = 20)
        {
            var getBalance = new GetBalanceAmmount
            {
                PersonId = Settings.UserId,
                EndDate = endDate,
                InitialDate = initialDate,
                Take = 0,
                Device = Settings.DeviceToken,
                Page = page,
                ItensPerPage = itensPerPage
            };

            var jsonRequest = JsonConvert.SerializeObject(getBalance);

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.BalanceAmmount,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            if (requestReturn != null)
            {
                if (string.IsNullOrEmpty(requestReturn.ErrorText))
                {
                    return JsonConvert.DeserializeObject<Balance>(requestReturn.Json);
                }
                else
                {
                    var erro = Util.Decrypt(requestReturn.ErrorText, Util.OpenK());
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public static async Task<Balance> GetBalancePaginated(HttpClient _client, long initialDate = 0, long endDate = 0, int page = 0, int itensPerPage = 20)
        {
            var getBalance = new GetBalanceAmmount
            {
                PersonId = Settings.UserId,
                EndDate = endDate,
                InitialDate = initialDate,
                Take = 0,
                Device = Settings.DeviceToken,
                Page = page,
                ItensPerPage = itensPerPage
            };

            var jsonRequest = JsonConvert.SerializeObject(getBalance);

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.BalanceAmmountPaginated,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            if (requestReturn != null)
            {
                if (string.IsNullOrEmpty(requestReturn.ErrorText))
                {
                    return JsonConvert.DeserializeObject<Balance>(requestReturn.Json);
                }
                else
                {
                    var erro = Util.Decrypt(requestReturn.ErrorText, Util.OpenK());
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
    }
}