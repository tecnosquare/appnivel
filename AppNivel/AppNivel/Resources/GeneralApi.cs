﻿using AppNivel.Helpers;
using AppNivel.Models;
using Newtonsoft.Json;
using System;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;

namespace AppNivel.Resources
{
    internal class GeneralApi
    {
        #region Recarga

        public static async Task<WSRecargaCelular> Recharge(HttpClient _client, WsRvRecargaCelular dataRecharge)
        {
            var jsonRequest = JsonConvert.SerializeObject(dataRecharge);//Pega os dados da recarga

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.CreditRecharge,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            if (requestReturn != null)
            {
                return JsonConvert.DeserializeObject<WSRecargaCelular>(requestReturn.Json);
            }
            else
            {
                return null;
            }
        }

        public static async Task<RetornoMetodoOperadorasProdutos> GetProviderCreditAsync(HttpClient _client, string providerCode = "", string userDoc = "")
        {
            var getProduct = new WsRvCodOperadoraLista()
            {
                CodOperadora = providerCode,
                userDoc = userDoc,
            };

            var jsonRequest = JsonConvert.SerializeObject(getProduct);

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.PhoneRechargeGetOperatorProducts,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            if (requestReturn != null)
            {
                return JsonConvert.DeserializeObject<RetornoMetodoOperadorasProdutos>(requestReturn.Json);
            }
            else
            {
                return null;
            }
        }

        #endregion Recarga



        public static async Task<WSAppVersion> GetAppVersion(HttpClient _client)
        {
            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.AppVersion,
                Json = null,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            return requestReturn?.Json != null ? JsonConvert.DeserializeObject<WSAppVersion>(requestReturn.Json) : null;
        }

        public static async Task<WsCEPConsulta> GetPostalCode(HttpClient _client, string postalCode)
        {
            try
            {
                var genericText = new WsGenericText { Text = postalCode };
                var jsonRequest = JsonConvert.SerializeObject(genericText);
                var request = new RequestModel
                {
                    Token = Settings.AppToken,
                    RequestType = (int)RequestType.SearchPostalCode,
                    Json = jsonRequest,
                    CurrentCultureName = CultureInfo.CurrentCulture.Name
                };

                var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

                var urlEndpoint = Settings.AppWsUrl;
                urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

                var content = await _client.GetStringAsync(urlEndpoint);
                var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
                var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
                var jsonReturn = Util.Decrypt(hash, Util.OpenK());
                var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
                return requestReturn?.Json != null
                    ? JsonConvert.DeserializeObject<WsCEPConsulta>(requestReturn.Json)
                    : null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static async Task<WSCity> GetClosestCity(HttpClient _client, double latitude, double longitude, bool hasEstablishment = true)
        {
            var es = new GetEstablishment
            {
                UserLongitude = longitude,
                UserLatitude = latitude,
                IsFavorite = hasEstablishment /* Passando neste campo para aproveitar a classe */
            };
            var jsonRequest = JsonConvert.SerializeObject(es);
            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.ClosestCity,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            return requestReturn?.Json != null ? JsonConvert.DeserializeObject<WSCity>(requestReturn.Json) : null;
        }

        public static async Task<RemainingIndication> Indicate(HttpClient _client, string email, string name, string lastname, PersonTypeEnum personType)
        {
            var indication = new WSIndication
            {
                PersonId = Settings.UserId,
                Name = name,
                Lastname = lastname,
                Email = email,
                PersonTypeId = (int)personType
            };

            var jsonRequest = JsonConvert.SerializeObject(indication);

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.IndicatePerson,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            return requestReturn?.Json != null ? JsonConvert.DeserializeObject<RemainingIndication>(requestReturn.Json) : null;
        }

        public static async Task<RemainingIndication> GetIndications(HttpClient _client)
        {
            var person = new WSPerson()
            {
                PersonId = Settings.UserId,
            };

            var jsonRequest = JsonConvert.SerializeObject(person);

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.GetIndications,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            return requestReturn?.Json != null ? JsonConvert.DeserializeObject<RemainingIndication>(requestReturn.Json) : null;
        }

        public static async Task<StateList> GetStatesAsync(HttpClient _client)
        {
            var getBusinessActivity = new GetEstablishment();

            var jsonRequest = JsonConvert.SerializeObject(getBusinessActivity);

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.StateList,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            if (requestReturn != null)
            {
                if (string.IsNullOrEmpty(requestReturn.ErrorText))
                {
                    return JsonConvert.DeserializeObject<StateList>(requestReturn.Json);
                }
                else
                {
                    var erro = Util.Decrypt(requestReturn.ErrorText, Util.OpenK());
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public static async Task<CityList> GetCitiesAsync(HttpClient _client, string stateCode = "", string searchText = "")
        {
            var getCity = new GetCity()
            {
                StateCode = stateCode,
                SearchText = searchText
            };

            var jsonRequest = JsonConvert.SerializeObject(getCity);

            var request = new RequestModel
            {
                Token = Settings.AppToken,
                RequestType = (int)RequestType.CityList,
                Json = jsonRequest,
                CurrentCultureName = CultureInfo.CurrentCulture.Name
            };

            var soup = Util.Encrypt(JsonConvert.SerializeObject(request), Util.OpenK());

            var urlEndpoint = Settings.AppWsUrl;
            urlEndpoint += "Handle?soup=" + System.Net.WebUtility.UrlEncode(soup);

            var content = await _client.GetStringAsync(urlEndpoint);
            var requestResult = JsonConvert.DeserializeObject<RequestModelResult>(content);
            var hash = System.Net.WebUtility.UrlDecode(requestResult.HandleResult).Replace(" ", "+");
            var jsonReturn = Util.Decrypt(hash, Util.OpenK());
            var requestReturn = JsonConvert.DeserializeObject<RequestModel>(jsonReturn);
            if (requestReturn != null)
            {
                if (string.IsNullOrEmpty(requestReturn.ErrorText))
                {
                    return JsonConvert.DeserializeObject<CityList>(requestReturn.Json);
                }
                else
                {
                    var erro = Util.Decrypt(requestReturn.ErrorText, Util.OpenK());
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
    }
}