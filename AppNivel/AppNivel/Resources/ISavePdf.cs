﻿using System.IO;
using System.Threading.Tasks;

namespace AppNivel.Resources
{
    public interface ISavePdf
    {
        Task SaveFile(string filename, string contentType, MemoryStream stream);
    }
}