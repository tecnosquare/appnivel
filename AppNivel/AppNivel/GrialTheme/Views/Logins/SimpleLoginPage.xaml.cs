using System;

using Xamarin.Forms;

namespace AppNivel
{
    public partial class SimpleLoginPage : ContentPage
    {
        public SimpleLoginPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private async void OnCloseButtonClicked(object sender, EventArgs args)
        {
            await Navigation.PopModalAsync();
        }
    }
}