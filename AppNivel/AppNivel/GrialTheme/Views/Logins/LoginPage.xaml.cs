using AppNivel.Models;
using AppNivel.Resources;
using Plugin.Connectivity;
using Rg.Plugins.Popup.Services;
using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace AppNivel.GrialTheme.Views.Logins
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        protected override void OnAppearing()
        {
            ChangeCountry.Text = Language.ChangeCountry;
            UserDocument.Placeholder = Language.PlaceholderDocument;
            UserPassword.Placeholder = Language.PlaceholderPassword;
            InfoLogin.Text = Language.InfoLogin;
            base.OnAppearing();
        }

        //public async void OnSignupStackTapped(object sender, EventArgs e)
        //{
        //    if (LoginPage.IsPageInNavigationStack<SignUpPage>(Navigation))
        //    {
        //        await Navigation.PopAsync();
        //        return;
        //    }

        //    var signUpPage = new SignUpPage();
        //    await Navigation.PushAsync(signUpPage);
        //}

        //async void OnCloseButtonClicked(object sender, EventArgs args)
        //{
        //    await Navigation.PopModalAsync();
        //}

        private void UserDocument_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            switch (CultureInfo.CurrentCulture.Name)
            {
                case "pt-PT":
                    Util.NifMask(sender, e);
                    break;

                default:
                    if (e.NewTextValue.Length > 14)
                    {
                        Util.CnpjMask(sender, e);
                    }
                    else
                    {
                        Util.CpfMask(sender, e);
                    }
                    break;
            }
        }

        private void RegisterTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Navigation.PushAsync(new GrialTheme.Views.Logins.SignUpPage());
        }

        private void PassRecoveryTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri(Helpers.Settings.UrlForgotPassword));
        }

        private void ChangeCountryTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new Pages.CountrySelectionPage());
        }

        private void WorkerTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            UserDocument.Keyboard = Keyboard.Email;
            InfoLogin.IsVisible = true;
            UserDocument.Placeholder = "Digite seu e-mail";
        }

        private async void SignIn_OnClicked(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var loadingPage = new Pages.LoadingPage();
                await PopupNavigation.PushAsync(loadingPage);
                var isValid = true;

                if (!ExtendedDocumentValidator.IsValid)
                {
                    await DisplayAlert("Aten��o", "O documento digitado n�o � v�lido.", "Fechar");
                    isValid = false;
                }

                if ((string.IsNullOrWhiteSpace(UserPassword.Text) || UserPassword.Text.Length < 6) && isValid)
                {
                    await DisplayAlert("Aten��o", "Sua senha deve conter, no m�nimo, 6 caracteres.", "Fechar");
                    isValid = false;
                }

                if (isValid)
                {
                    var customer = await CustomerApi.Login(new HttpClient(), UserDocument.Text, UserPassword.Text,
                            Helpers.Settings.DeviceToken, Regex.Matches(UserDocument.Text, @"[a-zA-Z]").Count > 0,
                            Regex.Matches(UserDocument.Text, @"[a-zA-Z]").Count > 0 ? UserDocument.Text : "");
                    if (customer != null)
                    {
                        if (customer.Error.Any())
                        {
                            var errorText = Util.GetWsErrorDescription(customer.Error.First());
                            await DisplayAlert("Aten��o", errorText, "Fechar");
                            Helpers.Settings.IsLogged = false;
                        }
                        else
                        {
                            Helpers.Settings.UserId = customer.Id;
                            Helpers.Settings.Username = customer.Firstname + " " + customer.Lastname;
                            Helpers.Settings.UserFirstname = customer.Firstname;
                            Helpers.Settings.UserLastname = customer.Lastname;
                            Helpers.Settings.UserDocument = customer.CpfCnpj;
                            Helpers.Settings.UserPicture = customer.Picture;
                            Helpers.Settings.UserRemainingCustomerIndications = customer.RemainingIndicationCustomer;
                            Helpers.Settings.UserRemainingEstablishmentIndications = customer.RemainingIndicationEstablishment;
                            Helpers.Settings.IndicateEstablishmentLink = customer.EstablishmentIndicationLink;
                            Helpers.Settings.IndicateCustomerLink = customer.CustomerIndicationLink;
                            Helpers.Settings.UserType = customer.PersonTypeId;
                            Helpers.Settings.UserPassword = UserPassword.Text;
                            Helpers.Settings.BusinessActivityFilter = 0;
                            Helpers.Settings.UserHasAddress = customer.Address.Count > 0;

                            if (customer.LoggedAsUser)
                            {
                                Helpers.Settings.IsChildUser = customer.LoggedAsUser;
                                Helpers.Settings.Username = customer.Username;
                                Helpers.Settings.UserChildId = customer.UserId;
                                Helpers.Settings.TotalAccess = customer.TotalAccess;
                                Helpers.Settings.TransactionValueLimit = customer.TransactionValueLimit;
                                Helpers.Settings.TransactionLimit = customer.TransactionLimit;
                            }
                            else
                            {
                                Helpers.Settings.IsChildUser = false;
                                Helpers.Settings.UserChildId = 0;
                                Helpers.Settings.TotalAccess = true;
                                Helpers.Settings.TransactionValueLimit = 0;
                                Helpers.Settings.TransactionLimit = 0;
                            }

                            var contactTypeCel = (int)ContactType.Celular;
                            var contactTypeEmail = (int)ContactType.Email;

                            if (customer.Contact.Any(x => x.ContactTypeId == contactTypeEmail))
                            {
                                var email = customer.Contact.First(x => x.ContactTypeId == contactTypeEmail && x.Main);
                                Helpers.Settings.UserEmail = email.ContactValue;
                            }

                            if (customer.Contact.Any(x => x.ContactTypeId == contactTypeCel))
                            {
                                var phone = customer.Contact.First(x => x.ContactTypeId == contactTypeCel && x.Main);
                                Helpers.Settings.UserPhone = phone.ContactValue;
                            }

                            Helpers.Settings.IsLogged = true;

                            Application.Current.MainPage = new NavigationPage(new Pages.MainPage());
                            await Navigation.PopToRootAsync(true);
                        }
                    }
                    else
                    {
                        await DisplayAlert("Erro", "Erro ao inciar o aplicativo - Realize o login novamente", "Ok");
                    }
                }

                await PopupNavigation.PopAllAsync();
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }

        private void InfoLoginTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            UserDocument.Keyboard = Keyboard.Numeric;
            InfoLogin.IsVisible = false;
            UserDocument.Placeholder = "CPF ou CNPJ";
        }

        public static bool IsPageInNavigationStack<TPage>(INavigation navigation) where TPage : Page
        {
            if (navigation.NavigationStack.Count > 1)
            {
                var last = navigation.NavigationStack[navigation.NavigationStack.Count - 2];

                if (last is TPage)
                {
                    return true;
                }
            }
            return false;
        }
    }
}