using Xamarin.Forms;

namespace AppNivel
{
    public partial class ProductItemFullScreenPage : ContentPage
    {
        public ProductItemFullScreenPage()
        {
            InitializeComponent();

            BindingContext = new ProductViewModel(SampleData.Products[0].Id);
        }
    }
}