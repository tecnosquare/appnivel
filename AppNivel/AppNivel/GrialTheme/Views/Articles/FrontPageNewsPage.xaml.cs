using Xamarin.Forms;

namespace AppNivel
{
    public partial class FrontPageNewsPage : ContentPage
    {
        public FrontPageNewsPage()
        {
            InitializeComponent();

            BindingContext = new FrontPageNewsViewModel();
        }
    }
}