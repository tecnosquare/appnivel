using AppNivel.GrialTheme.ViewModels;
using Xamarin.Forms;

namespace AppNivel
{
    public partial class ChatTimelinePage : ContentPage
    {
        public ChatTimelinePage()
        {
            InitializeComponent();
            BindingContext = new ChatViewModel(useRecent: false);
        }
    }
}