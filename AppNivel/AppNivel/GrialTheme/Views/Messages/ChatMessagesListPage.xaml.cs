using AppNivel.GrialTheme.ViewModels;
using Xamarin.Forms;

namespace AppNivel
{
    public partial class ChatMessagesListPage : ContentPage
    {
        public ChatMessagesListPage()
        {
            InitializeComponent();

            BindingContext = new ChatViewModel(useRecent: false);
        }
    }
}