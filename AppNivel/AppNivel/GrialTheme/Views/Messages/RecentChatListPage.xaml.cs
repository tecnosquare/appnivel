using AppNivel.GrialTheme.ViewModels;

using Xamarin.Forms;

namespace AppNivel
{
    public partial class RecentChatListPage : ContentPage
    {
        public RecentChatListPage()
        {
            InitializeComponent();

            BindingContext = new ChatViewModel(useRecent: true);
        }
    }
}