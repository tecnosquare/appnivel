using Xamarin.Forms;

namespace AppNivel
{
    public partial class DocumentTimelinePage : ContentPage
    {
        public DocumentTimelinePage()
        {
            InitializeComponent();
            BindingContext = new DocumentTimelineViewModel();
        }
    }
}