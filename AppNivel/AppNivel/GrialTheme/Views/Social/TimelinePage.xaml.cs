using Xamarin.Forms;

namespace AppNivel
{
    public partial class TimelinePage : ContentPage
    {
        public TimelinePage()
        {
            InitializeComponent();

            this.BindingContext = new TimelineViewModel();
        }
    }
}