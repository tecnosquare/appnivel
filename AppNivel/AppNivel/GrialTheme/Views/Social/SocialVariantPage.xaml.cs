using AppNivel.GrialTheme.ViewModels;

using Xamarin.Forms;

namespace AppNivel
{
    public partial class SocialVariantPage : ContentPage
    {
        public SocialVariantPage()
        {
            InitializeComponent();

            BindingContext = new SocialViewModel();
        }
    }
}