using AppNivel.GrialTheme.ViewModels;

using Xamarin.Forms;

namespace AppNivel
{
    public partial class TabControlAndroidSamplePage : ContentPage
    {
        public TabControlAndroidSamplePage()
        {
            InitializeComponent();

            BindingContext = new
            {
                Social = new SocialViewModel(),
                Chat = new ChatViewModel(useRecent: true)
            };
        }
    }
}