using AppNivel.GrialTheme.ViewModels;

using Xamarin.Forms;

namespace AppNivel.GrialTheme.Views.Theme
{
    public partial class TabControlCustomSamplePage : ContentPage
    {
        public TabControlCustomSamplePage()
        {
            InitializeComponent();

            BindingContext = new
            {
                Social = new SocialViewModel(),
                Chat = new ChatViewModel(useRecent: true)
            };
        }
    }
}