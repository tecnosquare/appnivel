using AppNivel.GrialTheme.ViewModels;

using Xamarin.Forms;

namespace AppNivel
{
    public partial class TabControlBottomPlacementSamplePage : ContentPage
    {
        public TabControlBottomPlacementSamplePage()
        {
            InitializeComponent();

            BindingContext = new
            {
                Social = new SocialViewModel(),
                Chat = new ChatViewModel(useRecent: true)
            };
        }
    }
}