using AppNivel.GrialTheme.ViewModels;

using Xamarin.Forms;

namespace AppNivel
{
    public partial class TabControliOSSamplePage : ContentPage
    {
        public TabControliOSSamplePage()
        {
            InitializeComponent();

            BindingContext = new
            {
                Social = new SocialViewModel(),
                Chat = new ChatViewModel(useRecent: true)
            };
        }
    }
}