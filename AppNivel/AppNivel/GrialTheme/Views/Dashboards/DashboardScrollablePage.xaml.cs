using Xamarin.Forms;

namespace AppNivel
{
    public partial class DashboardScrollablePage : ContentPage
    {
        public DashboardScrollablePage()
        {
            InitializeComponent();

            BindingContext = new DashboardScrollableViewModel();
        }
    }
}