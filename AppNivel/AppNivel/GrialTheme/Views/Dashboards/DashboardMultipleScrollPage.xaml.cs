using Xamarin.Forms;

namespace AppNivel.GrialTheme.Views.Dashboards
{
    public partial class DashboardMultipleScrollPage : ContentPage
    {
        public DashboardMultipleScrollPage()
        {
            InitializeComponent();

            BindingContext = new DashboardMultipleScrollPageViewModel();
        }
    }
}