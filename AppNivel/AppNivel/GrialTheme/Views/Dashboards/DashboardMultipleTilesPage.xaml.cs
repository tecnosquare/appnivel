using Xamarin.Forms;

namespace AppNivel
{
    public partial class DashboardMultipleTilesPage : ContentPage
    {
        public DashboardMultipleTilesPage()
        {
            InitializeComponent();

            BindingContext = new DashboardMutipleTilesViewModel();
        }
    }
}