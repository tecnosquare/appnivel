using Xamarin.Forms;

namespace AppNivel
{
    public partial class DashboardCardsPage : ContentPage
    {
        public DashboardCardsPage()
        {
            InitializeComponent();

            BindingContext = new DashboardCardsViewModel();
        }
    }
}