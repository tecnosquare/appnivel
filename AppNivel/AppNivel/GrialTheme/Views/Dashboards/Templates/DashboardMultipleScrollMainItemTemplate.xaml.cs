using AppNivel.GrialTheme.Resx;
using System;
using Xamarin.Forms;

namespace AppNivel
{
    public partial class DashboardMultipleScrollMainItemTemplate : ContentView
    {
        public DashboardMultipleScrollMainItemTemplate()
        {
            InitializeComponent();
        }

        public async void OnMovieTapped(object sender, EventArgs args)
        {
            await Application.Current.MainPage.DisplayAlert(
                AppResources.AlertTitleMovieTapped,
                AppResources.AlertMessageMovieShouldPlayMovieNow,
                AppResources.StringOK
            );
        }
    }
}