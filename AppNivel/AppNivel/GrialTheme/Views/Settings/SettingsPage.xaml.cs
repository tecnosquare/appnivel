using AppNivel.Pages;
using AppNivel.Resources;
using Plugin.Connectivity;
using Rg.Plugins.Popup.Services;
using System;
using System.Globalization;
using System.Net.Http;
using Xamarin.Forms;

namespace AppNivel.GrialTheme.Views.Settings
{
    public partial class SettingsPage : ContentPage
    {
        private HttpClient _client = new HttpClient();

        public SettingsPage()
        {
            Helpers.Settings.CurrentPage = "Profile";
            Helpers.Settings.CurrentPage = "Establishment";
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            LabelUserDocument.Text = Language.UserDocument;
            CellphoneLabel.Text = Language.Cellphone;
            UserPhone.Placeholder = Language.Cellphone;

            if (!string.IsNullOrEmpty(Helpers.Settings.UserPicture))
            {
                ProfileImage.Source = "https://www.nivel.com.br/" + Helpers.Settings.UserPicture;
            }

            Username.Text = Helpers.Settings.Username;
            UserFirstname.Text = Helpers.Settings.UserFirstname;
            //UserLastname.Text = Settings.UserLastname;
            UserEmail.Text = Helpers.Settings.UserEmail;
            UserPhone.Text = Helpers.Settings.UserPhone;
            UserDocument.Text = Helpers.Settings.UserDocument;
            base.OnAppearing();
        }

        private void ConfigTapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            Navigation.PopAsync(true);
        }

        private async void SaveData_OnClicked(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var loadingPage = new LoadingPage();
                await PopupNavigation.PushAsync(loadingPage);
                var isValid = true;
                if (!EmailValidator.IsValid)
                {
                    await DisplayAlert("Aten��o", "O e-mail digitado n�o � v�lido.", "Fechar");
                    isValid = false;
                }

                if ((string.IsNullOrWhiteSpace(UserFirstname.Text) || UserFirstname.Text.Length < 2) && isValid)
                {
                    await DisplayAlert("Aten��o", "Digite o seu nome.", "Fechar");
                    isValid = false;
                }

                //if ((string.IsNullOrWhiteSpace(UserLastname.Text) || UserLastname.Text.Length < 2) && isValid)
                //{
                //    await DisplayAlert("Aten��o", "Digite o seu sobrenome.", "Fechar");
                //    isValid = false;
                //}

                if ((string.IsNullOrWhiteSpace(UserPhone.Text) || UserPhone.Text.Length < 5) && isValid)
                {
                    await DisplayAlert("Aten��o", "Digite o seu telefone.", "Fechar");
                    isValid = false;
                }

                if (isValid)
                {
                    var saveDataReturn =
                        await
                            CustomerApi.UpdateData(_client, UserFirstname.Text, "", UserEmail.Text,
                                UserPhone.Text);

                    if (!saveDataReturn.Success)
                    {
                        if (saveDataReturn.ErrorText != null)
                            await DisplayAlert("Aten��o", Util.Decrypt(saveDataReturn.ErrorText, Util.OpenK()), "Fechar");
                        else
                            await DisplayAlert("Aten��o", "N�o foi poss�vel salvar os dados. Tente novamente mais tarde.", "Fechar");
                    }
                    else
                    {
                        Helpers.Settings.Username = UserFirstname.Text;
                        Helpers.Settings.UserFirstname = UserFirstname.Text;
                        Helpers.Settings.UserEmail = UserEmail.Text;
                        Helpers.Settings.UserPhone = UserPhone.Text;
                        await DisplayAlert("Sucesso", "Cadastro atualizado!", "Ok");
                    }
                }

                await PopupNavigation.PopAllAsync();
            }
            else
            {
                Util.NotConnectedMessage(this);
            }
        }

        private void UserPhone_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (CultureInfo.CurrentCulture.Name == "pt-BR")
            {
                Util.CelPhoneMask(sender, e);
            }
        }
    }
}