using Xamarin.Forms;

namespace AppNivel
{
    public partial class NotificationsPage : ContentPage
    {
        public NotificationsPage()
        {
            InitializeComponent();

            BindingContext = new NotificationsViewModel();
        }
    }
}