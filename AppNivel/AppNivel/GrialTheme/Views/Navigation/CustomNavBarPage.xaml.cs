using Xamarin.Forms;

namespace AppNivel
{
    public partial class CustomNavBarPage : ContentPage
    {
        public CustomNavBarPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}