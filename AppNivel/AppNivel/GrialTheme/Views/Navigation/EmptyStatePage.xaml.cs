using System;

using Xamarin.Forms;

namespace AppNivel
{
    public partial class EmptyStatePage : ContentPage
    {
        public EmptyStatePage()
        {
            InitializeComponent();
            BindingContext = this;
        }

        public async void OnCloseButtonClicked(object sender, EventArgs args)
        {
            await Navigation.PopAsync();
        }
    }
}